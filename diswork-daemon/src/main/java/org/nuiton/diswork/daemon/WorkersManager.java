/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.daemon.ActivityStrategy.ActivityStrategies;
import org.nuiton.diswork.daemon.WorkersManager.Worker;
import org.nuiton.diswork.fs.DisworkFileSystem;
import org.nuiton.diswork.fs.DisworkFileSystemException;
import org.nuiton.util.FileUtil;
import org.nuiton.util.StringUtil;
import org.nuiton.util.ZipUtil;

/**
 * The workers-manager aims to run and manage the different workers. A worker
 * is a thread that try to find a jobs and execute them.
 * 
 * The manager can manage multiple workers : one worker
 * doing one job at a time. By running n workers, the daemon can run up
 * to n job at the same time. The number of workers should be set carefully
 * (according to the actual hardware resources available). The number of workers
 * can be set through {@link DisworkConfig#setNumberOfWorkers(Integer)}.
 *  
 * Workers are not always active, before trying to find and job, they check
 * if they will have enough resources to execute the job without bugging 
 * the other processes of the host machine. To do so, the worker-manager
 * allow to define an activity-strategy to be followed by each worker. The
 * activity strategy tell the worker if they are allowed to take resources or
 * not, different strategies are available (see {@link ActivityStrategy}),
 * current strategy can be changed on the fly, while the daemon run. 
 * 
 * @author bleny
 */
public class WorkersManager {

    private static final Log log = LogFactory.getLog(WorkersManager.class);
    
    /** a job found in dir "key", should be move to "value" before running */
    protected static Map<String, String> RUNNING_MOVE = new HashMap<String, String>();
    
    /** a job found in dir "key" that fail should be moved to "value" */ 
    protected static Map<String, String> FAILED_MOVE = new HashMap<String, String>();
    
    /** a job found in dir "key" that is interrupted should be moved to "value" */ 
    protected static Map<String, String> INTERRUPTED_MOVE = new HashMap<String, String>();
    
    static {
        // initialize RUNNING_MOVE and FAILED_MOVE constants
        RUNNING_MOVE.put(DisworkDaemon.TODO, DisworkDaemon.TODO_RUNNING);
        RUNNING_MOVE.put(DisworkDaemon.FAILED_1, DisworkDaemon.FAILED_1_RUNNING);
        RUNNING_MOVE.put(DisworkDaemon.FAILED_2, DisworkDaemon.FAILED_2_RUNNING);
        RUNNING_MOVE.put(DisworkDaemon.TODO_RUNNING, DisworkDaemon.TODO_RUNNING);
        RUNNING_MOVE.put(DisworkDaemon.FAILED_1_RUNNING, DisworkDaemon.FAILED_1_RUNNING);
        RUNNING_MOVE.put(DisworkDaemon.FAILED_2_RUNNING, DisworkDaemon.FAILED_2_RUNNING);

        FAILED_MOVE.put(DisworkDaemon.TODO_RUNNING, DisworkDaemon.FAILED_1);
        FAILED_MOVE.put(DisworkDaemon.FAILED_1_RUNNING, DisworkDaemon.FAILED_2);
        FAILED_MOVE.put(DisworkDaemon.FAILED_2_RUNNING, DisworkDaemon.FAILED_3);

        INTERRUPTED_MOVE.put(DisworkDaemon.TODO_RUNNING, DisworkDaemon.TODO);
        INTERRUPTED_MOVE.put(DisworkDaemon.FAILED_1_RUNNING, DisworkDaemon.FAILED_1);
        INTERRUPTED_MOVE.put(DisworkDaemon.FAILED_2_RUNNING, DisworkDaemon.FAILED_2);
    }

    protected DisworkFileSystem fileSystem;
    protected DisworkConfig config;

    /** a pool of workers */
    protected List<Worker> workers = new ArrayList<Worker>();
    
    /** the current activity strategy followed by all the workers */
    protected ActivityStrategy activityStrategy;
    
    protected File applicationCache;
    
    protected Boolean flag = false;
    
    protected final Object sem = new Object();

    protected void updateFlag() throws DisworkException {
        flag = activityStrategy.canWork();
        if (flag) {
            synchronized (sem) {
                // workers can work, wake up sleeping workers
                sem.notifyAll();
            }
        }
    }

    protected Boolean getFlag() throws DisworkException {
        updateFlag();
        return flag;
    }
    
    /** A worker search a job and execute it.
     * 
     * Jobs are found on the file-system by browsing some special directories,
     * like {@link DisworkDaemon#TODO}. They do not contains jobs but symbolic
     * links to jobs. If a worker find a job, it must move it to another
     * directory ({@link DisworkDaemon#TODO_RUNNING}) for no other worker to
     * take it. Once finished, the job link, if successful, will be moved to
     * {@link DisworkDaemon#DONE}. If failed, the link will be moved to
     * {@link DisworkDaemon#FAILED_1} (read "failed once") for another worker
     * to try it and move it the same may. If the job is failed three times,
     * it's moved to {@link DisworkDaemon#FAILED_3} meaning it will not been 
     * tried again.
     *
     * Here is what a worker do all life long :
     * <ol>
     *   <li>First, look at flag value (using {@link #getFlag()}), if true, try
     *       to find a job, if false, sleep until the flag change</li>
     *   <li>Try to find a job ({@link #findAJob()}), if no job found, wait few
     *       seconds and restart from the beginning.</li>
     *   <li>If a job is found, mark it as started
     *       ({@link #jobIsStarted(String)})</li>
     *   <li>create a temp directory specific for the job</li>
     *   <li>download the application for the job
     *       ({@link #downloadApplication()}) and unzip it in the job dir.
     *       Download way use a cache to prevent downloading multiple time the
     *       same application (cache is in {@link #getApplicationData})</li>
     *   <li>stage input files : get all input files needed for the job from
     *       diswork FS or from the provided URL. Download everything to the job
     *       dir ({@link #stageInputFiles()})</li>
     *   <li>do a last check of {@link #shouldStop} before running the job, if
     *       shouldStop is true, give up the job using
     *       {@link #jobIsInterrupted(String)}</li>
     *   <li>if shouldStop is false, run the process
     *       ({@link #prepareAndRunJob()}). It implies to start a Thread that
     *       constantly read the output of the sub process
     *       ({@link Worker.OutputReader}) and to put data in the input file on
     *       the standard input of the process</li>
     *   <li>wait for the process to end. It may be interrupted due to a query
     *       to the worker manager to stop all worker. If the process is
     *       interrupted is, mark it as interrupted to put it back to the
     *       proposed jobs ({@link #jobIsInterrupted(String)})</li>
     *   <li>When the process ends, whatever the exitValue, upload the results
     *       ({@link #stageOutputFiles}}.</li>
     *   <li>look at the exit value. If the job is a success, mark it
     *       ({@link #jobIsSuccessful(String)}, if it failed, mark it so it will
     *       be proposed to others nodes ({@link #jobIsFailed(String)}</li>
     * </ol>
     * 
     * @author bleny
     */
    protected class Worker extends Thread {

        private final Log log = LogFactory.getLog(Worker.class);

        // TODO 20100614 bleny make it configurable
        /** after this time (ms), a job is considered as no longer running */
        protected static final long MAX_JOB_RUNNING_TIME = 24 * 60 * 60 * 1000;

        /** set this field to true will make run() return and thread stop */
        protected boolean shouldStop = false;

        /** the current job, null if worker do nothing */
        protected JobDescription currentJob = null;
        
        /** current job path on diwork FS */
        protected String currentJobPath = null;
        
        /** current job temp directory on the local file-system */
        protected File currentJobDir = null;

        /** current process, null if nothing is running */
        protected Process currentProcess;

        /** the date when the currentProcess started to be executed */
        protected Long currentProcessStartDate;

        /** read the standard output of the subprocess
         * 
         * By reading the standard output, this thread has multiple goals :
         * <ul>
         *   <li>if the process produces many data on standard output, the
         *       process will block (and waitfor will never return). This
         *       thread, by reading the output, unblock it.
         *       http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=4
         *       </li>
         *   <li>if asked, write the output to a file</li>
         *   <li>add the output to the log</li>
         * </ul>
         * 
         * @author bleny
         */
        protected class OutputReader extends Thread {
        	
        	protected InputStream output;
        	protected OutputStream outputFile;
        	
        	/**
        	 * @param output the stream of the standard output of the process
        	 * @param outputFile the file where to copy, null if you don't
        	 *        care about the what is on the standard output 
        	 */
        	public OutputReader(InputStream output, OutputStream outputFile) {
        		this.output = output;
        		this.outputFile = outputFile;
        	}

        	@Override
            public void run() {
        		InputStreamReader osr = new InputStreamReader(output);
                BufferedReader br = new BufferedReader(osr);
                
                BufferedWriter wr = null;
                OutputStreamWriter outputStreamWriter = null;
                if (outputFile != null) {
                	outputStreamWriter = new OutputStreamWriter(outputFile);
                	wr = new BufferedWriter(outputStreamWriter);
                }

                try {
                	// read a line from standard output
                    String line;
                    while ((line = br.readLine()) != null) {
                        // copy this line to output file
                        if (wr != null) {
                        	wr.write(line + "\n");
                        }
                        
                        // add reading line to logging output                    	
                        if (log.isTraceEnabled()) {
                            log.trace(this.toString() + ">" + line);
                        }
                    }
                } catch (IOException e) {
                    // may occur if process is destroyed
                	log.warn("error while reading the output of the subprocess", e);
                } finally {
                    // close file to make it contain as much data as possible
                	try {
                		if (wr != null) {
                			wr.close();
                		}
                		if (outputStreamWriter != null) {
                			outputStreamWriter.close();
                		}
					} catch (IOException e) {
	                	log.warn("error while closing the output of the subprocess", e);
					}
                }
        	}
        }
        
        /**
         * add a line to a job-specific log
         * @param jobPath the path to the job concerned
         * @param messages the line(s) to add to the log
         * @throws DisworkSystemException if an error occurred while writing
         *         the log 
         */
        protected void log(String jobPath, String... messages)
                              throws DisworkSystemException {
            try {
                String logPath = jobPath + "/" + DisworkDaemon.LOG_PATH;
                InputStream oldLogAsStream = fileSystem.read(logPath);
                String oldLog = IOUtils.toString(oldLogAsStream);
                String logEntry = "";
                for (String message : messages) {
                    logEntry += message + "\n";
                }
                String newLog = oldLog + logEntry;
                log.debug("writing new log content" + newLog);
                fileSystem.write(logPath, IOUtils.toInputStream(newLog));
            } catch (DisworkFileSystemException e) {
                log.error("unable to read or write log file", e);
                throw new DisworkSystemException("unable to read or write log file", e);
            } catch (IOException e) {
                log.error("unable to read log file", e);
                throw new DisworkSystemException("unable to read log file", e);
            }
        }

        /**
         * Check if the current job need an application. If needed,
         * download the application and unzip it in current job temp
         * dir
         * @throws DisworkException
         */
        protected void downloadApplication() throws DisworkException {
            // download application
            if (currentJob.getApplicationName() != null) {
                log.info("dependency needed for " + currentJob + " (" +
                        currentJob.getApplicationName() + "-" +
                        currentJob.getApplicationVersion() + ")");
                File application = getApplicationData(
                                        currentJob.getApplicationName(),
                                        currentJob.getApplicationVersion());
                // unzip application
                log.info("unzip application start");
                try {
                    ZipUtil.uncompress(application, currentJobDir);
                } catch (IOException e) {
                    log.error("error occured while extracting the application", e);
                    throw new LocalFileException("error occured while extracting the application", e);
                }
                log.info("unzip application finished");
            } else {
                log.info("no dependency specified for " + currentJob);
            }            
        }

        /**
         * For all the input files of the current job, download them
         * to the current job temp dir.
         * @throws DisworkException
         */
        protected void stageInputFiles() throws DisworkException {
            // staging input files
            for (String fileName : currentJob.getInput()) {
                log.info("staging " + fileName);
                
                // source is a stream containing the data of the
                // input file
                InputStream source = null;
                
                // set source according to job description, source
                // may be obtained via an URL or via the Diswork
                // File system
                if (currentJob.getInputUrls().containsKey(fileName)) {
                    // download this file from URL
                    URL url = currentJob.getInputUrls().get(fileName);
                    log.info("downloading from " + url);
                    try {
                        source = url.openStream();
                    } catch (IOException e) {
                        log.error("failed to download input data from" + url, e);
                        throw new DisworkSystemException("failed to download input data from" + url, e);
                    }
                } else {
                    // download this file from diswork
                    try {
                        source = fileSystem.read(currentJobPath + "/" + fileName);
                    } catch (DisworkFileSystemException e) {
                        log.error("unable to read input file from diswork", e);
                        throw new DisworkSystemException("unable to read input file from diswork", e);
                    } catch (FileNotFoundException e) {
                        log.warn("input file " + fileName + " is not provided", e);
                        throw new BadJobException("input file " + fileName + " is not provided", e);
                    }
                }

                // now, source is set, read those data and write it
                // to a local copy in the current job temp dir
                try {
                    File localCopy = new File(currentJobDir, fileName);
                    localCopy.createNewFile();
                    IOUtils.copy(source, new FileOutputStream(localCopy));
                } catch (IOException e) {
                    log.error("unable to write input file to local dir", e);
                    throw new LocalFileException("unable to write input file to local dir", e);
                }
            }
        }

        /**
         * Compute the command-line for the current job, prepare the process
         * by pluggin files in standard input/output. Start a thread to 
         * constantly read standard output. Finally, start the process and
         * set {@link #currentProcess}.
         * 
         * @throws DisworkException
         */
        protected void prepareAndRunJob() throws DisworkException {
            log.info("preparing the job");
            // prepare the job and run it
            String commandLine = config.parseCommandLine(
                                                currentJob.getCommandLine(),
                                                currentJobDir.getAbsolutePath());
            // String[] commandLineElements = commandLine.split(" ");
            String[] commandLineElements = StringUtil.split(commandLine, " ");
            ProcessBuilder builder = new ProcessBuilder(commandLineElements);
            builder.directory(currentJobDir);
            builder.redirectErrorStream(true);
            log.info("process will call " + commandLine);
            try {
                currentProcess = builder.start();
            } catch (IOException e) {
                log.error("unable to run process for job" + currentJob, e);
                throw new LocalFileException("unable to run process for job" + currentJob, e);
            }

            // start a thread to constantly read on the standard output
            String standardOutputFileName = currentJob.getStandardOutput();
            log.info("standardOutputFileName is " + standardOutputFileName);
            OutputStream outputFileStream = null;
            if (standardOutputFileName != null) {
                File outputFile = new File(currentJobDir, standardOutputFileName);
                log.info("writing standard output in " + outputFile);
                try {
                    outputFileStream = new FileOutputStream(outputFile);
                } catch (FileNotFoundException e) {
                    // should not occur since we just created the file
                    log.error("unable to find standard output file", e);
                    throw new LocalFileException("unable to find standard output file", e);
                }
            }

            OutputReader outputReader = new OutputReader(currentProcess.getInputStream(),
                                                         outputFileStream);
            log.info("starting job now");
            outputReader.start();

            // plugging a file on the standard input
            String standardInputFileName = currentJob.getStandardInput();
            if (standardInputFileName != null) {
                log.info("writing " + standardInputFileName + " on standard "
                                                            + "input");

                try {
                    InputStream input = new FileInputStream(
                                           new File(currentJobDir, standardInputFileName));
                    IOUtils.copy(input, currentProcess.getOutputStream());
                } catch (FileNotFoundException e) {
                    // file may not have been provided by job submitter
                    if (currentJob.getInput().contains(standardInputFileName)) {
                        log.error("standard input file is not found", e);
                        throw new DisworkSystemException("standard input file is not found", e);
                    } else {
                        throw new BadJobException("standard input file is not provided in " + currentJob);
                    }
                } catch (IOException e) {
                    log.error("unable to read data from input file", e);
                    throw new DisworkSystemException("unable to read data from input file", e);
                }
            }
        }

        protected void stageOutputFiles() throws DisworkSystemException, BadJobException {
            // output file staging 
            for (String fileName : currentJob.getOutput()) {
                File localCopy = new File(currentJobDir, fileName);

                if (localCopy.exists()) {
                    String filePath = currentJobPath + "/" + fileName;
                    // copy local file to diswork FS
                    
                    InputStream localCopyStream = null;
                    try {
                        localCopyStream = new FileInputStream(localCopy);

                        // erase before write if an input file as an output too
                        if (fileSystem.exists(filePath)) {
                            fileSystem.delete(filePath);
                        }

                        log.info("staging file " + fileName);
                        fileSystem.write(filePath, localCopyStream);
                    } catch (FileNotFoundException e) {
                        // file exists, tested just before
                    } catch (DisworkFileSystemException e) {
                        log.error("error while uploading results", e);
                        throw new DisworkSystemException("error while uploading results", e);
                    } finally {
                        IOUtils.closeQuietly(localCopyStream);
                    }
                } else {
                    log.warn("job " + currentJob + " do not produces a file " + fileName);
                }
            }
        }

        /**
         * Read the JSDL file of the curernt job. create a temp directory for
         * the current job and set {@link #currentJobDir}. Prepare the job
         * (download application, stage input files and prepare the process).
         * 
         * Then, if current worker is still allowed to work (if WorkersManager
         * didn't ask to stop working recently), run the process until it returns
         * 
         * Once the process is finished, check why and how the process returned
         * (interrupted, successful run or failure) and act accordingly
         * (update log and stage output files).
         * 
         * Finally, remove temp directory created and the start of the method,
         * unset all current variables, and update statistics about worked time.
         * 
         * @throws DisworkException 
         */
        protected void runJob() throws DisworkException {
            try {

                // We want to have a stat about how many time is passed at
                // working, start counting
                currentProcessStartDate = System.currentTimeMillis();                
                log.info("running job at " + currentJobPath);

                // read the JSDL for the current job, create the corresponding
                // JobDescription and set currentJob
                try {
                    String jsdlPath = currentJobPath + "/" + DisworkDaemon.JSDL_PATH;
                    String jsdl = IOUtils.toString(fileSystem.read(jsdlPath));
                    log.info("read jsdl " + jsdl);
                    currentJob = JobDescription.parseJSDL(jsdl);
                    log.info("will run job " + currentJob);
                } catch (IOException e) {
                    log.error("unable to read or parse JSDL", e);
                    throw new DisworkSystemException("unable to read or parse JSDL", e);
                } catch (DisworkFileSystemException e) {
                    log.error("unable to read JSDL", e);
                    throw new DisworkSystemException("unable to read JSDL", e);
                }
                
                // create a unique temp directory for this job only
                try {
                    currentJobDir = FileUtil.createTempDirectory("job", "",
                                               new File(config.getTempDirectory()));
                    currentJobDir.mkdirs();
                    log.info("created directory " + currentJobDir.getAbsolutePath());
                } catch (IOException e) {
                    log.error("unable to create temp directory for job", e);
                    throw new LocalFileException("unable to create temp directory for job", e);
                }

                // put application data in the temp dir 
                downloadApplication();

                // put input data in temp dir
                stageInputFiles();
        
                // until there we didn't started the job, it's not too late to
                // stop, last check of shouldStrop before running the process
                if (!shouldStop) {
                    // prepare the process and start currentProcess
                    prepareAndRunJob();

                    // mark this job as started
                    jobIsStarted(currentJobPath);

                    try {
                        // wait for the process to return
                        log.info("waiting for the end of the process");
                        int returnValue = currentProcess.waitFor();
                        log.info("process ended (return " + returnValue + ")");
                        
                        if (shouldStop) {
                            // process has returned because stop() called Process.destroy()
                            // so it's not job's fault and should not be considered has a failure
                            jobIsInterrupted(currentJobPath);
                        } else {
                            
                            // process returned and was not interrupted, output files
                            // are interesting in both successful and failure case
                            // upload them
                            stageOutputFiles();
                            
                            if (returnValue == 0) {
                                // job is successful
                                jobIsSuccessful(currentJobPath);
                            } else {
                                // job is a failure, the process returned an error
                                jobIsFailed(currentJobPath);
                            }
                        }
                        
                    } catch (InterruptedException e) {
                        // job was interrupted maybe by a call to WokersManager#stop()
                        log.debug("process was interrupted", e);
                        jobIsInterrupted(currentJobPath);
                    }
                }
            } catch (BadJobException e) {
                // if job is a bad one, consider it as a failure
                jobIsFailed(currentJobPath);
            } finally {
                
                // update stat about how many time was worked
                if (currentProcessStartDate != null) {
                    Long currentTime = System.currentTimeMillis();
                    config.addWorkedTime(currentTime - currentProcessStartDate);
                    currentProcessStartDate = null;
                }

                // clean up the job directory
                FileUtil.deleteRecursively(currentJobDir);

                // unset all variables for this job
                currentJob = null;
                currentProcess = null;
                currentJobDir = null;
            }
        }

        /**
         * browse all running directories. If a jobs is too old, it is considered
         * as interrupted and moved back. All obsolete jobs found are moved to
         * be available again.
         * @return true if an obsolete job has been found
         * @throws DisworkSystemException
         */
        protected boolean checkInteruptedJobs() throws DisworkSystemException {
            // use a synchronized block because multiple workers
            // may try to do concurrent move
            boolean result = false;
            synchronized (fileSystem) {
                
                // all those dirs will be browsed for unfinished obsolete jobs
                String[] runningJobsDirs = { DisworkDaemon.FAILED_2_RUNNING,
                                             DisworkDaemon.FAILED_1_RUNNING,
                                             DisworkDaemon.TODO_RUNNING
                                           };
                
                
                for (String path : runningJobsDirs) {
                    try {
                        List<String> jobsNames = fileSystem.readDirectory(path);
                        
                        // link names are dates so if we sort the content of this
                        // directory by names, old jobs are first and recent are
                        // last, so we can try to read from the beginning and
                        // stop without reading all the list
                        Collections.sort(jobsNames);
 

                        boolean obsoleteJobFound = true;
                        Iterator<String> it = jobsNames.iterator();

                        // iterate until a non-obsolete job is found or until
                        // there is no more file in this directory
                        while (obsoleteJobFound && it.hasNext()) {
                            String jobName = it.next();
                            Long linkAge = System.currentTimeMillis()
                                                     - Long.parseLong(jobName);
                            
                            // check is oldest job is too old and should be
                            // considered has to-be-rerun
                            if (linkAge > MAX_JOB_RUNNING_TIME) {
                                
                                // this link is too old, move it back to proposed jobs
                                String jobPath = path + "/" + jobName;
                                String newJobPath = INTERRUPTED_MOVE.get(path) + "/" + jobName;
                                try {
                                    fileSystem.move(jobPath, newJobPath);
                                    
                                    // an obsolete job was found, update the return value
                                    result = true;
                                } catch (DisworkFileSystemException e) {
                                    log.debug("failed at moving" + jobPath);
                                    // ignore, another node is moving it
                                    // FIXME 20100712 bleny catch the exact exception
                                }
                            } else {
                                // break the iteration, the last elements of the
                                // list are still valid
                                obsoleteJobFound = false;
                            }
                        }
                    } catch (DisworkFileSystemException e) {
                        log.warn("unable to read jobs directory", e);
                        throw new DisworkSystemException("unable to read jobs directory", e);
                    }
                }
            }
            return result;
        }

        /**
         * In a given directory, try to find a job. If a job is found, immediatly
         * move it to running dir and return it.
         * 
         * @param dirPath the directory to browse for a job
         * @return the path to the job found or null if no job found
         * @throws DisworkSystemException
         */
        protected String findAJobInDirectory(String dirPath) throws DisworkSystemException {
            // use a synchronized block because multiple workers
            // may try to take a same job
            synchronized (fileSystem) {
                List<String> jobsNames = null;
                try {
                    jobsNames = fileSystem.readDirectory(dirPath);
                    Collections.sort(jobsNames);
                } catch (DisworkFileSystemException e) {
                    log.warn("unable to read jobs directory", e);
                    throw new DisworkSystemException("unable to read jobs directory", e);
                }
                log.debug(jobsNames.size() + " jobs found at " + dirPath);
                String result = null;
                Iterator<String> it = jobsNames.iterator();
                while (result == null && it.hasNext()) {
                    String jobPath = dirPath + "/" + it.next();
                    String newJobPath = RUNNING_MOVE.get(dirPath) + "/" + DisworkDaemon.newJobLinkName();
                    try {
                        log.debug("job found at " + jobPath + ". moving it to " + newJobPath);
                        fileSystem.move(jobPath, newJobPath);
                        result = newJobPath;
                    } catch (DisworkFileSystemException e) {
                        log.debug("failed at moving" + jobPath);
                        // ignore, another node taking it
                        // FIXME 20100712 bleny catch the exact exception
                    }
                }
                return result;
            }
        }

        /**
         * Check one-by-one the job directories to find a job. If a job
         * is found, 
         * 
         * @return the path to the 
         * @throws DisworkSystemException
         */
        protected String findAJob() throws DisworkSystemException {

            // create a list with all directories where a job can be found
            List<String> jobsDirs = new ArrayList<String>();
            jobsDirs.add(DisworkDaemon.FAILED_2);
            jobsDirs.add(DisworkDaemon.FAILED_1);
            jobsDirs.add(DisworkDaemon.TODO);

            // For all those directories, call findAJobInDirectory until a job
            // is found
            Iterator<String> it = jobsDirs.iterator();
            String result = null;
            while (result == null && it.hasNext()) {
                String jobDir = it.next();
                result = findAJobInDirectory(jobDir);
            }

            // if all directories have been read without finding any job,
            // check for obsolete jobs, and if an obsolete job was found,
            // retry to find a job
            if (result == null) {

                // now, if no job was found
                boolean checkResult = checkInteruptedJobs();

                if (checkResult) {
                    // jobs that were interrupted are now made available, retry
                    return findAJob();
                } else {
                    // even with obsolete jobs, nothing found, waiting
                    // before try again later
                    try {
                        log.info("look for a job was unsuccessful, will wait " + config.getJobLooksWaitTime() + " seconds before next try");
                        Thread.sleep(config.getJobLooksWaitTime() * 1000);
                    } catch (InterruptedException e) {
                        log.error("worker interrupted while waiting before trying to find a new job", e);
                        throw new DisworkSystemException("worker interrupted while waiting before trying to find a new job", e);
                    }
                }
            }
            return result;
        }

        /** update the log of a given job 
         * mark it as done and finished (permit the user to use 
         * {@link DisworkDaemon#isSuccessful(JobDescription)})
         * and move the link 
         */ 
        protected void jobIsSuccessful(String jobPath) throws DisworkSystemException {            
            String newJobPath = DisworkDaemon.DONE + "/" + DisworkDaemon.newJobLinkName();
            try {
                // FIXME 20100720 bleny really useful ?
                fileSystem.move(jobPath, newJobPath);
                log.info("moved " + jobPath + " to " + newJobPath);
            } catch (DisworkFileSystemException e) {
                log.error("error while moving job link", e);
                throw new DisworkSystemException("error while moving job link", e);
            }
            log.info("marking " + newJobPath + " as done and finished");
            log(newJobPath, DisworkDaemon.LOG_KEYWORD_DONE,
                            DisworkDaemon.LOG_KEYWORD_FINISHED);
            config.addOneJobDone();
        }

        /** update the log of the job and move the link
         * permit the job submitter to use
         * {@link DisworkDaemon#isFailed(JobDescription)}
         */
        protected void jobIsFailed(String jobPath) throws DisworkSystemException {
            String jobDir = FilenameUtils.getFullPathNoEndSeparator(jobPath);
            String newDir = FAILED_MOVE.get(jobDir);
            String newJobPath = newDir + "/" + DisworkDaemon.newJobLinkName();

            try {
                // FIXME 20100720 bleny really useful to move to FAILED_3 ? a dir never read by anyone
                fileSystem.move(jobPath, newJobPath);
                log.info("moved " + jobPath + " to " + newJobPath);
            } catch (DisworkFileSystemException e) {
                log.error("error while moving job link", e);
                throw new DisworkSystemException("error while moving job link", e);
            }

            if (newDir.equals(DisworkDaemon.FAILED_3)) {
                log.info("marking " + newJobPath + " as failed and finished");
                log(newJobPath, DisworkDaemon.LOG_KEYWORD_FAILED,
                                DisworkDaemon.LOG_KEYWORD_FINISHED);
            } else {
                log.info("marking " + newJobPath + " as failed");
                log(newJobPath, DisworkDaemon.LOG_KEYWORD_FAILED);                
            }
        }

        /** put the job back where it was found */
        protected void jobIsInterrupted(String jobPath) throws DisworkSystemException {
            String jobDir = FilenameUtils.getFullPathNoEndSeparator(jobPath);
            String jobName = FilenameUtils.getName(jobPath);
            String newDir = INTERRUPTED_MOVE.get(jobDir);
            String newJobPath = newDir + "/" + jobName;
            
            try {
                log.info("moving " + jobPath + " to " + newJobPath);
                fileSystem.move(jobPath, newJobPath);
            } catch (DisworkFileSystemException e) {
                log.error("error while moving job link", e);
                throw new DisworkSystemException("error while moving job link", e);
            }
        }

        /** update the log the job
         * permit the job submitter to use
         * @link {@link DisworkDaemon#isStarted(JobDescription)}
         */
        protected void jobIsStarted(String jobPath) throws DisworkSystemException {
            log(jobPath, DisworkDaemon.LOG_KEYWORD_STARTED);
        }
        
        protected static final boolean NEVER_STOP_MODE = false;

        /**
         * Until shouldStop become true, check {@link WorkersManager#flag} value.
         * The flag tells the worker if the {@link WorkersManager#activityStrategy}
         * allows the worker to run a job.
         * 
         * If flag is true : try to find a job. If a job is found, {@link #currentJobPath}
         * is set and {@link #runJob()} is called.
         * 
         * If flag is false : it means that Activity Strategy don't want the worker
         * to work so the worker will sleep until the Activity Strategy update the
         * flag. Sleep time will depend of the activity strategy,
         * see {@link ActivityStrategy#timeBeforeNextUpdate()}.
         */
        @Override
        public void run() {
            // we want the worker to continue working whatever occurs
            while (! shouldStop) {
                try {
                    synchronized (sem) {
                        if (getFlag()) {
                            currentJobPath = findAJob();
                            if (currentJobPath != null) {
                                runJob();
                                currentJobPath = null;
                            }
                        } else {
                            log.debug("sleeping until a change on flag");
                            long waitTime = activityStrategy.timeBeforeNextUpdate();
                            try {
                                if (waitTime == -1) {
                                    // wait until flag is updated
                                    sem.wait();
                                } else {
                                    sem.wait(waitTime);
                                }
                            } catch (InterruptedException e) {
                                log.warn("interrupted while waiting for a change of activity", e);
                                // throw new DisworkSystemException("interrupted while waiting for a change of activity", e);
                            }
                        }
                    }
                } catch (DisworkException e) {
                    log.warn("exception caught by worker", e);
                    if (NEVER_STOP_MODE) {
                        // no exception is thrown because we want the worker to continue
                        // whatever occurs 
                    } else {
                        throw new RuntimeException(e);                        
                    }
                }
            }
        }
        
        @Override
        public String toString() {
            return getName();
        }
    }
    
    public WorkersManager(DisworkFileSystem fileSystem, DisworkConfig config)
                                                     throws DisworkException {
        this.fileSystem = fileSystem;
        this.config = config;

        applicationCache = new File(config.getTempDirectory(), "cache");
        applicationCache.mkdirs();

        // initialize activityStrategy according to config        
        setActivityStrategy(config.getActivityStrategy());

        // start as many workers as needed
        log.info("will start " + config.getNumberOfWorkers() + " workers");
        for (int i = 1 ; i <= config.getNumberOfWorkers() ; i++) {
            Worker worker = new Worker();
            // worker.setName("disworker-" + i);
            worker.start();
            workers.add(worker);
        }
    }
    
    /** read an application from the file system and use a cache 
     * @throws DisworkSystemException 
     * @throws LocalFileException */
    protected synchronized File getApplicationData(String applicationName,
                                                   String applicationVersion)
                                                     throws DisworkException {
        // method is synchronized to prevent multiple workers to download
        // the same application at the same time
        
        // a file on local File system is a copy of this application
        if (!applicationCache.exists()) {
            applicationCache.mkdirs();
        }
        File cachedApplicationData = new File(applicationCache,
                applicationName + "-" + applicationVersion + ".zip");
        
        if (cachedApplicationData.exists()) {
            // file is already available locally, it has been downloaded before
            // cache worked
            log.debug("cache matches for " + applicationName + "-" + applicationVersion);
        } else {
            // cache doesn't contains a copy of this application, creating one
            // by downloading the application from diswork FS
            
            log.debug("cache fail for " + applicationName + "-" + applicationVersion);
            synchronized (cachedApplicationData) {
                // getting application data from diswork File System 
                String applicationPath = DisworkDaemon.getPathForDependency(
                                                applicationName, applicationVersion);
                InputStream applicationData = null;
                try {
                    applicationData = fileSystem.read(applicationPath);
                } catch (DisworkFileSystemException e) {
                    log.error("unable to get application", e);
                    throw new DisworkSystemException("unable to get application", e);
                } catch (FileNotFoundException e) {
                    log.error("application data is not available", e);
                    throw new BadJobException("application data is not available", e);
                } finally {
                    IOUtils.closeQuietly(applicationData);
                }

                // writing data to local file system 
                OutputStream out = null;
                try {
                    cachedApplicationData.createNewFile();
                    log.info("will create " + cachedApplicationData.getAbsolutePath());
                    out = new FileOutputStream(cachedApplicationData);
                    log.debug("starting copy of " + applicationData.available() + " bytes");
                    IOUtils.copy(applicationData, out);
                } catch (IOException e) {
                    log.error("unable to write application in cache", e);
                    throw new LocalFileException("unable to write application in cache", e);
                } finally {
                    IOUtils.closeQuietly(applicationData);
                    IOUtils.closeQuietly(out);
                }
            }
        }
        return cachedApplicationData;
    }

    /** get a list of jobs that workers are doing. Only for monitoring purpose.
     * 
     * returns list which size is the number of worker. Thus, the list may
     * contains null elements for workers that are doing nothing.
     */
    public List<JobDescription> getAllWorkersCurrentJobs() {
        List<JobDescription> result = new ArrayList<JobDescription>();
        for (Worker worker : workers) {
            result.add(worker.currentJob);
        }
        return result;
    }
    
    public ActivityStrategy getActivityStrategy() {
        return activityStrategy;
    }
    
    public void setActivityStrategy(ActivityStrategy activityStrategy)
                                                     throws DisworkException {
        this.activityStrategy = activityStrategy;
        log.info("switching to " + activityStrategy);
        updateFlag();
    }

    public void setActivityStrategy(String activityStrategyLabel)
                                                     throws DisworkException {
        setActivityStrategy(ActivityStrategies.getNewInstance
                                             (config, activityStrategyLabel));
    }

    public void setActivityStrategy(ActivityStrategies activityStrategies)
                                                     throws DisworkException {
        setActivityStrategy(ActivityStrategies.getNewInstance
                                             (config, activityStrategies));
    }

    public void activeNoActivityStrategy() throws DisworkException {
        setActivityStrategy(ActivityStrategies.NONE);
    }

    public void activeUnlimitedActivityStrategy() throws DisworkException {
        setActivityStrategy(ActivityStrategies.UNLIMITED);
    }

    public void activeLimitedActivityStrategy() throws DisworkException {
        setActivityStrategy(ActivityStrategies.LIMITED);
    }

    public void activeScheduledActivityStrategy() throws DisworkException {
        setActivityStrategy(ActivityStrategies.SCHEDULED);
    }

    public void stop() throws DisworkException {
        // asking to all threads to stop
        for (Worker worker : workers) {
            log.debug("asking " + worker + " to stop");
            worker.shouldStop = true;
        }

        // workers should not take new jobs
        activeNoActivityStrategy();

        // killing all the processes, workers waiting for the end of the process
        // will wake up
        for (Worker worker : workers) {
            if (worker.currentProcess != null) {
                log.debug("killing " + worker + " process");
                worker.currentProcess.destroy();
            }
        }

        // empty the application-cache
        if (applicationCache.exists()) {
            FileUtil.deleteRecursively(applicationCache);
        }

        // waiting for all the workers to actually have finished
        for (Worker worker : workers) {
            while (worker.isAlive()) {
                log.debug("waiting for " + worker + " to return");
                // worker may be sleeping, wake it up
                synchronized (sem) {
                    sem.notifyAll();
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    log.warn("interrupted while waiting for a worker to " +
                             "stop", e);
                }
            }
        }

        log.debug("all workers stopped");
    }
}