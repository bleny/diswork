/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

/**
 * 
 * When writing your command line, you should use provided tokens. Token
 * are piece of command that will be replaced by the job-executor when
 * needed.
 * 
 * <dl>
 *   <dt>%java</dt>
 *     <dd>will be replaced by the actual path of java executable
 *          into the JRE</dd>
 *   <dt>%tmp</dt>
 *     <dd>will be replaced by the the path to dir where the job
 *         is executed, it's nice to use this to set a temp directory</dd>
 *   <dt>%sep</dt>
 *     <dd>will be replaced by the file separator (ie "/" under Linux,
 *         "\" under Windows)</dd>
 * </dl>
 * 
 * This class provides methods to read and parse those data to an XML file
 * following (as far as possible), the Job Submission Description Language
 * Specification.
 * 
 * @link http://en.wikipedia.org/wiki/Job_Submission_Description_Language
 * @link http://www.gridforum.org/documents/GFD.56.pdf
 * 
 * @author bleny
 */
public class JobDescription {

    private static final Log log = LogFactory.getLog(JobDescription.class);
    
    /** an id for diswork, not stored in the JSDL file */
    protected String jobId;

    /** a name for the job, it's a convenience for the user */
    protected String jobName;

    /** the name of the application needed for this job
     * can be null if no application is needed for complete this job 
     */
    protected String applicationName;
    
    /** the version of the application
     *  can't be null if applicationName is set
     */
    protected String applicationVersion;

    /** the command line to execute this job */
    protected String commandLine;

    /** all files expected at the beginning of the job */
    protected List<String> input = new ArrayList<String>();
    
    /** all files expected at the end of the job */
    protected List<String> output = new ArrayList<String>();
    
    /** the name of some input files and the URI where to get it */
    protected Map<String, URL> inputUrls = new HashMap<String, URL>();

    /** data needed for the job, provided through the file-system */
    protected Map<String, InputStream> inputData = new HashMap<String, InputStream>();
    
    /** file where to read the standard input, may be null */
    protected String standardInput;

    /** file where to write the standard output, may be null */
    protected String standardOutput;
    
    /**
     * constructor is protected to prevent bad jobIds. To get a JobDescription
     * instance, a client should use the {@link #JobDescription()} constructor
     * The given instance will have a valid jobId when needed.
     * @param jobId
     */
    protected JobDescription(String jobId) {
        this.jobId = jobId;
    }
    
    public JobDescription() {}
    
    public String getJobId() {
        return jobId;
    }

    protected void setJobId(String jobId) {
        this.jobId = jobId;
    }
    
    public String getCommandLine() {
        return commandLine;
    }
    
    /** set the command line to run for this job
     * command line is intended as a normal shell command line like
     * 
     * program arg1 arg2 arg3
     * 
     * At all place of the command-line, tokens may be used. They will be
     * replaced at runtime. Tokens, like "%java" or "%tmp" are described
     * in class documentation. The worker will parse the command-line to
     * replace token with something suitable to his environment.
     * 
     * @link {@link DisworkConfig#parseCommandLine(String, String)} 
     * @param commandLine a string that may contain tokens
     */
    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }
    
    public String getJobName() {
        return jobName;
    }

    public String getApplicationName() {
        return applicationName;
    }
    
    /**
     * this method is protected to force the use of
     * {@link #setApplication(String, String)} which need to give the version.
     */
    protected void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    /**
     * this method is protected to force the use of
     * {@link #setApplication(String, String)} which need a version.
     */
    protected void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public void setApplication(String applicationName,
                               String applicationVersion) {
        setApplicationName(applicationName);
        setApplicationVersion(applicationVersion);
    }
    
    @Override
    public String toString() {
        return "job : " + jobName + " (" + jobId + ")";
    }

    /**
     * Get a JSDL that describes this job
     * @return a string containing the complete XML
     */
    public String toJSDL() {        
        String jsdl = 
          "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        + "<jsdl:JobDefinition xmlns=\"http://www.example.org/\"\n"
        + "    xmlns:jsdl=\"http://schemas.ggf.org/jsdl/2005/11/jsdl\"\n"
        + "    xmlns:jsdl-posix=\"http://schemas.ggf.org/jsdl/2005/11/jsdl-posix\"\n"
        + "    xmlns:diswork=\"http://nuiton.org/projects/show/diswork\"\n"
        + "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
        + "<jsdl:JobDescription>\n"
        + "  <jsdl:JobIdentification>\n"
        + "    <jsdl:JobName>" + jobName + "</jsdl:JobName>\n"
        + "  </jsdl:JobIdentification>\n"
        + "  <jsdl:Application>\n";
        if (applicationName != null) {
        jsdl += "    <jsdl:ApplicationName>" + applicationName + "</jsdl:ApplicationName>\n"
        + "    <jsdl:ApplicationVersion>" + applicationVersion + "</jsdl:ApplicationVersion>\n";
        }
        jsdl += "    <jsdl-posix:POSIXApplication>\n"
        + "      <jsdl-posix:Executable />\n"
        + "      <jsdl-posix:Argument>" + commandLine + "</jsdl-posix:Argument>\n";
        if (standardInput != null) {
        jsdl += "      <jsdl-posix:Input>" + standardInput + "</jsdl-posix:Input>\n";
        }
        if (standardOutput != null) {
        jsdl += "      <jsdl-posix:Output>" + standardOutput + "</jsdl-posix:Output>\n";    
        }
        jsdl += "    </jsdl-posix:POSIXApplication>\n"
        + "  </jsdl:Application>\n";
        
        for (String inputName : input) {
            jsdl += "  <jsdl:DataStaging diswork:type=\"in\">\n"
                 + "    <jsdl:FileName>" + inputName + "</jsdl:FileName>\n";
            if (inputUrls.containsKey(inputName)) {
                jsdl += 
                    "    <jsdl:Source>\n"
                  + "      <jsdl:URI>" + inputUrls.get(inputName) + "</jsdl:URI>\n"
                  + "    </jsdl:Source>\n";
            }
              jsdl += "  </jsdl:DataStaging>\n";
        }

        for (String outputName : output) {
            jsdl += "  <jsdl:DataStaging diswork:type=\"out\">\n"
                 + "    <jsdl:FileName>" + outputName + "</jsdl:FileName>\n"
                 + "  </jsdl:DataStaging>\n";
        }
        
        jsdl +=
          "</jsdl:JobDescription>\n"
        + "</jsdl:JobDefinition>\n";
        
        return jsdl;
        
    }

    /**
     * Factory method to get a JobDescription from JSDL
     * @param jsdl the content of the JSDL file
     * @return a job description representing the content of the JSDL
     * @throws BadJobException 
     * @throws IOException if JSDL is malformed or if an URL is malformed
     */
    public static JobDescription parseJSDL(String jsdl) throws BadJobException {
        // TODO 20100616 bleny correctly set dependency to JDOM in pom.xml
        JobDescription result = new JobDescription();
        try {
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(IOUtils.toInputStream(jsdl));
            Element jobDefinition = document.getRootElement();

            // namespaces
            Namespace jsdlNamespace = jobDefinition.getNamespace("jsdl");
            Namespace jsdlPosixNamespace = jobDefinition.getNamespace("jsdl-posix");
            Namespace disworkNamespace = jobDefinition.getNamespace("diswork");

            // main element
            Element jobDescription = jobDefinition.getChild("JobDescription",
                                                            jsdlNamespace);

            // job identification
            Element jobIdentification = jobDescription.getChild
                    ("JobIdentification", jsdlNamespace);
            Element jobName = jobIdentification.getChild("JobName",
                                                         jsdlNamespace);
            result.setJobName(jobName.getText());

            // application
            Element application = jobDescription.getChild("Application",
                                                          jsdlNamespace);
            Element applicationName = application.getChild("ApplicationName",
                                                           jsdlNamespace);
            if (applicationName != null) {
                Element applicationVersion = application.getChild
                        ("ApplicationVersion", jsdlNamespace);
                result.setApplication(applicationName.getText(),
                                      applicationVersion.getText());
            }

            Element POSIXApplication = application.getChild("POSIXApplication",
                                                            jsdlPosixNamespace);

            Element argument = POSIXApplication.getChild("Argument",
                                                         jsdlPosixNamespace);
            result.setCommandLine(argument.getText());

            Element input = POSIXApplication.getChild("Input",
                                                      jsdlPosixNamespace);
            if (input != null) {
                result.setStandardInput(input.getText());
            }
            Element output = POSIXApplication.getChild("Output",
                                                       jsdlPosixNamespace);
            if (output != null) {
                result.setStandardOutput(output.getText());
            }

            // staging
            List<Element> dataStagings = jobDescription.getChildren
                    ("DataStaging", jsdlNamespace);
            for (Element dataStaging : dataStagings) {
                Attribute type = dataStaging.getAttribute("type",
                                                          disworkNamespace);
                Element fileName = dataStaging.getChild("FileName",
                                                        jsdlNamespace);
                if ("in".equals(type.getValue())) {
                    Element source = dataStaging.getChild("Source",
                                                          jsdlNamespace);
                    if (source != null) {
                        Element URI = source.getChild("URI", jsdlNamespace);
                        result.addInput(fileName.getText(), new URL(URI.getText()));
                    } else {
                        result.addInput(fileName.getText(), new NullInputStream(0));
                    }
                }
                if ("out".equals(type.getValue())) {
                    result.addOutput(fileName.getText());
                }
            }
        } catch (MalformedURLException e) {
            log.error("malformed URL", e);
            throw new BadJobException("malformed URL", e);
        }   catch (IOException e) {
            log.error("can't read malformed JSDL file", e);
            throw new BadJobException("can't read malformed JSDL file", e);
        } catch (JDOMException e) {
            log.error("can't read malformed JSDL file", e);
            throw new BadJobException("can't read malformed JSDL file", e);
        }

        return result;
    }

    public void addInput(String fileName, URL source) {
        input.add(fileName);
        inputUrls.put(fileName, source);
    }

    public void addInput(String fileName, InputStream source) {
        input.add(fileName);
        inputData.put(fileName, source);
    }

    public void addOutput(String fileName) {
        output.add(fileName);
    }

    public List<String> getInput() {
        return input;
    }

    public List<String> getOutput() {
        return output;
    }

    public Map<String, URL> getInputUrls() {
        return inputUrls;
    }

    public String getStandardInput() {
        return standardInput;
    }

    public void setStandardInput(String fileName) {
        standardInput = fileName;
    }

    public String getStandardOutput() {
        return standardOutput;
    }

    public void setStandardOutput(String fileName) {
        standardOutput = fileName;
    }

    public Map<String, InputStream> getInputData() {
        return inputData;
    }

}