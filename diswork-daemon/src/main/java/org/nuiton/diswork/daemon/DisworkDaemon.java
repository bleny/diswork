/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkAlreadyExistsException;
import org.nuiton.diswork.fs.DisworkFileSystem;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.diswork.fs.DisworkFileSystemException;
import org.nuiton.util.FileUtil;

/**
 * The diswork daemon is the gateway to the global diswork system. Instantiate
 * this class creates a new node on the system. The new node can be used
 * to submit jobs and monitor them.
 * 
 * For how-to use considerations see ...
 * 
 * Here is a list of what the daemon do when booting :
 * <ul>
 *   <li>join the network, given some information (IP+port) provided
 *       with diswork-config (a diswork-config is an instance of
 *       {@link DisworkConfig})</li>
 *   <li>define what is the ownedId for this node. It has to be
 *       recovered from the last run of the daemon. It's
 *       the link between two different sessions : if you submit a job, stop
 *       and restart the daemon with the same ownerId, you will find your
 *       job back. If ownerId can't be recovered or if it's the first
 *       run, an ownerId is generated and saved for future run.</li>
 *   <li>define the home-directory, it's deduced from the ownerId. Thus, if you
 *       change you ownerId, you'll change your home-dir and loose your jobs
 *       because they are all stored in your home-dir. If it's the first
 *       time the daemon is run by this ownerId, the homeDir is created</li>
 *   <li>If diswork is configured for, the daemon will start a workers-manager.
 *       This manager don't care about the jobs you submit but look
 *       for jobs that are proposed on the global system and try to
 *       help. For more details, see {@link WorkersManager}</li>
 *   <li>Write some stats about itself. Since all nodes do that, each nodes
 *       can performs statistics computation to estimate the powerfulness of
 *       the global diswork system. Each node write those info in a file
 *       in his home-dir at {@link #HARDINFO_PATH}</li>
 * </ul>
 * 
 * The main use of the daemon is to submit job to the global diswork
 * system.
 * 
 * A job is a task that need an application. An application is a simple file
 * that should be provided to the daemon once for all
 * (using {@link #submitApplication(String, String, InputStream)}. Then, all
 * nodes that would like to perform this job can download the application.
 * 
 * You can create your application ready for diswork, submit it to the daemon
 * and then submit as much jobs as you want that depends of this application.
 * Think an application contains the common stuff (programs, scripts, files etc.)
 * all the different jobs will need. An application is just a set of file.
 * 
 * Once the application has been made available, jobs can be created and
 * submitted. To do so, a programmer must write a class that
 * 
 * <ol>
 *   <li>may republish the application (it's always good to be sure an
 *       application is still present if the last time that it has
 *       been used is too long)</li>
 *   <li>Create a new {@link JobDescription} and set it right for his
 *       new job.</li> 
 *   <li>Submit the job by calling {@link #submitJob(JobDescription)}</li>
 *   <li>Monitor the job with {@link #isFinished(JobDescription)}</li>
 *   <li>Check the job state with {@link #isFailed(JobDescription)}
 *       and {@link #isSuccessful(JobDescription)} and get the results with
 *       {@link #getResults(JobDescription)}</li>
 *   <li>Finally, remove the job</li>
 * </ol>
 * 
 * Jobs will be saved in home-dir, in a path generated with
 * {@link #getPathForJob(JobDescription)}. In this directory will be placed
 * <ul>
 *   <li>All the data provided with the job {@link JobDescription#inputData}</li>
 *   <li>The JSDL file that describe the job (placed according to the
 *       value of {@link #JSDL_PATH})</li>
 *   <li>A log file placed at {@link #LOG_PATH}, it will be empty at the
 *       submission but lines will be added each time a node try to do
 *       this job. If the job is finished, the log will contain "FINISHED",
 *       if it has been completed, it will contain "DONE", if it has been
 *       failed, it will contain "FAILED" one or few times.</li>
 * </ul>
 * 
 * The daemon should be closed properly by using the {@link #close()} method.
 * It will release resources, update stats and stop the worker-manager.
 * 
 * As far as possible, the use of the file system follow the UNIX Filesystem
 * Hierarchy Standard. All usual paths used by Diswork are defined in
 * constants.
 * 
 * @link http://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
 *
 * @author bleny
 */
public class DisworkDaemon {

    private static final Log log = LogFactory.getLog(DisworkDaemon.class);

    /** contains applications */
    protected static final String BIN = "/bin";

    /** new jobs */
    protected static final String TODO = "/var/jobs/todo";
    
    /** jobs taken that was never tried */
    protected static final String TODO_RUNNING = "/var/jobs/todo_running";
    
    /** jobs failed once */
    protected static final String FAILED_1 = "/var/jobs/failed_1";
    
    /** jobs taken that was failed once */
    protected static final String FAILED_1_RUNNING = "/var/jobs/failed_1_running";
    
    /** jobs failed two times */
    protected static final String FAILED_2 = "/var/jobs/failed_2";
    
    /** jobs taken that was failed two times */ 
    protected static final String FAILED_2_RUNNING = "/var/jobs/failed_2_running";
    
    /** jobs failed 3 times */
    protected static final String FAILED_3 = "/var/jobs/failed_3";
    
    /** jobs done */
    protected static final String DONE = "/var/jobs/done";
    
    /** a place where are all user-directories */
    protected static final String HOME = "/home";
    
    /** in home, where jobs should be placed */
    protected static final String JOBS_DIR = "jobs";

    /** in a job directory, the place where the JSDL must be placed */
    protected static final String JSDL_PATH = ".diswork/job.jsdl";
    
    /** in a job directory, the place where the log must be placed */
    protected static final String LOG_PATH = ".diswork/job.log";
    
    /** in a home directory, the place where the hardware info must be placed */
    protected static final String HARDINFO_PATH = "hardinfo";

    /** a keyword that will be put in a job log-file on a single line when the job is started */
    protected static final String LOG_KEYWORD_STARTED = "STARTED";

    /** a keyword that will be put in a job log-file on a single line when the job is started */
    protected static final String LOG_KEYWORD_DONE = "DONE";

    /** a keyword that will be put in a job log-file on a single line when the job is failed */
    protected static final String LOG_KEYWORD_FAILED = "FAILED";

    /** a keyword that will be put in a job log-file on a single line when the job is failed */
    protected static final String LOG_KEYWORD_FINISHED = "FINISHED";
    
    
    /** the distributed file system where jobs, data and results are stored */ 
    protected DisworkFileSystem fileSystem;

    /** provide the configuration data about the daemon */
    protected DisworkConfig config;

    /** time when the deamon started this time, used for total uptime stat */
    protected Long sessionStartTime;
    
    /** owner id for this node */
    protected String ownerId;
    
    /** path to owned directory on fileSystem */
    protected String homeDir;
    
    /** worker-manager will make the daemon accomplish jobs */
    protected WorkersManager workers;

    protected HttpFrontEnd httpFrontEnd;
    
    public DisworkDaemon(DisworkConfig config) throws DisworkException {
        this.config = config;

        sessionStartTime = System.currentTimeMillis();
        // step by step, set all the dependencies of the daemon

        initFileSystem();

        initOwnerIdAndHomeDir();

        initWorkersManager();

        writeLocalStats();
        
        httpFrontEnd = new HttpFrontEnd(config, this);
        
    }

    /* *** init methods, used once by the constructor */

    /**
     * set {@link #fileSystem} and create all base directories
     * on the File System
     */
    protected void initFileSystem() throws DisworkException {

        try {
            DisworkFileSystemConfig fileSystemConfig =
                                                 config.getFileSystemConfig();

            fileSystem = new DisworkFileSystem(fileSystemConfig);

            // init fileSystem with all needed directories
            String[] directories = { BIN, HOME, TODO, TODO_RUNNING, FAILED_1,
                FAILED_1_RUNNING, FAILED_2, FAILED_2_RUNNING, FAILED_3, DONE,
                "/proc"};
            // if HOME exists, we suppose all others exists
            if (! fileSystem.exists(HOME)) {
                for (String directory : directories) {
                    if (! fileSystem.exists(directory)) {
                        fileSystem.createDirectories(directory);
                        log.info("created " + directory);
                    }
                }
            }
        } catch (DisworkFileSystemException e) {
            log.error("booting diswork file system failed", e);
            throw new DisworkException("booting diswork file system failed", e);
        }
    }

    /**
     * set {@link #ownerId} and {@link #homeDir}
     * @throws DisworkException
     */
    protected void initOwnerIdAndHomeDir() throws DisworkException {
        ownerId = config.getOwnerId(); // get job owner id from config

        if (ownerId == null) { // first time running the daemon
            log.info("can't find owner id, generating a new one");

            // check home dir do not exists
            try {
                // generate a new one by cheking if home dir exists
                String simpleName = System.getProperty("user.name", "anonymous");
                ownerId = simpleName;
                boolean alreadyExists = fileSystem.exists(HOME + "/" + ownerId);

                // if simpleName is already taken, try simpleName + random
                Random random = new Random();
                while (alreadyExists) {
                    ownerId = simpleName + random.nextInt();
                    alreadyExists = fileSystem.exists(HOME + "/" + ownerId);
                }

                fileSystem.createDirectory(HOME + "/" + ownerId);

            } catch (DisworkFileSystemException e) {
                log.error("can't create home dir", e);
                throw new DisworkException("can't create home dir", e);
            }

            config.setOwnerId(ownerId);

            config.setFirstRunTime(sessionStartTime);

            // config.saveForUser();
        }

        homeDir = HOME + "/" + ownerId;

        log.info("owner id is " + ownerId);        
    }

    /**
     *
     * @throws DisworkException
     */
    protected void initWorkersManager() throws DisworkException {
        // cleaning the temp dir
        FileUtil.deleteRecursively(config.getTempDirectory());
        
        // check if config implies to run a worker
        if (config.getNumberOfWorkers() >= 0) {
            workers = new WorkersManager(fileSystem, config);
        } else {
            log.info("worker manager disabled");
        }
    }

    protected void writeLocalStats() throws DisworkException {
        try {
            String infos = "";
            Map<String, String> localStats = getLocalStats();
            for (String key : localStats.keySet()) {
                infos += key + "\t" + localStats.get(key) + "\n";
            }
            fileSystem.write(homeDir + "/" + HARDINFO_PATH,
                             IOUtils.toInputStream(infos));
            log.info("writing local infos " + infos.replaceAll("\n", " "));
        } catch (DisworkFileSystemException e) {
            log.error("can't write hardware infos", e);
            throw new DisworkException("can't write hardware infos", e);
        }
    }
    
    /* *** methods about the web based front end *** */
    
    public void startHttpFrontEnd() throws DisworkException {
        httpFrontEnd.start();
    }
    
    public void stopHttpFrontEnd() throws DisworkException {
        httpFrontEnd.stop();
    }
    
    /* *** methods for defining some usual paths *** */

    /**
     * given a name and a version for an application, returns the path where
     * application data can be found on diswork file system.
     * @param applicationName
     * @param applicationVersion
     * @return a path
     */
    protected static String getPathForDependency(String applicationName,
                                                 String applicationVersion) {

        String result = BIN + "/" + applicationName // application directory
                      + "/"
                      // application file name
                      + applicationName + "-" + applicationVersion + ".zip";
        return result;
    }

    /**
     * Given a job description, returns the place on disworkFS where all data
     * for this jobs should be stored
     * @param jobDescription
     * @return an absolute path
     */
    protected String getPathForJob(JobDescription jobDescription) {
        return getPathForJob(jobDescription.getJobId());
    }
    
    /**
     * Given a job id, returns the place on disworkFS where all data
     * for this jobs should be stored
     * @param jobId
     * @return a path
     */
    protected String getPathForJob(String jobId) {
        // all jobs are stored in home dir
        return homeDir + "/" + JOBS_DIR + "/" + jobId;
    }

    /**
     * every-time a link to a job is created or modified in the job, his name
     * has to be generated by this method
     * @return the name to use for a link
     */
    protected static String newJobLinkName() {
        return ((Long) System.currentTimeMillis()).toString();
    }


    /* ** public methods for use of the daemon */

    /** true if a node already submitted this application on diswork */
    public boolean isApplicationAvailable(String applicationName, 
                                          String applicationVersion)
                                                     throws DisworkException {
        if (applicationName == null) {
            throw new NullPointerException("application name is null");
        }
        if (applicationVersion == null) {
            throw new NullPointerException("application version can't be null");
        }
        
        String path = getPathForDependency(applicationName, applicationVersion);
        
        Boolean result = null;
        try {
            result = fileSystem.exists(path);
        } catch (DisworkFileSystemException e) {
            throw new DisworkException("unable to access file-system", e);
        }
        return result;
    }
    
    /**
     * Provide an application to all nodes. Once provided, all nodes will be
     * able to perform a job with this application.
     * 
     * An application can be uploaded in different version. If a given
     * application in the given version is already on the File System, nothing
     * is done : the application file is <strong>NOT</strong> replaced.
     *  
     * @param applicationName the name of the application
     * @param applicationVersion the version of the application
     * @param applicationData an InputStream on the application .zip file
     * @throws DisworkException if an error occurs while uploading the file
     */
    public void submitApplication(String applicationName, 
                         String applicationVersion, InputStream applicationData)
                                                     throws DisworkException {

        if (applicationName == null) {
            throw new NullPointerException("application name is null");
        }
        if (applicationVersion == null) {
            throw new NullPointerException("application version can't be null");
        }
        if (applicationData == null) {
            throw new NullPointerException("application data can't be null");
        }

        // the place where dependency will be stored
        String path = getPathForDependency(applicationName, applicationVersion);

        String applicationDirectory = FilenameUtils.getFullPathNoEndSeparator(path);

        try {
            if (!fileSystem.exists(applicationDirectory)) {
                fileSystem.createDirectory(applicationDirectory);
            }

            if (fileSystem.exists(path)) {
                log.info("application " + applicationName + " is already available"
                        + " in version " + applicationVersion + " submit ignored");
            } else {
                fileSystem.write(path, applicationData);
            }
        } catch (DisworkFileSystemException e) {
            log.error("unable to publish application", e);
            throw new DisworkException("unable to publish application", e);
        }
    }

    /**
     * return a list of jobs submitted before for a particular application  
     * @param applicationName the name of the application, can be null
     * @return the submitted jobs (all if parameter was null, and only jobs
     *         for a particular application if parameter was set)
     * @throws DisworkException
     */
    public List<JobDescription> getAllJobs(String applicationName)
                                                       throws DisworkException {
        List<JobDescription> result = new ArrayList<JobDescription>();
        try {
            String jobsDir = homeDir + "/" + JOBS_DIR;
            if (fileSystem.exists(jobsDir)) {
                List<String> jobs = fileSystem.readDirectory(jobsDir);
                for (String jobId : jobs) {
                    String jsdl = IOUtils.toString(fileSystem.read(
                                       getPathForJob(jobId) + "/" + JSDL_PATH));
                    JobDescription jobDescription = JobDescription.parseJSDL(jsdl);
                    if (applicationName == null // don't filter application
                        || applicationName.equals(jobDescription.getApplicationName())) {
                        jobDescription.setJobId(jobId);
                        result.add(jobDescription);
                    }
                }
            }
        } catch (IOException e) {
            log.error("error in file while reading home-dir", e);
            throw new DisworkException("error in file while reading home-dir", e);
        } catch (DisworkFileSystemException e) {
            log.error("file-system error", e);
            throw new DisworkException("file-system error", e);
        }
        return result;
        
    }

    /**
     * Returns a list with all jobs submitted before.
     * @return
     * @throws DisworkException
     */
    public List<JobDescription> getAllJobs() throws DisworkException {
        return getAllJobs(null);
    }

    /**
     * Cancel the submission of a job
     * @param jobDescription
     * @throws DisworkException
     */
    public void deleteJob(JobDescription jobDescription) throws DisworkException {
        if (jobDescription == null) {
            throw new NullPointerException("job is null");
        }

        try {
            String jobPath = getPathForJob(jobDescription);
            if (fileSystem.exists(jobPath)) {
                log.info("cancelling job " + jobDescription);
                fileSystem.deleteRecursively(jobPath);
            } else {
                log.warn("can't cancel unknown job " + jobDescription);
            }
        } catch (DisworkFileSystemException e) {
            log.info("error while deleting a job", e);
            throw new DisworkException("error while deleting a job", e);
        }
    }

    public void submitJob(JobDescription jobDescription) throws DisworkException {
        if (jobDescription == null) {
            throw new NullPointerException("job is null");
        }

        // if an application is declared is used, check it is available
        if (jobDescription.getApplicationName() != null) {
            if ( ! isApplicationAvailable(jobDescription.getApplicationName(),
                                          jobDescription.getApplicationVersion())) {
                throw new DisworkException("job require a dependency " + 
                        jobDescription.getApplicationName() + "-" +
                        jobDescription.getApplicationVersion()
                        + " that is not available");
            }
        } else {
            log.debug("no dependency specified for " + jobDescription);
        }
        
        // check all dependencies are provided
        for (String name : jobDescription.getInput()) {
            if (!jobDescription.getInputData().containsKey(name) &&
                !jobDescription.getInputUrls().containsKey(name)) {
                throw new BadJobException("dependency " + name + " is missing");
            }
        }

       try {
            // trying to put the job in a new directory of home
            Random random = new Random();
            boolean alreadyExists = true;
            String newJobIntendifier = null;
            while (alreadyExists) {
                Integer randomInteger = random.nextInt();
                newJobIntendifier = "job_" + randomInteger.toString();
                alreadyExists = fileSystem.exists(getPathForJob(newJobIntendifier));
            }

            // XXX side-effect
            jobDescription.setJobId(newJobIntendifier);

            // create both job path and sub-directory .diswork
            fileSystem.createDirectories(
                              getPathForJob(jobDescription) + "/" + ".diswork");
            
            String jobDir = getPathForJob(jobDescription);
    
            // creating an empty log file
            log.info("creating log file " + jobDir + "/" + LOG_PATH);
            fileSystem.write(jobDir + "/" + LOG_PATH, IOUtils.toInputStream(""));
            
            // writing the JSDL file
            InputStream jobJSDL = IOUtils.toInputStream(jobDescription.toJSDL());
            fileSystem.write(jobDir + "/" + JSDL_PATH, jobJSDL);
            
            // file staging
            for (String fileName : jobDescription.getInputData().keySet()) {
                fileSystem.write(jobDir + "/" + fileName,
                                 jobDescription.getInputData().get(fileName));
            }

            // propose job
            boolean success = false;
            while (!success) {
                try {
                    String linkName = newJobLinkName();
                    // there is a risk that jobs be proposed at the same time
                    fileSystem.createSymbolicLink(TODO + "/" + linkName, jobDir);
                    success = true;
                } catch (DisworkAlreadyExistsException e) {
                    // retry
                    success = false;
                }
            }
            config.addOneJobSubmitted();
            log.info("job submited");
        } catch (DisworkFileSystemException e) {
            log.error("file system error", e);
            throw new DisworkException(e);
        }
    }

    protected boolean checkLogContains(JobDescription job,
                                    String pattern) throws DisworkException {
        try {
            String jobPath = getPathForJob(job);
            List<?> entries = IOUtils.readLines(fileSystem.read(jobPath + "/" + LOG_PATH));
            return entries.contains(pattern);
        } catch (IOException e) {
            log.info("unable to read log ", e);
            throw new DisworkException("unable to read log ", e);
        } catch (DisworkFileSystemException e) {
            log.error("unable to read log file " + job, e);
            throw new DisworkException("unable to read log file " + job, e);
        }
    }

    public boolean isStarted(JobDescription job) throws DisworkException {
        if (job == null) {
            throw new NullPointerException("job is null");
        }

        return checkLogContains(job, LOG_KEYWORD_STARTED);
    }

    public boolean isFinished(JobDescription job) throws DisworkException {
        if (job == null) {
            throw new NullPointerException("job is null");
        }

        return checkLogContains(job, LOG_KEYWORD_FINISHED);
    }

    public boolean isSuccessful(JobDescription job) throws DisworkException {
        if (job == null) {
            throw new NullPointerException("job is null");
        }

        return checkLogContains(job, LOG_KEYWORD_DONE);
    }

    public boolean isFailed(JobDescription job) throws DisworkException {
        if (job == null) {
            throw new NullPointerException("job is null");
        }

        return isFinished(job) && !isSuccessful(job);
    }

    /**
     * Must not be called until {@link #isFinished(JobDescription)} returns
     * true for this job. 
     * @param job
     * @return a map associating the file-name of a result to a stream to
     *         read the file
     * @throws DisworkException if job is not finished
     */
    public Map<String, InputStream> getResults(JobDescription job)
                                                     throws DisworkException {
        
        if (job == null) {
            throw new NullPointerException("job is null");
        }
        
        if (!isFinished(job)) {
            throw new DisworkException("can't get results for unfinished job "
                    + job);
        }
        
        Map<String, InputStream> results = new HashMap<String, InputStream>(); 
        for (String fileName : job.getOutput()) {
            String jobPath = getPathForJob(job);
            try {
                InputStream result = fileSystem.read(jobPath + "/" + fileName);
                results.put(fileName, result);
            } catch (DisworkFileSystemException e) {
                log.error("file system error ", e);
                throw new DisworkException("file system error ", e);
            } catch (FileNotFoundException e) {
                log.warn("expected output file was not found : " + fileName, e);
            }
        }
        return results;

    }

    
    /**
     * 
     */
    public void leave() throws DisworkException {
        try {
            fileSystem.deleteRecursively(homeDir);
        } catch (DisworkFileSystemException e) {
            log.error("error while leaving the system", e);
            throw new DisworkException("error while leaving the system", e);
        }
    }
    
    /** close the daemon (stop all workers)
     * update statistics and delete all temporary data
     */
    public void close() throws DisworkException {
        if (workers != null) {
            workers.stop();
        }

        try {
            fileSystem.delete(homeDir + "/" + HARDINFO_PATH);
        } catch (DisworkFileSystemException e) {
            log.warn(e);
        }
        
        // updating total uptime statistic
        Long totalUptime = getTotalUptime();
        log.info("saving total uptime: " + totalUptime);
        config.setTotalUptime(totalUptime);

        try {
            fileSystem.close();
        } catch (DisworkFileSystemException e) {
            log.error("unable to close file system", e);
            throw new DisworkException(e);
        }
        
        FileUtil.deleteRecursively(config.getTempDirectory());
    }

    /* *** methods about statistics *** */
    
    protected Long getTotalUptime() {
        Long currentTime = System.currentTimeMillis();
        Long sessionUptime = currentTime - sessionStartTime;
        Long totalUptime = config.getTotalUptime() + sessionUptime;
        return totalUptime;
    }
    
    /**
     * return a ratio of the total uptime on the time since the first demon
     * run. For exemple, if daemon was up 2 hours and was installed 4 hours ago
     * this method return 0.5. 
     * @return
     */
    protected Double getUptimeRatio() {
        Double uptimeRatio = (double) getTotalUptime().longValue()
                           / ((double) System.currentTimeMillis()
                             - (double) config.getFirstRunTime().longValue());
        return uptimeRatio;
    }

    public Map<String, String> getLocalStats() throws DisworkException {

        // TODO 20100615 bleny add RAM size and HDD capacities to hardinfos

        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMaximumFractionDigits(2);
        
        Map<String, String> result = new HashMap<String, String>();
        result.put("total_uptime", getTotalUptime().toString());
        result.put("uptime_ratio", numberFormat.format(getUptimeRatio()));

        result.put("jobs_done", config.getNumberOfJobsDone().toString());
        result.put("jobs_submitted", config.getNumberOfJobsSubmitted().toString());
        result.put("worked_time", config.getWorkedTime().toString());
        
        if (config.getNumberOfJobsSubmitted() != 0) {
            Double jobsRatio = (double) (config.getNumberOfJobsDone() / config.getNumberOfJobsSubmitted());
            result.put("jobs_ratio", numberFormat.format(jobsRatio));
        }

        Double karma = ((config.getNumberOfJobsDone() - config.getNumberOfJobsSubmitted()) * getUptimeRatio());
        result.put("karma", numberFormat.format(karma));

        OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
        result.put(os.getName(), "1");
        result.put(os.getArch(), "1");
        result.put("workers", config.getNumberOfWorkers().toString());        

        return result;
    }

    /** a file that contains global stats about diswork */
    protected static final String GLOBAL_STATS_PATH = "/proc/globalstats";
    
    protected Map<String, String> updateGlobalStats() throws DisworkException {
        try {
            log.info("global stats file doesn't exists, creating one");

            Map<String, Long> stats = new HashMap<String, Long>();
            Map<String, String> result = new HashMap<String, String>();
            
            List<String> homeDirs = fileSystem.readDirectory(HOME);
            for (String homeDir : homeDirs) {
                String hardInfoPath = HOME + "/" + homeDir + "/" + HARDINFO_PATH;
                if (fileSystem.exists(hardInfoPath)) {
                    BufferedReader in = new BufferedReader(
                                        new InputStreamReader(
                                        fileSystem.read(hardInfoPath)));
                    String line;
                    while ((line = in.readLine()) != null) {
                        if (!line.equals("")) {
                            log.debug("reading line " + line);
                            String[] keyValue = line.split("\t");
                            String key = keyValue[0];
                            Long value = Long.parseLong(keyValue[1]);
                            if (!stats.containsKey(key)) {
                                stats.put(key, 0L);
                            }
                            stats.put(key, stats.get(key) + value);
                        }
                    }
                }
            }

            Long currentTime = System.currentTimeMillis();
            stats.put("date", currentTime);
    
            // write the result
            String statsFileContent = "";
            for (String key : stats.keySet()) {
                result.put(key, stats.get(key).toString());
                statsFileContent += key + "\t" + stats.get(key) + "\n";
            }
    
            log.debug("writing stats file " + statsFileContent);
            fileSystem.write(GLOBAL_STATS_PATH, IOUtils.toInputStream(statsFileContent));

            return result;
        } catch (DisworkFileSystemException e) {
            log.error("file system error ", e);
            throw new DisworkException("file system error ", e);
        } catch (IOException e) {
            log.error("can't write hardware infos ", e);
            throw new DisworkException("can't read hardware infos ", e);
        }
    }
    
    /** get infos on hardware available on the global Diswork system
     * 
     * @return
     * @throws DisworkException 
     */
    public Map<String, String> getGlobalStats() throws DisworkException {
        final int timeBeforeGlobalStatsAreObsolete = 1 * 60 * 60 * 1000;

        // in this file, key and values are split with \t and entries are split
        // with \n, if a global-stats file is found, read it. If not, create
        // it and date it. If file is obsolete read it and delete it
        
        try {
            Map<String, String> result = new HashMap<String, String>();

            if (fileSystem.exists(GLOBAL_STATS_PATH)) {
                log.debug("global stats file found, reading");
                BufferedReader globalStats = new BufferedReader(
                                             new InputStreamReader(
                                             fileSystem.read(GLOBAL_STATS_PATH)));
                String line;
                while ((line = globalStats.readLine()) != null) {
                    if (!line.equals("")) {
                        String[] keyValue = line.split("\t");
                        result.put(keyValue[0], keyValue[1]);
                    }
                }
                
                // delete the file if it's too old
                Long currentTime = System.currentTimeMillis();
                Long statsTime = Long.parseLong(result.get("date"));
                if (currentTime - statsTime > timeBeforeGlobalStatsAreObsolete) {
                    log.info("deleting global stats file");
                    fileSystem.delete(GLOBAL_STATS_PATH);
                }
            } else {
                result = updateGlobalStats();
            }
            
            return result;
        } catch (DisworkFileSystemException e) {
            log.error("file system error ", e);
            throw new DisworkException("file system error ", e);
        } catch (IOException e) {
            log.error("can't read hardware infos ", e);
            throw new DisworkException("can't read hardware infos ", e);
        }
    }
    
    public void activeNoActivityStrategy() throws DisworkException {
        if (workers == null) {
            log.warn("trying to change activity while working is disabled");
        } else {
            workers.activeNoActivityStrategy();
        }
    }

    public void activeUnlimitedActivityStrategy() throws DisworkException {
        if (workers == null) {
            log.warn("trying to change activity while working is disabled");
        } else {
            workers.activeUnlimitedActivityStrategy();
        }
    }

    public void activeLimitedActivityStrategy() throws DisworkException {
        if (workers == null) {
            log.warn("trying to change activity while working is disabled");
        } else {
            workers.activeLimitedActivityStrategy();
        }
    }

    public void activeScheduledActivityStrategy() throws DisworkException {
        if (workers == null) {
            log.warn("trying to change activity while working is disabled");
        } else {
            workers.activeScheduledActivityStrategy();
        }
    }
    
    /**
     * 
     * @return null if working is disabled
     * @throws DisworkException
     */
    public List<JobDescription> getAllWorkersCurrentJobs() throws DisworkException {
        if (workers != null) {
            return workers.getAllWorkersCurrentJobs();
        }
        return null;
    }
}