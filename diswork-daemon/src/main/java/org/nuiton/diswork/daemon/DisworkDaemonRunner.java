/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;

/**
 * This class is used to run diswork daemon. It can be used to run a deamon
 * using a simple command-line.
 * 
 * This class can be used to run a diswork daemon as a service on the OS.
 * Thus, it will be started when the machine boot and stopped just before the
 * computer is shut down. To install diswork as a daemon, see commons-daemon
 * documentation.
 * 
 * Usage is 
 * 
 * <code>
 * java DisworkDaemonRunner [bootstrap_ip bootstrap_port]
 * </code>
 * 
 * if no parameter provided, diswork will start and boot alone. If ip and port
 * are provided, diswork will connect to this other node.
 *  
 * @link http://commons.apache.org/daemon/
 * 
 * @author bleny
 */
public class DisworkDaemonRunner implements Daemon {
    
    private static final Log log = LogFactory.getLog(DisworkDaemonRunner.class);

    protected static DisworkConfig config;
    
    protected static DisworkDaemon daemon;

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
    	DisworkDaemonRunner instance = new DisworkDaemonRunner();
    	instance.init(args);
        instance.start();
    }
    
    /**
     * set <code>config</code> according args
     * @param args
     * @throws DisworkException
     */
    protected void init(String[] args) throws DisworkException {
        config = new DisworkConfig();
            config.setFileSystemConfig(new DisworkFileSystemConfig());
        if (args.length == 2) {
            config.setFileSystemConfig(new DisworkFileSystemConfig(args[0],
                            Integer.parseInt(args[1])));
        }
        config.setActivityStrategy("unlimited");
    }

    // constructor need to have empty params
    public DisworkDaemonRunner() {}
    
	@Override
	public void destroy() {
		// nothing to do
	}

	@Override
	public void init(DaemonContext context) throws Exception {
		init(context.getArguments());
	}

	@Override
	public void start() throws Exception {
        daemon = new DisworkDaemon(config);
	}

	@Override
	public void stop() throws Exception {
		daemon.close();
	}
}
