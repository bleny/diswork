/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronExpression;

/**
 * This interface introduce the concept of "activity strategy" in
 * diswork, an activity strategy answer to "Can i run a job now ?".
 * 
 * This provide a simple method that returns true if Diswork can run a job
 * and take hardware resources. If false is returned, it means that resources
 * are not available or we want diswork not to consume more resources.
 * 
 * This interface come with 4 implementations. Two first are very simple,
 * one is used when diswork is "shut down", it always returns false. The
 * second is used when diswork is in "free-wheel" and take as much resources
 * as jobs need (always return true).
 * 
 * LimitedActivity implementation is user-activity aware and returns true
 * only if it seems that the user incompletely use the resources of 
 * his computer. This implementation is based on the a load-average
 * computation of the computer. If load-average is low, computer is 
 * idling : it returns true and diswork will use remaining resources.
 * If load-average is high, false is returned and diswork don't make
 * the computer over-loaded.
 * 
 * ScheduledActivity implementation make diswork run jobs only at
 * fixed period of time. An user can configure it to make
 * diswork run job only at night are during the week-end, for example.
 * 
 * @author bleny
 *
 */
public interface ActivityStrategy {
    
    public enum ActivityStrategies {
        NONE ("none"),
        UNLIMITED ("unlimited"),
        LIMITED ("limited"),
        SCHEDULED ("scheduled");
        
        /** those labels will be used in the config file */        
        public String label;
        
        ActivityStrategies(String name) {
            this.label = name;
        }

        /** return the enumeration element given its label */
        static ActivityStrategies valueOfLabel(String label) { 
            for (ActivityStrategies aStrategy : values()) {
                 if (aStrategy.label ==  label)  {
                     return aStrategy;
                 }
            }
            throw new IllegalArgumentException("wrong strategy label : " + label);
        }

        /** factory method, instantiate the good strategy given its label */
        static ActivityStrategy getNewInstance(DisworkConfig config, String label) {
            return getNewInstance(config, valueOfLabel(label));
        }

        static ActivityStrategy getNewInstance(DisworkConfig config, ActivityStrategies strat) {
            ActivityStrategy result = null;           
            switch (strat) {
                case NONE:
                    result = new NoActivity();
                    break;
                case UNLIMITED:
                    result = new UnlimitedActivity();
                    break;
                case LIMITED:
                    result = new LimitedActivity();
                    break;
                case SCHEDULED:
                    result = new ScheduledActivity(config);
                    break;
            }
            return result;
        }
    }
    
    /** use this strategy to never run a job */
    public static class NoActivity implements ActivityStrategy {

        @Override
        public boolean canWork() {
            return false;
        }

        @Override
        public long timeBeforeNextUpdate() {
            // this will never change
            return -1;
        }

        @Override
        public String toString() {
            return "no activity";
        }
    }
    
    /** use this strategy to always run a job */
    public static class UnlimitedActivity implements ActivityStrategy {
        @Override
        public boolean canWork() {
            return true;
        }

        @Override
        public long timeBeforeNextUpdate() {
            // this will never change
            return -1;
        }

        @Override
        public String toString() {
            return "unlimited activity";
        }
    }
    
    /** use this strategy to run a job only if computer is idling */
    public static class LimitedActivity implements ActivityStrategy {

        private static final Log log = LogFactory.getLog(LimitedActivity.class);

        /** give three load averages, update them every five minutes */
        protected class LoadAverageMonitoring extends Thread {

            OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();

            Double loadAverageNow = 1.0;
            Double loadAverage5MinutesBefore = 1.0;
            Double loadAverage10MinutesBefore = 1.0;

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5 * 60 * 1000); // 5 min
                    } catch (InterruptedException e) {
                        log.warn("load average monitoring interrupted", e);
                    }
                    loadAverage10MinutesBefore = loadAverage5MinutesBefore;
                    loadAverage5MinutesBefore = loadAverageNow;
                    loadAverageNow = os.getSystemLoadAverage();
                    log.info("load averages : " + loadAverageNow + " " +
                                               loadAverage5MinutesBefore + " "
                                                + loadAverage10MinutesBefore);
                }
            }
        }

        LoadAverageMonitoring monitoring;

        public LimitedActivity() {
            monitoring = new LoadAverageMonitoring();
            monitoring.start();
        }
        
        @Override
        public boolean canWork() {
            boolean canWork = monitoring.loadAverageNow < 1.0
                           && monitoring.loadAverage5MinutesBefore < 1.0
                           && monitoring.loadAverage10MinutesBefore < 1.0;
            return canWork; 
        }

        @Override
        public long timeBeforeNextUpdate() {
            return 5 * 60 * 1000;
        }

        @Override
        public String toString() {
            return "limited activity";
        }
    }
    
    /** use this strategy to run a job only at fixed times of the week */
    public static class ScheduledActivity implements ActivityStrategy {

        protected DisworkConfig config;

        protected long timeBeforeNextUpdate = 1000L; // 1 second is the minimum

        protected ScheduledActivity(DisworkConfig config) {
            this.config = config;
        }
        
        @Override
        public boolean canWork() throws DisworkException {
            // look in the schedule file if at least one of all the expression
            // match the current date
            Date currentDate = new Date();
            boolean result = false;
            
            // during the reading of the file, we will evaluate the next time
            // the canWork method will change, we set it to (now + 1 hour)
            // to begin and we will overwrite it if a preceding date is found 
            Date nextChange = new Date(System.currentTimeMillis() + 60 * 60 * 1000);
            
            // browse all cron expressions found in the config file
            for (CronExpression pattern : config.getSchedule()) {
                // canWork is true if current expression matches current date
                // or if canWork was already true due to a previous expression
                result = result || pattern.isSatisfiedBy(currentDate);
                
                // for the current expression, look for the date when it will
                // be invalid or valid and update nextChange
                // Thus, nextChange will be the earliest date when we will have
                // to check if canWork change
                Date aDate = pattern.getNextInvalidTimeAfter(currentDate);
                if (aDate.before(nextChange)) {
                    nextChange = aDate;
                }
                aDate = pattern.getNextValidTimeAfter(currentDate);
                if (aDate.before(nextChange)) {
                    nextChange = aDate;
                }
            }
            timeBeforeNextUpdate = nextChange.getTime() - currentDate.getTime();
            return result;
        }

        @Override
        public long timeBeforeNextUpdate() {
            return timeBeforeNextUpdate;
        }
        
        @Override
        public String toString() {
            return "scheduled activity";
        }
    }

    /**
     * @return true if a job can be run now
     */
    boolean canWork() throws DisworkException;

    /**
     * @return time to wait before next update, -1 is never (wait definitly)
     */
    long timeBeforeNextUpdate();
}