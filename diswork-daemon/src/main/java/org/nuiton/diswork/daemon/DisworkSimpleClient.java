/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;

/**
 * 
 * @author bleny
 */
public class DisworkSimpleClient {

    private static final Log log = LogFactory.getLog(DisworkSimpleClient.class);

    static DisworkDaemon daemon;
    
    public static void userPrompt() throws Exception {

        //  open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String userEntry = "";
        while (!userEntry.equals("quit")) {
            System.out.print("diswork: ");
            userEntry = br.readLine();
            
            if ("stats".equals(userEntry)) {
                Map<String, String> stats = daemon.getGlobalStats();
                for (String key : stats.keySet()) {
                    System.out.println(key + " => " + stats.get(key));
                }
            } else if (!"quit".equals(userEntry)) {
                JobDescription job = new JobDescription();
                job.setStandardOutput("output.txt");
                job.addOutput("output.txt");
                job.setCommandLine(userEntry);
                daemon.submitJob(job);
                
                while(! daemon.isFinished(job)) {
                    Thread.sleep(5 * 1000);
                    System.out.print(".");
                }
                
                System.out.println(IOUtils.toString(daemon.getResults(job).get("output.txt")));
            }
        }
    }
    
    public static void isisSubmit() throws Exception {
        File isis = new File("/tmp/isis/isis-fish-3.3.0.4-dev3069-bin.zip");
        // File isis = new File("/tmp/isis/fake-app-1.0.zip");
    	
        InputStream in = new FileInputStream(isis);
        log.info("isis is " + in.available() + " bytes");
        daemon.submitApplication("isis-fish", "3.3.0.4-dev3069", in);
        
        JobDescription jobDescription = new JobDescription();
        jobDescription.setJobName("Mon Job Isis");
        jobDescription.setApplication("isis-fish", "3.3.0.4-dev3069");
        
        File input = new File("/tmp/isis/isisfish-simulation-ica10ans.zip");
        jobDescription.addInput("isisfish-simulation-ica10ans.zip", new FileInputStream(input));
        jobDescription.addOutput("isisfish-simulation-ica10ans-result.zip");
        
        jobDescription.setCommandLine(
        		"%java -Xmx2500M -jar isis-fish-3.3.0.4-SNAPSHOT/isis-fish-3.3.0.4-SNAPSHOT.jar"
              + " --option launch.ui false"
              +	" --option perform.vcsupdate false"
              + " --option perform.migration false"
              + " --option perform.cron false"
              + " --simulateRemotelly my_isis_job isisfish-simulation-ica10ans.zip" 
              + " isisfish-simulation-ica10ans-result.zip");
        
        jobDescription.setStandardOutput("output.txt");
        jobDescription.addOutput("output.txt");
        
        daemon.submitJob(jobDescription);
        
        while (! daemon.isFinished(jobDescription)) {
            Thread.sleep(15 * 1000);
            System.out.print(".");
        }

        System.out.println("getting result");
        File result = new File("/tmp/isis/sim_test-gdg-3.2-3.3-result.zip");
        IOUtils.copy(daemon.getResults(jobDescription).get("isisfish-simulation-ica10ans-result.zip"), new FileOutputStream(result));

        System.out.println("getting output");
        result = new File("/tmp/isis/output.txt");
        IOUtils.copy(daemon.getResults(jobDescription).get("output.txt"), new FileOutputStream(result));
        
        System.out.println("end");
    }
    
    /**
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        DisworkConfig config = new DisworkConfig();
        Integer port = Integer.parseInt(args[1]);
        System.out.println("port = " + port);
        config.setFileSystemConfig(new DisworkFileSystemConfig(args[0], port));
        config.setActivityStrategy("none");
        config.setUsedPort(30000);
        config.setStartHttpFrontend(false);
        daemon = new DisworkDaemon(config);

        userPrompt();
        //isisSubmit();
    }
}
