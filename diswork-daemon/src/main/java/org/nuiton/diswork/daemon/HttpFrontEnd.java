/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;

/**
 * The HttpFrontEnd boot an embedded http server on same machine of the daemon.
 * 
 * A user can open a web-browser to read the single page that is available.
 * This page shows statistics and status of the different daemon components.
 * 
 * @author bleny
 */
public class HttpFrontEnd {

    private static final Log log = LogFactory.getLog(HttpFrontEnd.class);

    /** config provides directive about the http server */
    protected DisworkConfig config; 

    /** embedded http server */
    protected Server server;

    /** daemon will permit to retrieve stats */
    protected DisworkDaemon daemon;

    public HttpFrontEnd(DisworkConfig config, DisworkDaemon daemon)
                                                     throws DisworkException {
        this.config = config;
        this.daemon = daemon;

        if (config.getStartHttpFrontend()) {
            start();
        }
    }

    /** lazy instanciation of {@link #server} */
    protected void initServer() {
        log.info("web server use port " + config.getHttpFrontendPort());
        server = new Server(config.getHttpFrontendPort());
        Context root = new Context(server, "/", Context.NO_SESSIONS);
        root.addServlet(new ServletHolder(new MainServlet()), "/");
    }

    /** start the server, the page will no longer be available for web-browser */
    public void start() throws DisworkException {
        if (server == null) {
            initServer();
        }
        log.info("starting web front-end");
        try {
            server.start();
        } catch (Exception e) {
            log.error("error while booting http server", e);
            throw new DisworkException("error while booting http server", e);
        }        
    }
    
    /** stop the server, the page will no longer be available for web-browser */
    public void stop() throws DisworkException {
        log.info("stopping web front-end");
        try {
            if (server != null) {
                server.stop();
            } // else server has never been started 
        } catch (Exception e) {
            log.error("error while stopping server", e);
            throw new DisworkException("error while stopping server", e);
        }
    }

    /** this servlet send a static html page that shows current status and stats */
    public class MainServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException {
            
            log.info("page request");
            
            String pageContent = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n"
                               + "<html>\n"
                               + "<h1>Diswork Node</h1>"               + "\n\n"
                               + "<h2>Submitted jobs</h2>"             + "\n\n"
                               + "<table>"                               + "\n"
                               + "  <tr>"                                + "\n"
                               + "    <th>Name</th>"                     + "\n"
                               + "    <th>Application</th>"              + "\n"
                               + "    <th>Status</th>"                   + "\n"
                               + "  </tr>"                               + "\n";            
            
            List<JobDescription> jobs;
            try {
                jobs = daemon.getAllJobs();
            } catch (DisworkException e) {
                log.error("error while retrieving local stats", e);
                throw new ServletException("error while retrieving local stats", e);
            }
            
            if (jobs.isEmpty()) {
                pageContent += "  <tr>\n"
                            +  "    <td colspan=\"3\"><em>no job submitted</em></td>\n"
                            +  "  </tr>\n";
            } else {
                for (JobDescription job : jobs) {

                    String status = "unknown";
                    try {
                        if (daemon.isFinished(job)) {
                            if (daemon.isSuccessful(job)) {
                                status = "done";
                            } else {
                                status = "failed";
                            }
                        } else {
                            if (daemon.isStarted(job)) {
                                status = "started";
                            } else {
                                status = "waiting";
                            }
                        }
                    } catch (DisworkException e) {
                        log.error("unable to read job status", e);
                        throw new ServletException("unable to read job status", e);
                    }
                        
                        
                    pageContent += "  <tr>\n"
                                +  "    <td>" + job.getJobName() + "</td>\n"
                                +  "    <td>" + job.getApplicationName() + "</td>\n"
                                +  "    <td>" + status + "</td>\n"
                                +  "  </tr>\n";
                }
            }
            
            pageContent += "</table>\n\n";
            
            
            

            pageContent += "<h2>Currently running jobs</h2>"           + "\n\n"
                        + "<table>"                               + "\n"
                        + "  <tr>"                                + "\n"
                        + "    <th>Name</th>"                     + "\n"
                        + "    <th>Application</th>"              + "\n"
                        + "  </tr>"                               + "\n";            
            
            try {
                jobs = daemon.getAllWorkersCurrentJobs();
                } catch (DisworkException e) {
                log.error("error while retrieving local stats", e);
                throw new ServletException("error while retrieving local stats", e);
            }
            
            if (jobs.isEmpty()) {
                pageContent += "  <tr>\n"
                            +  "    <td colspan=\"2\"><em>working is disabled</em></td>\n"
                            +  "  </tr>\n";
            } else {
                for (JobDescription job : jobs) {  
                     
                    if (job == null) {
                        pageContent += "  <tr>\n"
                            +  "    <td colspan=\"2\"><em>no job</em></td>\n"
                            +  "  </tr>\n";
                    } else {
                         pageContent += "  <tr>\n"
                                     +  "    <td>" + job.getJobName() + "</td>\n"
                                     +  "    <td>" + job.getApplicationName() + "</td>\n"
                                     +  "  </tr>\n";
                    }
                }
            }
            pageContent += "</table>\n\n";

            
            pageContent += "<h2>Diswork statistics</h2>\n\n";
            Map<String, String> stats;
            try {
                stats = daemon.getLocalStats();
            } catch (DisworkException e) {
                log.error("error while retrieving local stats", e);
                throw new ServletException("error while retrieving local stats", e);
            }

            pageContent += "<h3>Local statistics</h3>"                   + "\n"
                         + "<table>"                                     + "\n"
                         + "  <tr>"                                      + "\n"
                         + "    <th>Key</th>"                            + "\n"
                         + "    <th>Value</th>"                          + "\n"
                         + "  </tr>"                                     + "\n";
            for (String stat : stats.keySet()) {
                pageContent += "  <tr>"                                  + "\n"
                             + "    <td>"                                + "\n"
                             + "      " + stat                           + "\n"
                             + "    </td>"                               + "\n"
                             + "    <td>"                                + "\n"
                             + "      " + stats.get(stat)                + "\n"
                             + "    </td>"                               + "\n"
                             + "  </tr>"                                 + "\n";
            }

            pageContent += "</table>"                                    + "\n"
                         + "<h3>Global statistics</h3>"                  + "\n"
                         + "<table>"                                     + "\n"
                         + "  <tr>"                                      + "\n"
                         + "    <th>Key</th>"                            + "\n"
                         + "    <th>Value</th>"                          + "\n"
                         + "  </tr>"                                     + "\n";

            try {
                stats = daemon.getGlobalStats();
            } catch (DisworkException e) {
                log.error("error while retrieving local stats", e);
                throw new ServletException("error while retrieving local stats", e);
            }
            for (String stat : stats.keySet()) {
                pageContent += "  <tr>"                                  + "\n"
                             + "    <td>"                                + "\n"
                             + "      " + stat                           + "\n"
                             + "    </td>"                               + "\n"
                             + "    <td>"                                + "\n"
                             + "      " + stats.get(stat)                + "\n"
                             + "    </td>"                               + "\n"
                             + "  </tr>"                                 + "\n";
            }
            
            pageContent += "</table>\n<html>"                            + "\n";

            resp.getWriter().write(pageContent);
        }
    }

}
