/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.util.ApplicationConfig;
import org.quartz.CronExpression;

/**
 * 
 * <dl>
 *   <dt>diswork.workers_number</dt>
 *     <dd>The number of jobs the daemon can run at the same time. Should be set
 *         to a value that consider the number of processors, core and RAM
 *         available. By default, it's the number processors available to the
 *         JVM. Set it to 0 will disable job-processing.</dd>
 *   <dt>diswork.temp_directory</dt>
 *     <dd>Diswork need a temporary directory to store temporary data for each
 *         jobs. By default, the temp dir of the OS is used (ie "/tmp/diswork"
 *         under Linux).</dd>
 *   <dt>diswork.activity_strategy</dt>
 *     <dd>This is the way the daemon is started, different values are
 *         available:
 *         <dl>
 *           <dt>none</dt>
 *             <dd>never try to do a job</dd>
 *           <dt>unlimited</dt>
 *             <dd>always try to job, whatever the cost</dd>
 *           <dt>limited</dt>
 *             <dd>run a job only if hardware resources are available (based
 *                 on the system load average)</dd>
 *           <dt>scheduled</dt>
 *             <dd>run a job only at fixed time of the week (for example, 
 *                 nights, week-end, etc.). It needs to define a pattern.</dd>
 *         </dl>
 *     </dd>
 *   <dt>diswork.tokens_file</dt>
 *     <dd>a path to a file containing token-names and the replacement strings
 *         this is a property file</dd>
 *   <dt>diswork.job_looks_wait_time</dt>
 *      <dd>the number of seconds to wait between two looks for a job.</dd>
 * </dl>
 * 
 * For the http front-end :
 * <dl>
 *   <dt>diswork.http_front_end.start</dt>
 *     <dd>"true" and the front end will start at diswork boot. Note that
 *         the front-end can be started later even if this config directive is
 *         at "false"</dd>
 *   <dt>diswork.http_front_end.port</dt>
 *     <dd>the port to use for HTTP server. It means that the browser while
 *         have to use this port to get connected to the front-end. Default
 *         port is 8080.</dd>
 * </dl>
 * 
 * @author bleny
 */
public class DisworkConfig extends ApplicationConfig {

    private static final Log log = LogFactory.getLog(DisworkConfig.class);

    protected DisworkFileSystemConfig fileSystemConfig;


    /** TOKENS are piece of string you can use for writing command lines
     * some tokens are defined by default, some others can be defined in a
     * properties file
     *
     * @see #initTokens()
     */
    protected Map<String, String> tokens;

    public DisworkConfig() {
        setConfigFileName("diswork.config");
        
        Integer availableProcessors = ManagementFactory
                                    . getOperatingSystemMXBean()
                                    . getAvailableProcessors();
        setDefaultOption("diswork.workers_number", availableProcessors.toString());
        
        setDefaultOption("diswork.temp_directory",
                System.getProperty("java.io.tmpdir")
                                    + File.separator + "diswork");

        setDefaultOption("diswork.activity_strategy", "unlimited");
        setDefaultOption("diswork.job_looks_wait_time", "60"); // 1 minute
        
        setOption("diswork.http_front_end.start", "true");
        setOption("diswork.http_front_end.port", "8080");
        
        setFileSystemConfig(new DisworkFileSystemConfig());
        
        
        

        
        // init data never initialized
        setDefaultOption("diswork.total_uptime", "0");
        setDefaultOption("diswork.number_of_jobs_done", "0");
        setDefaultOption("diswork.number_of_jobs_submitted", "0");
        setDefaultOption("diswork.worked_time", "0");
    }


    /**
     *
     * @return null if no file specified
     */
    public File getTokensFile() {
        String path = getOption("diswork.tokens_file");
        if (path == null) {
            return null;
        } else {
            File file = new File(path);
            return file;
        }
    }
    
    public void setTokensFile(String tokensFilePath) {
        setOption("diswork.tokens_file", tokensFilePath);
    }
    
    /**
     * Read the tokens file if one is given in the config and merge the content
     * of this file into {@link #tokens} 
     * @throws BadConfigurationException  if token file doesn't exists
     * @throws LocalFileException if token file can't be read
     */
    protected void initTokens() throws BadConfigurationException, LocalFileException  {
        tokens = new HashMap<String, String>();

        String java = // full java path
                      System.getProperty("java.home") +
                      File.separator + "bin" + File.separator + "java"
                      // setting some system properties
                    + " -Duser.dir=%tmp"
                    + " -Duser.home=%tmp"
                    + " -Djava.io.tmpdir=%tmp"
                    ;
        tokens.put("%java", java);
        tokens.put("%sep", File.separator);
        
        File tokensFile = getTokensFile();
        if (tokensFile != null) {
            InputStream tokensStream = null;
            try {
                tokensStream = new FileInputStream(tokensFile);
                Properties userTokens = new Properties();
                userTokens.load(tokensStream);
                for (String token : userTokens.stringPropertyNames()) {
                    String replacement = userTokens.getProperty(token);
                    // prevent java to replace \t by a tab in C:\temp
                    String escapedReplacement = StringEscapeUtils.escapeJava
                                    (StringEscapeUtils.escapeJava(replacement));
                        
                    log.debug("adding token " + token + " → " + escapedReplacement);
                    tokens.put(token, escapedReplacement);
                }
            } catch (FileNotFoundException e) {
                log.warn("tokens file not found, 0 tokens loaded", e);
                throw new BadConfigurationException("tokens file not found,"
                                         + "no token loaded", e);
            } catch (IOException e) {
                log.error("can't read tokens file", e);
                throw new LocalFileException("can't read tokens file", e);
            } finally {
                IOUtils.closeQuietly(tokensStream);
            }
        }
    }



    protected String applyTokensRecursively(String commandLine) {
        String result = commandLine;
        for (String token : tokens.keySet()) {
            result = result.replaceAll(token, tokens.get(token));
        }
        if (result.equals(commandLine)) {
            return result;            
        } else {
            // a token has been applied, re-do a pass
            return applyTokensRecursively(result);
        }
    }

    protected String parseCommandLine(String commandLine, String tempDir)
                      throws BadConfigurationException, LocalFileException {
        if (tokens == null) {
            initTokens();
        }

        if (tempDir != null) {
            tokens.put("%tmp", tempDir);
        }

        return applyTokensRecursively(commandLine);
    }






    protected List<CronExpression> schedule;

    /**
     * 
     * @return null if no path for a file have been specified
     * @throws BadConfigurationException  if schedule file doesn't exists
     * @throws LocalFileException if schedule file can't be read
     */
    protected List<CronExpression> getSchedule() throws BadConfigurationException, LocalFileException {
        // lazy instanciation of schedule
        if (schedule == null) {
            String path = getOption("diswork.schedule_file");
            if (path != null) {
                File file = new File(path);
                schedule = new ArrayList<CronExpression>();
                BufferedReader in = null;
                try {
                    Reader reader = new FileReader(file);
                    in = new BufferedReader(reader);
                    String line;
                    while ((line = in.readLine()) != null) {
                        if (!line.startsWith("#") && !"".equals(line)) {
                            try {
                                schedule.add(new CronExpression(line));
                            } catch (ParseException e) {
                                log.warn("failed to parse " + line + " : line ignored");
                            }
                        }
                    }
                }  catch (FileNotFoundException e) {
                    log.error("schedule file doesn't exists", e);
                    throw new BadConfigurationException("schedule file doesn't exists", e);
                } catch (IOException e) {
                    log.error(e);
                    throw new LocalFileException("can't read schedule file", e);
                } finally {
                    IOUtils.closeQuietly(in);
                }
            } else {
                throw new BadConfigurationException("schedule file has not been specified");
            }
        }
        return schedule;
    }

    public void setSheduleFile(String path) {
        setOption("diswork.schedule_file", path);
    }



    
    /* ** those config data are not set by the user but computed by daemon ** */
    
    // FIXME 20100607 bleny its not config data, it should be moved to a persistent file

    protected void save() {
        saveForUser();
    }
    
    protected String getOwnerId() {
        return getOption("diswork.owner");
    }

    protected void setOwnerId(String ownerId) {
        setOption("diswork.owner", ownerId);
        save();
    }

    protected Long getTotalUptime() {
        String upTime = getOption("diswork.total_uptime");
        return Long.parseLong(upTime);
    }

    protected void setTotalUptime(Long upTime) {
        setOption("diswork.total_uptime", upTime.toString());
        save();
    }

    protected void setFirstRunTime(Long time) {
        setOption("diswork.first_run_time", time.toString());
        save();
    }

    protected Long getFirstRunTime() {
        String firstRunTime = getOption("diswork.first_run_time");
        return Long.parseLong(firstRunTime);
    }

    protected void addOneJobDone() {
        Integer newValue = getNumberOfJobsDone() + 1;
        setOption("diswork.number_of_jobs_done", newValue.toString());
        save();
    }
    
    protected Integer getNumberOfJobsDone() {
        return getOptionAsInt("diswork.number_of_jobs_done");
    }

    protected void addOneJobSubmitted() {
        Integer newValue = getNumberOfJobsDone() + 1;
        setOption("diswork.number_of_jobs_submitted", newValue.toString());
        save();        
    }
    
    protected Integer getNumberOfJobsSubmitted() {
        return getOptionAsInt("diswork.number_of_jobs_submitted");
    }

    protected void addWorkedTime(Long workedTime) {
        Long newValue = getWorkedTime() + workedTime;
        setOption("diswork.worked_time", newValue.toString());
        save();
    }

    protected Long getWorkedTime() {
        Long workedTime = Long.parseLong(getOption("diswork.worked_time"));
        return workedTime;
    }



    /* ** trivial applicationConfig setters and getters ** */

    public String getTempDirectory() {
        return getOption("diswork.temp_directory");
    }

    public String getBootstrapIp() {
        return fileSystemConfig.getBootstrapIp();
    }

    public Integer getBootstrapPort() {
        return fileSystemConfig.getBootstrapPort();
    }

    public Integer getUsedPort() {
        return fileSystemConfig.getUsedPort();
    }

    public void setBootstrapIp(String ip) {
        fileSystemConfig.setBootstrapIp(ip);
    }

    public void setBootstrapPort(Integer port) {
        fileSystemConfig.setBootstrapPort(port);
    }

    public void setUsedPort(Integer port) {
        fileSystemConfig.setUsedPort(port);
    }

    public DisworkFileSystemConfig getFileSystemConfig() {
        return fileSystemConfig;
    }

    public void setFileSystemConfig(DisworkFileSystemConfig fileSystemConfig) {
        this.fileSystemConfig = fileSystemConfig;
    }

    public Integer getNumberOfWorkers() {
        return getOptionAsInt("diswork.workers_number");
    }

    public void setNumberOfWorkers(Integer number) {
        setOption("diswork.workers_number", number.toString());
    }

    public String getActivityStrategy() {
        return getOption("diswork.activity_strategy");
    }

    public void setActivityStrategy(String activityStrategyName) {
        setOption("diswork.activity_strategy", activityStrategyName);
    }
    
    public Boolean getStartHttpFrontend() {
        return getOptionAsBoolean("diswork.http_front_end.start");
    }

    public void setStartHttpFrontend(Boolean startHttpFrontend) {
        setOption("diswork.http_front_end.start", startHttpFrontend.toString());
    }

    public Integer getHttpFrontendPort() {
        return getOptionAsInt("diswork.http_front_end.port");
    }

    public void setHttpFrontendPort(Integer httpFrontendPort) {
        setOption("diswork.http_front_end.port", httpFrontendPort.toString());
    }

    /** number of seconds to wait between two look for a jobs
       *  @return a number of seconds
       */
    public int getJobLooksWaitTime() {
        return getOptionAsInt("diswork.job_looks_wait_time");
    }
    
    public void setJobLooksWaitTime(Integer seconds) {
        setOption("diswork.job_looks_wait_time", seconds.toString());
    }
}
