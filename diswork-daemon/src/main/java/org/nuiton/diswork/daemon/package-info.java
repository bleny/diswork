/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 *  
 * {@link org.nuiton.diswork.daemon.DisworkDaemon} is the class the that
 * provides all the code needed to submit a job, monitor it, and download
 * the results. 
 * 
 * {@link org.nuiton.diswork.daemon.WorkersManager} is the component run by the
 * daemon. It's purpose is to look for the jobs proposed on the global system.
 * Its role is to find jobs, execute the process, publish the status and the
 * results.
 * 
 * For user responsible of the machine that will host the daemon,
 * {@link org.nuiton.diswork.daemon.HttpFrontEnd} provides a web-based
 * interface to monitor the status of the daemon and obtain local and global
 * statistics about diswork global system.
 * 
 * {@link org.nuiton.diswork.daemon.ActivityStrategy} interface and its
 * realizations provides means for the daemon administrator to make the daemon
 * more or less hardware-resources consumer.
 * 
 * {@link org.nuiton.diswork.daemon.DisworkDaemonRunner} is a facility class
 * that permit to run simply a daemon.
 */

package org.nuiton.diswork.daemon;