/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

/**
 * This exception occurs when a job or his description is not valid. It may
 * be the code calling DisworkDaemon as client that may be responsible for
 * such an error. 
 * 
 * @author bleny
 */
public class BadJobException extends DisworkException {

    private static final long serialVersionUID = 1L;

    public BadJobException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadJobException(String message) {
        super(message);
    }

    public BadJobException(Throwable cause) {
        super(cause);
    }

}
