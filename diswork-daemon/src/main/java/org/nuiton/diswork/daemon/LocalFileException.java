/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

/**
 * A raise of this exception is due to an error on the local file system.
 * Maybe it's not writable (rights ? not enough space ?) or readable by
 * the daemon. 
 * 
 * @author bleny
 */
public class LocalFileException extends DisworkException {

    private static final long serialVersionUID = 1L;

    public LocalFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public LocalFileException(String message) {
        super(message);
    }

    public LocalFileException(Throwable cause) {
        super(cause);
    }

}
