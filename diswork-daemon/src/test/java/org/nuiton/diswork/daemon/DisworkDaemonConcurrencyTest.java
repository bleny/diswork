/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

/**
 * This test is the same as {@link DisworkDaemonTest} except that
 * the single node has more than one worker.
 *
 * This tests shows that having multiple workers should not raise
 * any problem like throwing concurrent exceptions, illegal thread
 * state exception etc.
 *
 * @author bleny 
 */
public class DisworkDaemonConcurrencyTest extends DisworkDaemonTest {
    
    @Override
    protected void setConfigs() {
        config = newConfig();
        config.setNumberOfWorkers(32);
    }

}
