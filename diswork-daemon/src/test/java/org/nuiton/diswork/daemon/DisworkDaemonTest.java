/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DisworkDaemonTest {

    protected DisworkConfig config;
    protected DisworkConfig config2;

    protected DisworkDaemon daemon;
    protected DisworkDaemon daemon2;

    protected static int port = 3200;

    /** a factory method to ease the creation of configs */
    protected DisworkConfig newConfig() {
        DisworkConfig config = new DisworkConfig();
        port += 1;
        config.setUsedPort(port);
        config.setJobLooksWaitTime(5);
        
        // useless in tests
        config.setStartHttpFrontend(false);
        return config;
    }
    
    /** initialize config and config2
     * override this method permit to create different configuration to test,
     * see sub-classes
     */
    protected void setConfigs() {
        config = newConfig();
    }
    
    @Before
    public void setUp() throws Exception {
        setConfigs();
        daemon = new DisworkDaemon(config);
        if (config2 != null) {
            daemon2 = new DisworkDaemon(config2);
        }
        InputStream application = ClassLoader.getSystemResourceAsStream("fake-app-1.0.zip");
        daemon.submitApplication("fake-app", "1.0", application);    
    }
    
    @After
    public void tearDown() {
        try {
            daemon.close();
            if (daemon2 != null) {
                daemon2.close();
            }
        } catch (Exception e) {
            // close raise errors due to DHT doesn't manage peer leaving 
        }
    }

    @Test
    public void applicationManagement() throws Exception {
        // this application has been submitted
        assertTrue(daemon.isApplicationAvailable("fake-app", "1.0"));
        
        // those have not been submitted
        assertFalse(daemon.isApplicationAvailable("unknow_app", "1.0"));
        assertFalse(daemon.isApplicationAvailable("fake-app", "1.1"));
    }
    
    @Test
    public void simpleSubmit() throws Exception {
        JobDescription job = new JobDescription();
        job.setCommandLine("java -version");
        daemon.submitJob(job);
    }
    
    @Test(expected = DisworkException.class)
    public void testSubmitWithoutDependency() throws Exception {
        JobDescription job = new JobDescription();
        job.setApplication("non-existing-application", "0.0");
        daemon.submitJob(job);
    }
    
    @Test
    public void testSubmitSuccessfulJob() throws Exception {
        JobDescription job = new JobDescription();
        job.setApplication("fake-app", "1.0");
        job.setCommandLine("%java -jar fake-app.jar");
        daemon.submitJob(job);

        while(! daemon.isFinished(job)) {
            Thread.sleep(5 * 1000);
        }
        
        assertTrue(daemon.isStarted(job));        
        assertTrue(daemon.isSuccessful(job));
        
        // check getAllJobs return
        List<JobDescription> currentJobs = daemon.getAllJobs();
        assertEquals(1, currentJobs.size());
        
        JobDescription currentJob = currentJobs.get(0);
        assertEquals(job.getJobId(), currentJob.getJobId());
        assertEquals(job.getCommandLine(), currentJob.getCommandLine());

    }
    
    @Test
    public void testSubmitFailJob() throws Exception {
        JobDescription job = new JobDescription();
        job.setApplication("fake-app", "1.0");
        job.setCommandLine("%java -jar fake-app.jar fail");
        daemon.submitJob(job);

        while(! daemon.isFinished(job)) {
            Thread.sleep(5 * 1000);
        }
        
        assertTrue(daemon.isFailed(job));
    }
    
    /**
     * Create a complex job, submit it and check the results:
     * <ul>
     *   <li>the job need an application (fake-app version 1.0);</li>
     *   <li>the job come with input data (input.txt);</li>
     *   <li>the job need another input file to be downloaded from http;</li>
     *   <li>the job ask for a file to be provided as a result at the end
     *       of the job;</li>
     *   <li>standard output is asked has results.</li>
     * </ul>
     * 
     */
    @Test
    public void testStaging() throws Exception {
        JobDescription job = new JobDescription();
        job.setJobName("My Job");
        job.setApplication("fake-app", "1.0");
        job.setCommandLine("%java -jar fake-app.jar");
        
        // defining data in input
        job.addInput("example.com_index", new URL("http://www.example.com/"));
        job.addInput("input.txt", ClassLoader.getSystemResourceAsStream("input.txt"));
        
        // defining expected data in output
        job.addOutput("output.txt");
        job.addOutput("example.com_index");
        
        // setting standard input and output file
        job.setStandardInput("input.txt");
        job.setStandardOutput("output.txt");
                
        // submit the job
        daemon.submitJob(job);

        // waiting for the job to finish
        while(! daemon.isFinished(job)) {
            Thread.sleep(5 * 1000);
        }

        // check that job is successful
        assertTrue(daemon.isSuccessful(job));
        
        // checking the presence of results
        Map<String, InputStream> results = daemon.getResults(job);
        assertEquals(2, results.size());
        assertTrue(results.containsKey("output.txt"));
        assertTrue(results.containsKey("example.com_index"));
        
        // checking that results are what was expected
        String output = IOUtils.toString(results.get("output.txt"));
        assertEquals("a print on standard output\n", output);
        output = IOUtils.toString(results.get("example.com_index"));
        assertTrue(output.contains("Example Web Page"));
    }

    /**
     * tests the stats given by the daemon
     */
    @Test
    public void testStats() throws Exception {
        Map<String, String> stats = daemon.getLocalStats();

        // deamon should read 10 stats
        stats = daemon.getGlobalStats();
        assertEquals(10, stats.size());
        
        // a second read should return the same data, without re generate them
        // check logs
        stats = daemon.getGlobalStats();
        assertEquals(10, stats.size());
    }
    
    @Test
    public void testJobsManagement() throws Exception {
        assertEquals(0, daemon.getAllJobs().size());
    }
    
    @Test
    public void testJobError() throws Exception {
        JobDescription job = new JobDescription();
        job.setJobName("My Job with wrong command line");
        job.setApplication("fake-app", "1.0");
        job.setCommandLine("%java -jar no_such_jar.jar");
        job.setStandardOutput("output.txt");  
        job.addOutput("output.txt"); 

        daemon.submitJob(job);
        
        while(! daemon.isFinished(job)) {
            Thread.sleep(5 * 1000);
        }

        assertTrue(daemon.isFailed(job));
        assertTrue(daemon.getResults(job).containsKey("output.txt"));
        String output = IOUtils.toString(daemon.getResults(job).get("output.txt"));
        assertTrue(output.contains("Unable to access jarfile no_such_jar.jar"));
    }
}
