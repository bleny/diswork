/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;
import org.nuiton.util.FileUtil;

public class DisworkConfigTest {

    @Test
    public void testSchedule() throws Exception {
        String schedule = "# every night of the week on working day"     + "\n"
                        + "* * 22-8 ? * MON-FRI"                         + "\n"
                        + ""                                             + "\n"
                        + "# all day long during the week-end"           + "\n"
                        + "* * * ? * SAT-SUN"                            + "\n"
                        + ""                                             + "\n";
        File scheduleFile = FileUtil.getTempFile(schedule);
        
        DisworkConfig config = new DisworkConfig();
        config.setSheduleFile(scheduleFile.getAbsolutePath());
        
        // schedule file contains 2 expressions
        assertEquals(2, config.getSchedule().size());
    }
    
    @Test
    public void testTokens() throws Exception {
        // directories can be fake, only strings are considered
        String tokens = "# setting the path to python"                   + "\n"
                      + "%python=/usr/bin/python"                        + "\n"
                      + ""                                               + "\n"
                      + "# overwriting temp dir C:\temp"                 + "\n"
                      + "%tmp=C:\temp"                                   + "\n"
                      + ""                                               + "\n";
        File tokensFile = FileUtil.getTempFile(tokens);

        DisworkConfig config = new DisworkConfig();
        config.setTokensFile(tokensFile.getAbsolutePath());

        assertEquals("/usr/bin/python C:\\temp",
                     config.parseCommandLine("%python %tmp", null));
    }

}
