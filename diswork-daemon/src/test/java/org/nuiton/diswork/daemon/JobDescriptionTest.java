/*
 * #%L
 * Diswork daemon
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.daemon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URL;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests in this class create a complex JobDescription and check that
 * transformation to JSDL and back to an object keep all informations
 * 
 * @author bleny
 */
public class JobDescriptionTest {

    protected static JobDescription job;
    protected static JobDescription job2;
    
    @Before
    public void setUp() throws Exception {
        job = new JobDescription();
        job.setJobName("My Job");
        job.setApplication("fake-app", "1.0");
        job.setCommandLine("%java -jar fake-app.jar");
        
        job2 = new JobDescription();
        job2.setJobName("My Job");
        job2.setApplication("fake-app", "1.0");
        job2.setCommandLine("%java -jar fake-app.jar");
        
        
        // defining data in input
        job2.addInput("example.com_index", new URL("http://www.example.com/"));
        job2.addInput("input.txt", IOUtils.toInputStream(""));
        
        // defining expected data in output
        job2.addOutput("output.txt");
        job2.addOutput("example.com_index");
        
        // setting standard input and output file
        job2.setStandardInput("input.txt");
        job2.setStandardOutput("output.txt");
        
    }

    @Test
    public void testParseJSDL1() throws Exception {
            JobDescription jobCopy;
            try {
                jobCopy = JobDescription.parseJSDL(job.toJSDL());
            } catch (BadJobException e) {
                fail();
                throw e;
            }
            assertNotNull(jobCopy);
            assertEquals(job.getJobName(), jobCopy.getJobName());
            assertEquals(job.getApplicationName(),
                     jobCopy.getApplicationName());
            assertEquals(job.getApplicationVersion(),
                     jobCopy.getApplicationVersion());
            assertEquals(job.getCommandLine(), jobCopy.getCommandLine());
            assertEquals(job.getStandardInput(), jobCopy.getStandardInput());
            assertEquals(job.getStandardOutput(), jobCopy.getStandardOutput());
    }
    
    @Test
    public void testParseJSDL2() throws Exception {
        JobDescription job2Copy;
        try {
            job2Copy = JobDescription.parseJSDL(job2.toJSDL());
        } catch (BadJobException e) {
            fail();
            throw e;
        }
        assertEquals(job2.getCommandLine(), job2Copy.getCommandLine());
        assertEquals(job2.getStandardInput(), job2Copy.getStandardInput());
        assertEquals(job2.getStandardOutput(),
                 job2Copy.getStandardOutput());

        assertTrue(ListUtils.isEqualList(job2.getInput(),
                                     job2Copy.getInput()));
        assertTrue(ListUtils.isEqualList(job2.getOutput(),
                                     job2Copy.getOutput()));
        assertTrue(ListUtils.isEqualList(
                                    job2.getInputUrls().keySet(),
                                job2Copy.getInputUrls().keySet()));
        assertTrue(ListUtils.isEqualList(
                                    job2.getInputUrls().values(),
                                job2Copy.getInputUrls().values()));
    }
}
