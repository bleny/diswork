.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========================
Draft pour Diswork v 0.2
========================

Ce document part de l'expérience acquise lors du développement de la version 0.1
et donne les pistes pour la suite des développements.

État actuel de la version 0.1
=============================

Sur les choix de conception
---------------------------

Hormis les problèmes de sécurité du système, le choix qui a été fait de découper
le problème en trois couches semble justifié :

* Diswork daemon est un petit logiciel qui tourne sur une machine et qui propose
  à Isis de lui soumettre des jobs.

* Diswork File System est un système de fichiers utilisé comme sous-couche du
  démon. Le truc, c'est que tous les daemon utilisent tous le même système de
  fichiers : c'est ce qui les relie entre eux. Lorsqu'un démon reçoit une
  soumission de job, en fait, il écrit le job sur le FS, et comme tous les
  démons ont accès au FS, il suffit d'aller lire ce qui se trouve sur le FS
  pour trouver de jobs.

* Pour assurer la distribution du système de fichiers sur tous les noeuds.
  Diswork FS écrit toutes les données du système dans une seule Map (telle que
  java.util.Map). C'est cette Map qui est distribuée entre toutes les machines,
  et comme toutes les machines ont accès à la même Map, le système de fichier se
  trouve être distribué. Cette Map distribuée, c'est la fameuse DHT, c'est sur
  elle que repose tout le système.

Cette conception est globalement satisfaisante. Le seul problème est inhérent à
l'utilisation d'une DHT : les blocs sont répartis de façon uniforme sur tous 
les nœuds sans tenir compte des capacités de stockage de chacun. Il s'agit
du fameux problème dit de « satisfaction » dans les DHT. Pour que le système
marche, personne ne choisit les données dont il est responsable.

Problèmes de la DHT
-------------------

Il est difficile de trouver une implémentation de DHT qui soit fiable et qui
corresponde à nos besoins. Nos besoins sont :

1. Nécéssité de pouvoir connecté des nœuds qui se trouvent sur des machines
   différentes

2. Une DHT fonctionnelle : et un get() qui suit un put() doit pouvoir permettre
   de récupérer la valeur publiée juste avant.

3. Nécéssité de gérer la réplication pour ne pas perdre de données

4. Nécessité d'être robuste vis-à-vis des connexions/déconnexions (dynamicité de
   structure du réseau)
  
5. Implémenter le contrat Map tel que définit dans java.util.Map, ce qui implique
   notamment de permettre de récupérer l'ancienne valeur lors d'un put() ou 
   d'un remove().

6. La nécessité de pouvoir publier des valeurs qui sont volumineuses. On souhaite
   découper les fichiers en morceaux de 10 Mo. Notamment, cela exlue d'utiliser
   UDP et nécessite d'utiliser TCP pour les échanges, car TCP peut découper
   d'aussi gros messages ce qui n'est pas le cas d'UDP. 

Les tentatives de réutiliser les DHT ont été nombreuses :

* JDHT ne valide pas le point 2.

* Pastry a nécessité un correctif pour valider le poitn 1. Elle ne vérifie pas
  le point 6 et dès que l'utilisation est intense
  (plusieurs put et get en quelques secondes sur une même clé) le point 2
  devient ératique

* XMLStore après deux corrections (pour valider les points 1 et 6) s'avère
  la solution la plus fiable. Son comportement reste soumis à erreur au niveau
  du point 4 dès qu'un noeud quitte le système de façon innopinée

* Mojito ne valide pas le point 6

Sécurité dans Diswork 0.1
-------------------------

Les systèmes distribués en général et P2P en particulier comme Diswork sont
réputés pour être difficiles à concevoir de façon sûre. 

Le problème qui se pose qui nous intéresse ici est le suivant : que se passe-t-il
si on imagine qu'un individu mal intentionné (au hasard, appelons-le Juan)
décide de nuire au système.

Juan a de nombreuses possibilités :

* au niveau du démon on peut déjà soumettre toutes les seconds des jobs qui ne
  finissent jamais. Des jobs qui, en fait, envoient du spam ou font crasher la
  machine qui les lances (fork bombs) ou encore envoyer des tas de requêtes HTTP
  vers un serveur cible ce qui aurait pour effet (en soumettant ce job plusieurs)
  de faire participer tous les noeuds diswork à un DDoS. On peut modifier le
  code du démon pour réserver un job comme si on allait le faire, puis, en fait,
  ne jamais le faire.

* au niveau du File System, c'est simple, les droits ne sont pas gérés
  (et pour cause, voir la suite). Tout est accessible en lecture/écriture.
  Juan peut donc faire un petit « rm -rf / ».

* au niveau de la DHT, nous avons vu que c'est une Map. C'est donc une mémoire
  partagée entre tous les nœuds du réseau : et ça, c'est bon pour Juan. Les DHT
  ont pour particularité de ne pas permettre, contrairement à toutes les Map
  habituelles, de pouvoir parcourir l'ensemble des clés (espace trop grand).
  Toutefois, en se documentant un peu (oui, j'ai fait de la doc.) Juan pourra
  vite trouver un moyen de trouver tout un tas de clés utilisées dans la Map et
  pourra, à loisirs, faire des map.remove() dessus. Je vais pas m'attarder
  m'attarde pas sur les attaques possibles qui sont intrinsèques à l'algorithme
  et/ou à l'implémentation de la DHT utilisée mais si on parle de l'algo
  Kademlia, on peut parler d'attaque Sybil (ou Spartacus)

Comme on peut le voir, il y a trop de problèmes à trop de niveaux pour espérer
tout réglé mais on va quand même essayé.

Tentons de régler les problèmes, niveaux par niveaux :

Commençons par le démon. Envisageons de filtrer les jobs pour ne faire que des
jobs qui sont "gentils" : sur quel critère se baser pour dire que tel ou tel de
job est mauvais ?

* L'application utilisée ? On peut créer des jobs qui dépendent de Isis (isis
  sera téléchargé) mais préciser une ligne de commande pour ce job qui n'a rien
  à voir (fork bomb)

* Le nœud qui a soumis le job ou les résultats d'un job ? Comment savoir quel
  nœud a posé ce job sur le FS. Vu que le FS est accessible en écriture pour
  Juan, Juan pourrait changer les informations concernant le propriétaire du job
  et donc poser un job vicieux et dire qu'il est à Éric (qui est un ange, comme
  chacun sait). Même si ce n'était pas le cas, comme savoir que Éric est gentil
  et Juan méchant ? Pas moyen. Pour s'assurer de l'origine d'un job, on pense
  tout de suite à signer les jobs avec un système de clés asymétriques. Ça ne
  change rien, Juan peut signer des jobs et des résultats aussi. Vient alors
  encore une idée, on a qu'à faire faire le job par 4 machines différentes,
  comparer les quatre résultats et voir si les résultats sont différent. Si un
  résultat diffère des trois autres, on considère qu'il a été mal fait par une
  machine contrôlée par Juan et on exclue ce résultat pour se baser sur les
  trois restants, supposés fiables. Sauf que, comme dit, les jobs ne sont pas
  poussés vers ceux qui les font mais posés sur le file-system, d'autres nœud,
  en parcourant le FS les trouve. Du coup, Juan peut facilement lancer quatre
  nœud (voire un seul nœud), réserver les quatre jobs, calculer (une fois) de
  faux résultats, les signer avec des clés différentes et les publier 4 fois
  comme 4 résultats faits par des machines différentes (avec un pti délai entre
  chaque dépôt pour pas qu'on voit que tout a été fait en même temps...). Juan
  est vraiment vicieux. En plus, ça impliquerait de mettre en place un système
  pour échanger les clés publiques, il ne faut surtout pas les mettre sur le FS,
  Juan pourrait remplacer celle d'Éric par la sienne. On peut imaginer les
  mettre toutes sur un serveur à côté, où les clés sont publiées une fois pour
  toutes mais comment éviter que Juan y publie ses propres clés ?

Passons au système de fichier. Pour résoudre les problèmes évoqués ci dessus, on
pourrait souhaiter des droits de le forme UNIX pour le FS distribué (des
utilisateurs, des groupes et des droits lectures/écritures par fichier pour le
propriétaire, le groupe et les autres). Il faudrait stocker ces nouvelles
données dans la Map, c'est pas un problème. Cependant, comme, de toute façon,
c'est Juan qui choisit d'interpréter les données qu'il peut lire dans la Map,
il pourra lire les droits, les ignorer, et lire le fichier quand même : il
suffit de faire des get().

Et si on crypte ? Et bien Juan n'arrivera pas à lire les données, c'est déjà ça
de pris. Encore faut-il savoir avec quelle clé crypter. Celle du propriétaire ?
Non, le membres du groupe ne pourrait pas lire. Celle du groupe alors ? Oui,
c'est déjà mieux. En fait, il faut choisir le clé en fonction des droits. Quand
on écrit le fichier, il faut crypter avec la clé du groupe si le groupe a accès
en lecture, celle du proprio si le groupe n'a pas accès en lecture et ne pas
crypter si tout le monde a accès en écriture. Ça suppose que le proprio
appartienne au groupe (c'est pas absurde). Encore faut-il ne distribuer la clé
du groupe qu'à ceux qui sont dans le groupe et ne pas la divulguer. Ça peut
s'imaginer disons, en déposant dans le home d'un utilisateur un fichier qui
contient la clé du groupe cryptée avec la clé publique de l'utilisateur qu'on
veut ajouter au groupe. On retombe sur le problème des clés publiques parce
que si je la mets sur le FS, elle doit être en clair pour que tout le monde
puisse m'ajouter à un groupe et donc, Juan peut m'usurper. Du coup, il faut
distribuer les clés publiques par l'extérieur du FS (via HTTP par exemple) et
là, on peut croire qu'on est bon. Encore faut-il supposer qu'on ne peut pas
changer les proprio et groupe d'un fichier (parce qu'on ne peut pas re-crypter
sans la clé privée) et qu'ils sont donc fixés à la création et immuables. Ne
parlons pas du cas où on souhaiterait exclure un utilisateur d'un groupe : la
clé lui a déjà été divulguée et donc, il faudrait ré-encoder tous les fichiers
(plusieurs Go ?) de ce groupe avec une nouvelle clé...

On a donc réussi à faire un système qui permet à un utilisateur d'écrire ses
fichiers et de les lire sans que personnes d'autres ne puissent les lire. C'est
intéressant mais parfaitement inutile dans le cas de Diswork vu que tous les
fichiers doivent être partagés. Autre possibilité, y'a qu'à les mettre dans le
groupe "diswork", sauf qu'il faudrait savoir comment un noeud honnête peut
rejoindre se groupe ou pourquoi Juan, lui, ne pourrait pas en se faisant passer
pour honnête (voire faire quelques jobs bien), puis trahir plus tard. Donc le
groupe, ça marche pas vraiment, donc autant tout mettre en clair. Donc retour
à la case départ.

Mais, y'a pire ! Là, au niveau du FS, on a vu qu'on pouvait embêter Juan s'il
voulait faire un get() en lui rendant la valeur lue incompréhensible. Peut-être,
mais ça n'empêchera pas Juan de faire un remove() d'une clé, ou un put() avec
des données vides voire des grossièretés. Donc de toute façon, on peut peut-être
l'empêcher de lire, il reste toujours à Juan la possibilité de supprimer nos
données.

C'est ce qui m'amène à penser que, finalement, le problème, c'est soit la DHT
soit son utilisation.

Suivons la première hypothèse, la seule échappatoire que je vois, c'est
d'utiliser une DHT qui gère les droits au niveaux des clés.
Je ne connais aucune DHT qui permet cela.

On peut repenser à l'époque où il était très facile d'écrire des malwares.
Il suffisait de faire un peu de C ou de tout langage bas niveau, on pouvait
aller taper tout simplement dans la mémoire vive, faire un malloc sur un tableau
et écrire au delà de l'espace alloué et, du coup, écrire sur le reste dans la
mémoire : en allant assez loin, on écrit sur la mémoire utilisée par les autres
processus voire sur celle utilisée par l'OS. Pareil pour détruire des système
de fichiers, on court-circuite le système de droit ou la possibilité de
parcourir le FS en écrivant direct des I/O sur le disque dur, on arrivera bien à
abîmer quelque-chose...

Ce temps est aujourd'hui révolu. Aujourd'hui les programmes n'ont accès qu'à la
mémoire qui leur est allouée par l'OS. Ça clos le problème.

On peut faire une analogie avec notre utilisation de la DHT. Notre DHT est
mémoire partagée et cela pose le même problème que précédemment. N'importe qui
peut écrire n'importe-quoi. Qu'il s'agisse de gérer la mémoire vive d'un ordinateur
(mémoire partagée entre différents processus locaux),
ou d'une DHT (mémoire partagée entre des processus distants), les probèmes sont
les même et la solution du premier pourrait amener une solution au deuxième. 

Seconde hypothèse, on a pas compris le principe (et on est pas les seuls à
s'être planté, Pastis est un exemple) et pour faire un système de fichier P2P,
on a pas la bonne approche. Peut-être qu'il faut utiliser la DHT autrement,
voire ne pas utiliser une DHT mais pour l'instant je ne vois pas de meilleure
solution que l'actuelle, étant donné le CdC.

Le problème est intrinsèque à la DHT qui ne gère pas les droits. Je ne connais
pas de DHT qui le fasse mais ça semble envisageable. Dans le cas de la gestion
de la mémoire vive, un code intermédiaire (l'OS) fait les contrôles adéquats
pour éviter qu'un programme fasse des I/O indésirables. On pourrait faire pareil
dans le DHT, en empêchant chaque nœud de faire les opérations lui-même et en
devant systématiquement les déléguées à un autre qui vérifiera les droits.
L'intermédiaire ne doit pas être choisi mais imposé et doit se charger simplement
de vérifier le droit avant que les opérations soit réellement menées, en instaurant
la méfiance entre l' nœud qui fait la requête, le nœud intermédiaire et le nœud 
responsable, on multiplie les intervenants et on se base sur le principe que les
nœuds bienveillants sont majoritaires. Les solutions apportées au problème des
généraux byzantins devraient être utiles.  

En l'état actuel, le plus sage reste la solution de placer la sécurité à la
périphérie, et de veiller à ce que seuls des nœuds de confiance se connectent
au système global. Ainsi, on cloisonne le système, c'est pas absurde si les
noeuds peuvent se trouver dans un même labo comme c'est le cas pour Isis.

Ou alors... il faut tout remettre à plat, et envisager pour une prochaine
version de Diswork, de s'y prendre autrement, avec une DHT utilisée autrement
ou sans DHT. Mais là, je sèche parce que vu les contraintes exprimées, il y
avait pas tellement possibilité de faire autrement...

Pistes pour la version 0.2
==========================

Implémenter une DHT qui assure toutes les propriétés nécessaires
----------------------------------------------------------------

1. Nécéssité de pouvoir connecté des nœuds qui se trouvent sur des machines
   différentes

2. Une DHT fonctionnelle : et un get() qui suit un put() doit pouvoir permettre
   de récupérer la valeur publiée juste avant.

3. Nécéssité de gérer la réplication pour ne pas perdre de données

4. Nécessité d'être robuste vis-à-vis des connexions/déconnexions (dynamicité de
   structure du réseau)
  
5. Implémenter le contrat Map tel que définit dans java.util.Map, ce qui implique
   notamment de permettre de récupérer l'ancienne valeur lors d'un put() ou 
   d'un remove().

6. La nécessité de pouvoir publier des valeurs qui sont volumineuses. On souhaite
   découper les fichiers en morceaux de 10 Mo. Notamment, cela exlue d'utiliser
   UDP et nécessite d'utiliser TCP pour les échanges, car TCP peut découper
   d'aussi gros messages ce qui n'est pas le cas d'UDP. 

::
  
    application
       ↓ ↑
     blocks
       ↑ ↓
     overlay
       ↑ ↓
     network

** La couche application **

Elle contient le code spécifique à l'application P2P, elle est développée
en utilisant des briques fournies par la couche blocks. 

** La couche blocks **

La couche blocks devrait fournir les éléments constitutifs d'une application
P2P : collections distribuées notamment. Les blocks doivent être implémentés
de façon à gérer la réplication (propriété 3).
Ces blocks doivent suffir pour écrire une application P2P. Au niveau
des blocks, il faudra veiller à limiter le parallèlisme pour assurer
la propriété 2.

Un des block proposé dans le couche block sera évidemment une Map. Il faudrait
également proposé MultiMap et Tree. Ce dernier sera idéal pour écrire le FS.

Idéalement, il faudrait prévoir l'utilisation de plusieurs instances d'un même
block dans une même application. Par exemple, un système de fichier pourrait
utiliser Tree pour stocker l'arborescence, une Map pour stocker les données
et une autre Map pour stoker les utilisateurs. 

Au niveau des blocs collections, il faudra proposer plusieurs méthodes de stockage
des clés stockées localement dont l'une stockera les données sur le système
de fichier local. D'autres implémentations pourront utiliser HashMap, une BdD
embarquée, Cassandra...

Pour assurer le réplication, la couche overlay doit fournir un ensemble de noeud
accessibles rapidement (des proches). Chaque noeud doit répliquer les clés/valeurs
sur tous les noeuds proches, par exemple en utilisant un algorithme de transaction
distribué comme Two Phase Commit.

Il faudrait gérer les droits à ce niveau-là. 

** La couche overlay **

La couche overlay se charge d'entretenir une table de routage et l'algorithme
pour atteindre tous les noeuds du réseau à partir d'une table de routage partielle.
Dans cette couche se trouve les algorithmes de routages (kademlia, pastry...).

L'implémentation devra veiller à bien gérer les erreurs et éventuellement,
attendre que le système se stabilise avant de pouvoir exécuter les opérations
demandées au niveau block. La gestion d'erreur peut avoir quelque-chose à voir
avec http://en.wikipedia.org/wiki/Byzantine_fault_tolerance

L'overlay doit également gérer l'entretien d'un groupe restreint de noeuds proches
(tels que les leafset de Pastry).

Cette couche doit être implémentée en ayant toujours l'hypothèse qu'un noeud
a pu se déconnecter depuis le dernier message échangé.

** La couche network **

La couche network permet à l'overlay d'envoyer des messages et d'en recevoir
et de pinguer un noeud (pour que la couche overlay puisse assurer la propriété
4).

Une implémentation réseau pourrait faire usage 
d'Apache Mina pour assurer les points 1 et 6 (Mina est basé sur TCP). C'est à ce
niveau qu'on peut intervenir sur la sécurité bas niveau (connexion cryptées via
TLS) et sur le passage de Firewall. D'autres implémentations plus fantaisistes
peuvent se baser sur REST (cf Riak).

On peut envisager que les connexions entre un
noeud et les noeuds membres de son Leafset soient permanentes.

Éventuellement, on pourra avoir une sous-couche décourverte qui permet de 
d'essayer de découvrir un noeud ou une liste de noeud de bootstrap. Possibilité
de cumuler les méthodes :

* Aller chercher une liste noeuds souvent connectés dans un fichier (fichier
  local livré avec l'appli ou dispo via http)

* Découverte dynamique de noeuds sur le réseau local via Zeroconf 

Ne plus utiliser de DHT comme base pour le sytème de fichiers
-------------------------------------------------------------

Comment faire ?

Ne plus utiliser de système de fichier
--------------------------------------

Et préférer un système de partage de fichiers, les
noeuds conservent leurs données localement et les proposent à qui les demande.

Ça évite tout risque de perte de données mises en ligne mais ça à l'inconvénient
de nécessiter à un noeud d'être présent et d'attendre que quelqu'un demande
un fichier alors qu'avec la solution précédente, on peut publier un fichier
et quitter le système juste après, le fichier est toujours disponible.

Ça pose également le problème de la modification d'un fichier qui se trouve
chez l'autre.

Il faut aussi trouver le moyen de mettre les noeuds en relation, une solution
serait d'avoir, à la eMule, des serveurs centralisés dont le seul rôle est
d'indexer les fichiers proposés par les noeuds. Les noeuds peuvent ensuite faire
des recherches via ce serveur central.

Ça nous éloigne beaucoup (trop) du Cahier des charges initial.