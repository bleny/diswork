.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Presentation
============

Diswork is a `distributed computing`_ library writen in Java_ used to run Java_
application.

References
----------
Here is a list a client application that use Diswork_ :
  * IsisFish_ (*soon*)


.. _Diswork: http://maven-site.nuiton.org/diswork
.. _Java: http://java.sun.com/
.. _distributed computing: http://en.wikipedia.org/wiki/Distributed_computing
.. _IsisFish: http://isis-fish.labs.libre-entreprise.org/