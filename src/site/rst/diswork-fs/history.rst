.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==========
Historique
==========

Premier prototype
-----------------

L'objectif était de réaliser un système opportuniste : à chaque fois que des
données sont envoyées par un nœud parce qu'un autre nœud les a demandées,
l'envoi en multicast permet à tous les autres nœuds de recevoir les données et
ainsi faire la réplication.

Le premier prototype de diswork-fs est basé sur l'échange de messages. Les
nœuds connectés à un même réseau peuvent envoyés et recevoir en multicast des
messages. Il existe quatre types de messages :

* LookUp qui permet de demander à tous les noeuds, étant donné un chemin,
  si un fichier existe ;
* LookUpResponse qui est envoyé si un nœud suite à la réception d'un LookUp,
  constate qu'il est en possession du fichier demandé. Avant la réponse, on
  calcule à partir du fichier un descripteur (FileDescription) comprenant
  diverses informations nécessaire à l'échange du fichier (taille totale,
  somme de contrôle, nombre de parties) ;
* FileRequest est éventuellement envoyé après réception d'un LookUpResponse,
  il vise à demander l'envoi des données d'un fichier ;
* La réception d'un FileRequest, génère l'envoi, en réponse, de message de types
  FileTransfer. Chacun d'eux contient une partie du fichier.

Ainsi, ce protocole permet :

* la recherche d'un fichier pour un chemin donné dans une arborescence ;
* si besoin, son téléchargement ;
* la réplication.

Ce protocole ne permet pas :

* Lister le contenu d'un répertoire ;
* Utiliser des liens symboliques.

Cela n'est pas rendu possible par l'absence d'informations communes et
synchronisées entre les nœuds. Pour remédier à ce problème, il semble qu'une
voie possible serait de répliquer entre tous les nœuds l'intégralité de 
l'arbre décrivant l'arborescence[1]_ et de ne pratiquer l'échange des données
des fichiers qu'au moment opportun[2]_.

Ce premier prototype a été implémenté grâce à JGroups.
Il est fonctionnel s'il s'agit d'avoir un système de
fichier partagé et si on lit et écrit dans l'arborescence à des chemins connus
à l'avance. C'est trop limité pour un système de fichier.

.. [1] voir le building block `ReplicatedTree <http://www.jgroups.org/javadoc/org/jgroups/blocks/ReplicatedTree.html>`_.
.. [2] voir `Replicated Filesystem based on ReplicatedTree <http://www.jgroups.org/open_projects.html>`_.