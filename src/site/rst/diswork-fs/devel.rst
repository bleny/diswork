.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=============================
How Diswork File System works
=============================

Abstract
========

This document is for people who want to develop Diswork File System. It is not
an interesting reading for people who write to write code that use Diswork
FS. 

Diswork File System uses a map to store every data
==================================================

Diswork File System uses a map to store data persistently.

We illustrate the use of a file system by setting the content of "/",
the root directory as follow:

*  a directory named "my_folder" that contains:

  - a directory named "my_sub_folder" that contains:
  
    + a file name "my_deep_file.ext"
    
    + an empty folder named "my_empty_folder"
    
  - a file named "my_other_file.ext"
  
* a file named "my_file.ext" (containing binary data: 010101)

* a symbolic link named "my_link" which target the path
  "/my_folder/my_sub_folder/my_deep_file.ext"

Storage uses a Map to store data.

+--------------+---------------------------------------------+
| Key (String) | Value (byte[])                              |
+==============+=============================================+
|              | "D:my_folder:ID1\n                          |
|              |  F:my_file.ext:ID2\n                        |
| "/"          |  L:my_link:ID3"                             |
+--------------+---------------------------------------------+
|              | "D:my_sub_folder:ID4\n                      |
| "ID1"        |  F:my_other_file.ext:ID5"                   |
+--------------+---------------------------------------------+
| "ID2"        | 010101                                      |
+--------------+---------------------------------------------+
| "ID3"        | "/my_folder/my_sub_folder/my_deep_file.ext" |
+--------------+---------------------------------------------+
|              | "F:my_deep_file:ID6\n                       |
| "ID4"        |  D:my_empty_folder:ID7"                     |
+--------------+---------------------------------------------+
| "ID5"        | 011010100111010...                          |
+--------------+---------------------------------------------+
| "ID6"        | 1100010101010110110010...                   |
+--------------+---------------------------------------------+
| "ID7"        | ""                                          |
+--------------+---------------------------------------------+

An entry is, for example, "D:my_folder:ID1". It contains the type of the 
entry (<strong>D</strong>irectory, <strong>F</strong>ile or
<strong>L</strong>ink), the name of the element, and an ID to be used as
a key on the map to get the actual content. Those three informations are
separated by ":" which is EntryUtil.ENTRY_SEPARATOR.

A directory way have multiple <em>entries</em>. Entries of a directory are
separated by "\n" which is EntryUtil.ENTRIES_SEPARATOR.

The above description shows the main principle used to store a tree
structure in a map.

In fact, the storage of actual data is a bit more complex, due to
the need of splitting the data that may be too large (the value of "ID6"
may be a series of 1002<sup>20</sup> bytes for a 200 MiB file).
Actually, files content (like ID2, ID5, ID6) and directory content
values ("/", ID1, ID4) will be split and the map will look like that (
<strong>note that only the files have been split for readability</strong>): 

+--------------+---------------------------------------------+
| Key (String) | Value (byte[])                              |
+==============+=============================================+
|              | "D:my_folder:ID1\n                          |
|              |  F:my_file.ext:ID2\n                        |
| "/"          |  L:my_link:ID3"                             |
+--------------+---------------------------------------------+
|              | "D:my_sub_folder:ID4\n                      |
| "ID1"        |  F:my_other_file.ext:ID5"                   |
+--------------+---------------------------------------------+
| "ID2"        | "6;ID8"                                     |
+--------------+---------------------------------------------+
| "ID3"        | "/my_folder/my_sub_folder/my_deep_file.ext" |
+--------------+---------------------------------------------+
|              | "F:my_deep_file:ID6\n                       |
| "ID4"        |  D:my_empty_folder:ID7"                     |
+--------------+---------------------------------------------+
| "ID5"        | "14689;ID9"                                 |
+--------------+---------------------------------------------+
| "ID6"        | "79874567;ID10;ID11;ID12"                   |
+--------------+---------------------------------------------+
| "ID7"        | ""                                          |
+--------------+---------------------------------------------+
| "ID8"        | 010101                                      |
+--------------+---------------------------------------------+
| "ID9"        | 011010100111010...                          |
+--------------+---------------------------------------------+
| "ID10"       | 110001010101011...                          |
+--------------+---------------------------------------------+
| "ID11"       | 111010110101101...                          |
+--------------+---------------------------------------------+
| "ID12"       | 011101100111010...                          |
+--------------+---------------------------------------------+

Values of ID2, ID5, ID6 are no longer the file content. It's now a set
of meta-information information data called metablock.
A metablock is composed of the total size of the file followed by
an ordered lists of IDs of the different blocks composing the file.
Those informations are separated by ";" (see
{@link org.nuiton.disworkfs.storage.EntryUtil#BLOCKIDS_SEPARATOR}).

In the above example:

* "/my_file.ext" is a file which size is 6 bytes, it is
    composed of one block available at ID8;

* "/my_sub_folder/my_other_file.ext" is a file (size = 14689 bytes)
   which content is available at ID9;
  
* "/my_sub_folder/my_sub_folder/my_deep_file.ext" is a big file: its
  size is 79874567. It has been split in three blocks: ID10, ID11 and
  ID12.
 
When reading and writing in storage, split is done transparently. When
reading, a Stream is returned: it loads data blocks after blocks
when needed inner class Storage.SplitBlocksInputStream}).
When writing, data are split in blocks of a maximum configurable size.

The map is distributed
----------------------

The map described above is distributed over all nodes through a DHT.

Diswork File System manage concurrency
--------------------------------------

DFS uses copy-on-write to deal with concurrency. Nothing is overridden, when 
a file is replaced, the new file is written on the FS without removing the
old file. The old file data is kept and actually removed later.

User rights management and security issues
==========================================

Peer-to-peer applications are subject to malicious attacks. Introduction of one
or few malicious node in such a network can cause many problems.

At the File System level, since there is no user-rights management, a malicious
node can connect and remove recursively root directory or replace any file
with any bits.

At the DHT level, any malicious node introduce multiple threats :

* a distributed denial of service attack (multiple nodes always asking for the
  same key) 

* various DHT-specific attacks like joining the DHT with a chosen ID allowing
  to become responsible of a target key that will no longer be avaible due to
  a malicious peer that do not respond.

* low-level attacks : by sniffing network traffic, key and values that pass
  through the target connection may be revealed and data privacy corrupted  

DHT implementation may provides means to prevent DHT-level threats but we still
need some ways to prevent File-System level threads. The main problems here is
that DHT is readable/writable shared memory for each of the nodes connected to
the network. Thus, a single node can write any bad data.

Those issues lead to the need to implements consider user-rights on the
File-System. This is not possible because all nodes are self-responsible to
what they do on the DHT. Thus, the nodes that do the operation on the DHT will
will be responsible to check it has the right to do so. It's easy to imagine
a malicious node, allowing itself all operations by badly implementing rights
checks.

To make user-rights management possible, DHT should give make all operation
of a node controlled by another one. Just like a given program, running in
an operating system, can't do any I/O on local file system, the underlying
file-system is another piece of code that manage the program rights. Here,
we don't have this intermediate code, so another node should do this job and
system will become reliable if it's not possible for a malicious node to choose
itself as intermediate node or any node cooperating with the malicious one.

However, this is a DHT consideration and we can't deal with malicious nodes
at the File System level.

But we can provide a security layer on top of the file-system. This layer
should permit to crypt and sign data one writes in order for others to check
data integrity. Those operations can be done using asymetric key encryption.

When reading/writing a file or directory, keys will permit to be sure that
a given file or directory content has been written by a given user.

That's not sufficient, malicious nodes can crypt and sign datas too. So
top level application, that use the File system should make is own
business-oriented checks before considering a data.

Nodes can write and then read encrypted data if it checks the
file integrity. Public keys should not be made available through Diswork FS
since it can be replaced by a malicious node.

Public keys for each nodes should be made available by another means. It may
be a read-only public key store available through HTTP for example.