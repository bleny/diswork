.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==================
How to use Diswork
==================

How to install a diswork node on my computer
--------------------------------------------

How to install a diswork node on my server
------------------------------------------

::

  svn checkout http://svn.nuiton.org/svn/diswork/trunk/ diswork
  cd diswork/diswork-daemon
  mvn assembly:assembly -DdescriptorId=jar-with-dependencies
  
  cd ../..
  # under debian commons-daemon can be found at /usr/share/java/commons-daemon.jar
  # run as root :
  jsvc -cp /usr/share/java/commons-daemon.jar:/usr/share/maven-repo/log4j/log4j/1.2.15/log4j-1.2.15.jar:diswork/diswork-daemon/target/diswork-daemon-0.0.1-SNAPSHOT-jar-with-dependencies.jar -pidfile ./pid -outfile ./out -errfile ./err org.nuiton.diswork.daemon.DisworkDaemonRunner

How to make my own application ready for being run by all diswork nodes
-----------------------------------------------------------------------

Applications for diswork are very easy to create. An application is a zip file.
The only specification of the content is that it has to be set considering that
the whole application will be unzipped in the job-specific directory.

You can put executables in it, a jar file with its dependencies, scripts, etc.

You have to write the command-line for the job being aware that the application
and all input files will be in the current working directory.

How to make my application able to submit jobs to Diswork and retrieve results
------------------------------------------------------------------------------

Your application have to use the Daemon API, here is a sample code :

::

    // First, start a daemon
    DisworkConfig config = DisworkConfig.newConfig();
    daemon = new DisworkDaemon(config);
    
    // submit the application
    daemon.submitApplication("fake-app", "1.0", new FileInputStream("fake-app-1.0.zip"));
    
    // now all the nodes will be able to run jobs that need fake-app version 1.0
    
    // create a new job
    JobDescription job = new JobDescription();
    job.setJobName("My Job");
    job.setApplication("fake-app", "1.0");
    job.setCommandLine("%java -jar fake-app.jar");
    
    // now, defining data in input
    
    // this will be downloaded by the worker before job start
    job.addInput("example.com_index", new URL("http://www.example.com/"));
    // this will, needed for fake-app will be provided to the worker
    job.addInput("input.txt", new FileInputStream("input.txt"));
    
    // defining expected data in output
    job.addOutput("output.txt");
    job.addOutput("example.com_index");
    
    // setting standard input and output file
    job.setStandardInput("input.txt");
    job.setStandardOutput("output.txt");
            
    // submit the job
    daemon.submitJob(job);
    
    // waiting for the job to finish
    while(! daemon.isFinished(job)) {
        Thread.sleep(5 * 60 * 1000);
    }
    
    // check that job is successful and read the result files
    if(daemon.isSuccessful(job)) {
        // results will contains keys "output.txt" and "example.com_index"
        // values are InputStream on the content of those files
        Map<String, InputStream> results = daemon.getResults(job);
    }

    
