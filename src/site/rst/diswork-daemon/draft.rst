.. -
.. * #%L
.. * Diswork
.. * %%
.. * Copyright (C) 2010 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Diswork first draft (fin mars 2010)
===================================


Principe
--------
3 types de machines : annuaires (uptime très grand), stockage (beaucoup de HDD),
calculateur (beaucoup de CPU). Un noeud (une machine physique) peut être d'un ou
plusieurs type.
Les annuaires sont reliés entre eux en DHT (Distributed Hashtable).
Potentiellement 1 ou 2 serveurs existent pour indiquer les point d'entrées
dans le réseau pour pouvoir fonctionner hors d'un réseau local.
Un client envoie aux annuaires les simulations qu'il doit effectuer : version du
simulateur et data.
Quand un calculateur a du temps, il demande à l'annuaire ce qu'il doit faire.
Récupère une simu, le simulateur approprié si il ne l'a pas (P2P ?) fait tourner
la simu et dit aux stockages que les résultats sont dispo. Réplication des
résultats entre les stockages. Client demande à l'annuaire où sont les résultats
et les télécharges (P2P ?)

Storage
~~~~~~~
Pour les serveurs de stockage, il recherche a duplique l'information
qui ne l'est pas assez. Par exemple dans l'annuaire, un resultat est
marque comme ayant une disponibilite de 1 (une seul machine active a
l'avoir) alors un serveur de stockage au repos va essaye de recuperer
ce resultat pour augmenter ce taux de partage.

Il faut bien penser que l'intelligence est dans les serveurs qui
veulent faire du travail, pas dans un serveur qui repartira les choses
(surtout pour le stockage on se n'est pas une approche vraiment
naturelle)

Tracker
~~~~~~~
Les trackers possèdent les logiciels de simulation.

Détails
-------
- Utilisation d'algorithme efficace pour découvrir l'ensemble des noeuds
- Un noued peut se contenter de ne connaitre que ces voisins (annuaire, stockage, calculateur)
- Un peu de config pour que le calculateur puisse choisir les applis qui ont le droit de tourner
- Quand résultats sont produits, aucun calculateur ne peut plus démarrer la simu
- Il faut optimiser les simulations a faire pour que le maximum de calculateur en fasse le plus de differente
- Plusieurs calculateur peuvent effectuer la même simulation
- Les calculateurs gere eux même les simulations en erreur (telle simulation a echoué 2 fois, je ne la fait plus)
- Le client peut télécharger les résultats depuis plusieurs noeud de stockage (P2P)
- Communication : utiliser HTTP pour éviter les firewall
- Vérification d'un ckecksum sur les logiciels de simulation (et les résultats?)
- Voir comment faire entrer Caparmor dans le réseau (noeud qui délègue à Caparmor ?)
- Mettre en pause la simulation quand l'utilisateur se ressert de son poste
- Detecter que l'utilisateur ne se sert plus de son poste pour lancer les simulations (inactivité, pas de charge cpu)
- Postes windows, utilisation d'un screensaver ? (diswork en cours)
- Les noeuds savent se mettre à jour (ou détecter qu'il ne sont pas à jour)
- Bonne gestion de l'espace disque sur les noeuds de stockage
- Systèmes de statistique pour insiter à offrir du stockage, du cpu, des annuaires (bonus)


Contacts
--------
- Gerson Sunye pour le DHT
- Lamarre pour le plus haut niveau (les algos de recherche/decouverte...)
- TMG pour le P2P (on va avoir les mêmes problématiques : connexion entrante sur un pair qui n'est pas un serveur)


Technique
---------
- JXTA : protocole P2P
- rechercher s'il n'y a pas d'autre librairie utilisable. Par exemple
regarder dans les implantations java bittorrent s'il n'y a pas des
choses recuperables (vuze, ...)


Premier objectif
----------------
Bonne structure de l'annuaire.
Etude des protocoles.
Solutionnement des problématiques de communication (firewall).

Fonctionne sans réplication des données.
Fonctionne sans téléchargement depuis plusieurs sources.

La version 1.0.0 doit être entierement fonctionnelle (sans contenir toutes les
fonctionnalités)