/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ConcurrentModificationException;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class DisworkFileSystemKademliaIT extends AbstractDisworkFileSystemTest {
    
    protected Integer bootstrapPort;
    
    /**
     * this code executed after {@link AbstractDisworkFileSystemTest#setUp()}
     * @throws Exception
     */
    @Before
    public void setUpFileSystem() throws Exception {
        // finally, initiate the fileSystem
        DisworkFileSystemConfig disworkConfig1 = new DisworkFileSystemConfig();
        disworkConfig1.setMapType("kademlia");
        bootstrapPort = disworkConfig1.getUsedPort();
        fileSystem = new DisworkFileSystem(disworkConfig1);
    }

    
    @Test
    public void testMultipleNodes1() throws Exception {
        DisworkFileSystemConfig disworkConfig = new DisworkFileSystemConfig(bootstrapPort);
        disworkConfig.setMapType("kademlia");
        DisworkFileSystem fileSystem2 = new DisworkFileSystem(disworkConfig);
        
        fileSystem.write("/file", new FileInputStream(randomFilePath));
        assertTrue(fileSystem2.exists("/file"));
    }
    
    @Test
    public void testMultipleNodes2() throws Exception {
        DisworkFileSystemConfig disworkConfig = new DisworkFileSystemConfig(bootstrapPort);
        disworkConfig.setMapType("kademlia");
        DisworkFileSystem fileSystem2 = new DisworkFileSystem(disworkConfig); 
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }

        assertTrue(fileSystem.exists("/my_file"));
        assertTrue(fileSystem2.exists("/my_file"));
        assertEquals(1, fileSystem2.readDirectory("/").size());
        
        // now, the checks. We read the original file and the result of
        // a read() and then compare it byte-to-byte

        InputStream readResult = null;
        try {
            source = new FileInputStream(randomFilePath);
            readResult = fileSystem2.read("/my_file");

            assertEquals(randomFileSize, source.available());
            assertEquals(randomFileSize, readResult.available());

            assertTrue(IOUtils.contentEquals(source, readResult));
            /*
            byte[] sourceAsBytes = IOUtils.toByteArray(source);
            byte[] readResultAsBytes = IOUtils.toByteArray(readResult);
        
            assertArrayEquals(sourceAsBytes, readResultAsBytes);
            */
        } finally {
            IOUtils.closeQuietly(source);
            IOUtils.closeQuietly(readResult);
        }

    }
    
    @Test
    public void testMultipleNodes3() throws Exception {
        DisworkFileSystemConfig disworkConfig = new DisworkFileSystemConfig(bootstrapPort);
        disworkConfig.setMapType("kademlia");
        DisworkFileSystem fileSystem2 = new DisworkFileSystem(disworkConfig);

        fileSystem.createDirectory("/mydir");
        try {
            fileSystem2.createDirectory("/myseconddir");
        } catch (ConcurrentModificationException e) {
            fail();
        }
    }    
}
