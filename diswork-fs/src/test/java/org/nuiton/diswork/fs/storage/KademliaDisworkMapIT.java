/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.diswork.fs.DisworkFileSystemException;
import org.planx.xmlstore.routing.Identifier;

public class KademliaDisworkMapIT extends AbstractDisworkMapTest {
    
    @Before
    public void setUp() throws Exception {
        DisworkFileSystemConfig config1 = new DisworkFileSystemConfig();
        map1 = new KademliaDisworkMap(config1);
        
        DisworkFileSystemConfig config2 = new DisworkFileSystemConfig(config1.getUsedPort());
        map2 = new KademliaDisworkMap(config2);
    }
    
    /**
     * this tests the key generation algorithm, it has different properties to
     * check :
     * * two call with same param should return same key
     * * two call with different parameter should return different keys
     * * a call should never return null
     */
    @Test
    public void testStringToIdentifier() {
        
        List<String> paths = new ArrayList<String>();
        
        // a set will remove doubloons
        Set<Identifier> keys = new HashSet<Identifier>();
        
        paths.add("/");
        paths.add("/dir");
        paths.add("/dir/dir");
        paths.add("/a_directory_with_a_long_name_may_lead_to_the_raise_of_an_"
                + "exception");
        paths.add("1111111111111111111111111111111111111111111111111111111111");
        paths.add("1111111111111111111111111111111111111111111111111111111112");
        
        for (String path : paths) {
            Identifier key = KademliaDisworkMap.stringToIdentifier(path);
            assertNotNull(key);
            
            // keys should be equals (depend only from path, not random factor)
            Identifier otherKey = KademliaDisworkMap.stringToIdentifier(path);
            assertEquals(key, otherKey);
            
            keys.add(key);
        }
        
        // check differents paths have different keys
        // if keys set is too small, there are collisions
        assertEquals(paths.size(), keys.size());
    }
    
    /**
     * Tests that an exception is raised when attempting to connect using
     * a bad bootstrap 
     * @throws Exception
     */
    @Test
    public void testBadBootrap() throws Exception {
        DisworkFileSystemConfig config1 = new DisworkFileSystemConfig();
        config1.setBootstrapIp("microsoft.com");
        config1.setBootstrapPort(80);
        try {
            new KademliaDisworkMap(config1);
            fail();
        } catch (DisworkFileSystemException e) {
            assertTrue(true);
        }
    }
}
