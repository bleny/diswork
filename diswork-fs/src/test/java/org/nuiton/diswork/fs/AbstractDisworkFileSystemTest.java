/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.FileUtil;


public abstract class AbstractDisworkFileSystemTest {
    
	/**
	 * a place to store files for the test it's a subdirectory of the OS temp
	 * dir e.g. under linux /tmp/disworkfs/tests/
	 */
	static protected String tempDirectoryPath =
	               System.getProperty("java.io.tmpdir", ".") // temp directory
			                           + "/disworkfs/tests";

	/**
	 * We will create a file at this path for test purpose
	 */
	static protected String randomFilePath = tempDirectoryPath + "/randomfile";

	/**
	 * The file will have this fixed size
	 */
	static protected int randomFileSize = 15 * 1000 * 1000;

	static protected DisworkFileSystem fileSystem;
	
	/**
	 * This setUp creates in a temp directory a file of size 
	 * {@link #randomFileSize} (fulfilling  it with random bytes)
	 * This file can be found at {@link #randomFilePath}
	 * At the end of the method {@link #fileSystem} is ready to be used
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
	    // create a temp directory for our test
		File tempDirectory = new File(tempDirectoryPath);
		tempDirectory.mkdir();

        // creating random data for the file
		Random random = new Random();
		byte[] randomBytes = new byte[randomFileSize];
		random.nextBytes(randomBytes);

		// dumping random data into the file
		File randomFile = new File(randomFilePath);
		FileUtils.writeByteArrayToFile(randomFile, randomBytes);

	}
	
	@After
	public void tearDown() throws Exception {
        fileSystem.close();
		// cleaning
		FileUtil.deleteRecursively(tempDirectoryPath);
	}
    
	/**
	 * writing a file at root directory should not raise any exception
	 * @throws Exception
	 */
    @Test
    public void testWrite() throws Exception {
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * First, write a file then test if exists return true.
     * @throws Exception
     */
    @Test
    public void testExists() throws Exception {
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", new FileInputStream(randomFilePath));
            assertTrue(fileSystem.exists("/my_file"));
            assertFalse(fileSystem.exists("/my_other_file"));
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * try to read a file that as never been created nor written
     */
    @Test
    public void testFailAtRead() throws Exception {
        try {
            fileSystem.read("/not_existing_file");
            fail();
        } catch (FileNotFoundException e) {
            assertTrue(true);
        }
    }
    
    /**
     * tests {@code org.nuiton.diswork.fs.storage.Storage.SplitBlocksInputStream}
     * by storing a "-1" byte, can be buggy due to the use of read()
     */
    @Test
    public void testSplit() throws Exception {
        
        byte[] bytes = new byte[1];
        bytes[0] = -0x1;
        
        InputStream source = null;
        try {
            source = new ByteArrayInputStream(bytes);
            fileSystem.write("/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        
        try {       
            source =  new ByteArrayInputStream(bytes);
            InputStream readResult = fileSystem.read("/my_file");
        
            int read = 0;
            byte[] b = new byte[1];
        
            read = readResult.read(b);

            assertEquals(1, read);
            assertArrayEquals(bytes, b);
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * writing a file at the root directory and reading it.
     * finally, compare original source and read result
     * byte-to-byte: contents should be equals  
     * @throws Exception
     */
    @Test
    public void testWriteRead() throws Exception {
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        
        
        // now, the checks. We read the original file and the result of
        // a read() and then compare it byte-to-byte

        InputStream readResult = null;
        try {
            source = new FileInputStream(randomFilePath);
            readResult = fileSystem.read("/my_file");

            assertEquals(randomFileSize, source.available());
            assertEquals(randomFileSize, readResult.available());
        
            byte[] sourceAsBytes = IOUtils.toByteArray(source);
            byte[] readResultAsBytes = IOUtils.toByteArray(readResult);
        
            assertArrayEquals(sourceAsBytes, readResultAsBytes);
        } finally {
            IOUtils.closeQuietly(source);
            IOUtils.closeQuietly(readResult);
        }
        
    }

    /**
     * this use case should raise an exception because my_folder
     * doesn't exists
     */
    @Test
    public void testFailAtWrite() throws Exception {
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
            fail();
        } catch (IllegalArgumentException e ){
            assertTrue(true);
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * this writing a file that already exists should not be a problem
     */
    @Test
    public void testDoubleWrite() throws Exception {
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", source);            
        } finally {
            IOUtils.closeQuietly(source);
        }

        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_file", source);            
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * This test uses mkdir to create dirs and sub-dirs
     * @throws Exception
     */
    @Test
    public void testCreateDirectory() throws Exception {
        fileSystem.createDirectory("/my_folder");
        assertTrue(fileSystem.exists("/my_folder"));
        fileSystem.createDirectory("/my_folder/my_sub_folder");
        assertTrue(fileSystem.exists("/my_folder/my_sub_folder"));
    }

    /**
     * Create some folders with mkdir and write files in those directories
     * @throws Exception
     */
    @Test
    public void testWriteInFolder() throws Exception {
        fileSystem.createDirectory("/my_folder");
        fileSystem.createDirectory("/my_folder/my_sub_folder");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_sub_folder/my_file", source);
            assertTrue(fileSystem.exists("/my_folder/my_sub_folder/my_file"));
        } finally {
            IOUtils.closeQuietly(source);
        }
    }
    
    /**
     * create a symbolic link to a file. This test show that we can read the
     * file using the link
     * @throws Exception
     */
    @Test
    public void testLinking() throws Exception {
        fileSystem.createDirectory("/my_folder");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        
        fileSystem.createSymbolicLink("/my_link", "/my_folder/my_file");
        
        InputStream readResult = null;
        try {
            source = new FileInputStream(randomFilePath);
            readResult = fileSystem.read("/my_link");
        
            boolean actualContentEquality =
                                    IOUtils.contentEquals(source, readResult);
            assertTrue(actualContentEquality);
        } finally {
            IOUtils.closeQuietly(source);
            IOUtils.closeQuietly(readResult);
        }        
    }
    
    /**
     * Trying to create a link to a wrong target, this sould raise an exception
     * @throws Exception
     */
    @Test
    public void testFailAtLinking() throws Exception {
        try {
            fileSystem.createSymbolicLink("/my_link", "/wrong_target_path");
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
    
    /**
     * Trying to remove files and directories
     * @throws Exception
     */
    @Test
    public void testRemove() throws Exception {
        fileSystem.createDirectory("/my_folder");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        fileSystem.delete("/my_folder/my_file");
        assertTrue(fileSystem.exists("/my_folder"));
        assertFalse(fileSystem.exists("/my_folder/my_file"));
        fileSystem.delete("/my_folder");
        assertFalse(fileSystem.exists("/my_folder"));
    }
    
    /**
     * By trying to remove a non-empty directory, this test should raise an
     * exception
     * @throws Exception
     */
    @Test
    public void testFailAtRemove() throws Exception {
        fileSystem.createDirectory("/my_folder");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }
        
        // trying to remove a non-empty directory should raise an exception
        try {
            fileSystem.delete("/my_folder");
            fail();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
    }
    
    /**
     * This tests uses ls
     */
    @Test
    public void testListDirectory() throws Exception {
        fileSystem.createDirectory("/my_folder");
        fileSystem.createDirectory("/my_folder/my_sub_dir");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/my_folder/my_file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }        
        
        fileSystem.createSymbolicLink("/my_folder/my_link", "my_file");
        
        List<String> lsResult = fileSystem.readDirectory("/my_folder");
        
        // checking that result contains all the required data
        assertTrue(lsResult.contains("my_sub_dir"));
        assertTrue(lsResult.contains("my_file"));
        assertTrue(lsResult.contains("my_link"));
        
        // ... and only those
        assertEquals(3, lsResult.size());
        
        lsResult = fileSystem.readDirectory("/");
        assertEquals(1, lsResult.size());
    }
    
    @Test
    public void testConcurrency() throws Exception {     
        fileSystem.createDirectory("/mydir");
        try {
            fileSystem.createDirectory("/myseconddir");
        } catch (ConcurrentModificationException e) {
            fail();
        }
    }
    
    @Test
    public void testLinks() throws Exception {
        fileSystem.createDirectory("/dir");     
        fileSystem.createDirectory("/dir/subdir");
        fileSystem.createDirectory("/otherdir");
        
        fileSystem.createSymbolicLink("/link", "dir/subdir");
        fileSystem.createSymbolicLink("/link/subsubdirlink", "/otherdir");
        List<String> readResult = fileSystem.readDirectory("/dir/subdir");
        assertEquals(1, readResult.size());
        assertTrue(readResult.contains("subsubdirlink"));
        
        fileSystem.createDirectory("/link/subsubdirlink/finaldir");
        readResult = fileSystem.readDirectory("/otherdir");
        assertEquals(1, readResult.size());
        assertTrue(readResult.contains("finaldir"));

        // tests that delete remove the link itself and not the target
        fileSystem.delete("/link/subsubdirlink");
        assertTrue(fileSystem.exists("/otherdir"));
        fileSystem.delete("/link");
        assertTrue(fileSystem.exists("/dir/subdir"));

        // testing the use of multiple links at the end of a path
        fileSystem.createSymbolicLink("/link1", "/dir");
        fileSystem.createSymbolicLink("/link2", "/link1");
        fileSystem.createSymbolicLink("/link3", "/link2");
        
        readResult = fileSystem.readDirectory("/link3");
        assertEquals(1, readResult.size());
        assertTrue(readResult.contains("subdir"));
    }
    
    @Test
    public void testMove() throws Exception {
        fileSystem.createDirectory("/dir");
        fileSystem.createDirectory("/dir/subdir");
        fileSystem.createDirectory("/dir/subdir/subsubdir");
        fileSystem.createSymbolicLink("/dir/link", "subdir");
        
        InputStream source = null;
        try {
            source = new FileInputStream(randomFilePath);
            fileSystem.write("/dir/file", source);
        } finally {
            IOUtils.closeQuietly(source);
        }

        fileSystem.createDirectory("/otherdir");
        fileSystem.move("/dir/subdir", "/otherdir/subdir");
        fileSystem.move("/dir/link", "/otherdir/link");
        fileSystem.move("/dir/file", "/otherdir/newfile");
        
        List<String> readDir = fileSystem.readDirectory("/dir");
        for (String c : readDir) {
            System.out.println(c);
        }
        assertTrue(readDir.isEmpty());
        
        readDir = fileSystem.readDirectory("/otherdir");
        assertEquals(3, readDir.size());
        
        assertTrue(fileSystem.exists("/otherdir/newfile"));
        assertTrue(fileSystem.exists("/otherdir/subdir/subsubdir"));

        // FIXME 20100610 bleny after moving a link, it still points the old target

        //assertTrue(fileSystem.exists("/otherdir/link/subsubdir"));
        /*
        readDir = fileSystem.readDirectory("/otherdir/link");
        for (String e : readDir) {
            System.out.println(e);
        }
        */
    }
    
    @Test
    public void failAtMove() throws Exception {
        fileSystem.createDirectory("/dir");
        fileSystem.createDirectory("/dir2");
        try {
            fileSystem.move("/dir", "/dir2");
        } catch (DisworkAlreadyExistsException e) {
            assertTrue(true);
        }
    }
    
    @Test
    public void testCreateDirectories() throws Exception {
        try {
            fileSystem.createDirectories("/dir/subdir/subsubdir");
            assertTrue(fileSystem.exists("/dir/subdir/subsubdir"));
        } catch (DisworkFileSystemException e) {
            fail();
        }
    }
    
    @Test
    public void testDeleteRecursively() throws Exception {
        try {
            fileSystem.createDirectories("/dir/subdir/subsubdir");
            fileSystem.createDirectory("/dir/subdir/other");
            fileSystem.createSymbolicLink("/dir/subdir/link", "/dir/subdir/other");
            fileSystem.deleteRecursively("/dir/subdir");
            assertEquals(0, fileSystem.readDirectory("/dir").size());
        } catch (DisworkFileSystemException e) {
            fail();
        }
    }
}
