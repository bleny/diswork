/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;

import org.junit.Before;
import org.junit.Test;

public class DisworkFileSystemInMemoryTest extends AbstractDisworkFileSystemTest {
    
    /**
     * this code executed after {@link AbstractDisworkFileSystemTest#setUp()}
     * @throws Exception
     */
    @Before
    public void setUpFileSystem() throws Exception {
        // finally, initiate the fileSystem
        DisworkFileSystemConfig disworkConfig = new DisworkFileSystemConfig();
        disworkConfig.setMapType("inmemory");
        fileSystem = new DisworkFileSystem(disworkConfig);
    }
    
    @Test
    public void testMemoryLeak() throws Exception {
        int initialSize = fileSystem.storage.getMap().size();

        fileSystem.createDirectory("/dir");
        fileSystem.write("/dir/file", new FileInputStream(randomFilePath));
        fileSystem.write("/dir/file", new FileInputStream(randomFilePath));
        fileSystem.createSymbolicLink("/dir/link", "/dir/file");

        fileSystem.delete("/dir/link");
        fileSystem.delete("/dir/file");
        fileSystem.delete("/dir");
        
        fileSystem.storage.clean();
        
        int finalSize = fileSystem.storage.getMap().size();
        assertEquals(initialSize, finalSize);
    }
    
}
