/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/* *##%
 * Copyright (c) 2010 poussin. All rights reserved.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *##%*/

package org.nuiton.diswork.fs.storage;


import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.nuiton.diswork.fs.storage.EntryUtil;

/**
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */
public class EntryUtilTest {

    static protected String directoryEntry =
        EntryUtil.TYPE.D + EntryUtil.ENTRY_SEPARATOR
        + "mydir" + EntryUtil.ENTRY_SEPARATOR
        + "myid1";
    
    static protected String fileEntry = 
        EntryUtil.TYPE.F + EntryUtil.ENTRY_SEPARATOR
        + "myfile" + EntryUtil.ENTRY_SEPARATOR
        + "myid2";
    
    static protected String linkEntry = 
        EntryUtil.TYPE.L + EntryUtil.ENTRY_SEPARATOR
        + "mylink" + EntryUtil.ENTRY_SEPARATOR
        + "myid3";
    
    static protected String metaBlock = 
          "12345" + EntryUtil.BLOCKIDS_SEPARATOR
        + "myid1" + EntryUtil.BLOCKIDS_SEPARATOR
        + "myid2" + EntryUtil.BLOCKIDS_SEPARATOR
        + "myid3";
    
    @Test
    public void testIsDirectory() {
        assertTrue(EntryUtil.isDirectory(directoryEntry));
        assertFalse(EntryUtil.isDirectory(fileEntry));
    }

    @Test
    public void testFind() {
        
        String content = directoryEntry + EntryUtil.ENTRIES_SEPARATOR
                       + fileEntry      + EntryUtil.ENTRIES_SEPARATOR
                       + linkEntry      + EntryUtil.ENTRIES_SEPARATOR;

        // seek and return entry
        String findResult;

        findResult = EntryUtil.findEntryInDirectory(content, "mydir");
        assertEquals(directoryEntry, findResult);
        findResult = EntryUtil.findEntryInDirectory(content, "myfile");
        assertEquals(fileEntry, findResult);
        findResult = EntryUtil.findEntryInDirectory(content, "mylink");
        assertEquals(linkEntry, findResult);
        
        
        // return null if file not found
        findResult = EntryUtil.findEntryInDirectory(content, "this_does_not_exists");
        assertEquals(null, findResult);
    }

    @Test
    public void testGetId() {
        assertEquals("myid1", EntryUtil.getIdFromEntry(directoryEntry));
        assertEquals("myid2", EntryUtil.getIdFromEntry(fileEntry));
        assertEquals("myid3", EntryUtil.getIdFromEntry(linkEntry));
    }

    @Test
    public void testGetName() {
        assertEquals("mydir", EntryUtil.getNameFromEntry(directoryEntry));
        assertEquals("myfile", EntryUtil.getNameFromEntry(fileEntry));
        assertEquals("mylink", EntryUtil.getNameFromEntry(linkEntry));

    }

    @Test
    public void testGetType() {
        assertEquals(EntryUtil.TYPE.D, EntryUtil.getTypeFromEntry(directoryEntry));
        assertEquals(EntryUtil.TYPE.F, EntryUtil.getTypeFromEntry(fileEntry));
        assertEquals(EntryUtil.TYPE.L, EntryUtil.getTypeFromEntry(linkEntry));
    }
    
    @Test
    public void testResolveLink() {
        String path;

        path = EntryUtil.resolveLink("/dir/subdir", "/anotherdir");
        assertEquals("/anotherdir", path);
        path = EntryUtil.resolveLink("/dir/subdir/", "anotherdir");
        assertEquals("/dir/subdir/anotherdir", path);
        path = EntryUtil.resolveLink("/dir/subdir/", "../anotherdir");
        assertEquals("/dir/anotherdir", path);
        path = EntryUtil.resolveLink("/dir/", "subdir/file");
        assertEquals("/dir/subdir/file", path);
    }
    
    @Test
    public void testBytesToArray() {
        String s = "abcdefg@^:éèàÉÈÀvwxyz,!;*$";        
        String copy = EntryUtil.bytesToString(EntryUtil.stringToBytes(s));
        assertEquals(s, copy);        
    }

    @Test
    public void testGetParentFromPath() {
        String path;
        path = "/file";
        assertEquals("/", EntryUtil.getParentFromPath(path));
        path = "/dir/file";
        assertEquals("/dir", EntryUtil.getParentFromPath(path));
    }
    
    @Test
    public void testGetTotalSizeFromMetaBlock() {
        assertEquals(12345, EntryUtil.getTotalSizeFromMetaBlock(metaBlock));
    }
    
    @Test
    public void testGetBlockIdsFromMetaBlock() {
        String[] expectedBlocksIds = {"myid1", "myid2", "myid3"};
        String[] actualBlocksIds = EntryUtil.getBlockIdsFromMetaBlock(metaBlock);
        assertArrayEquals(expectedBlocksIds, actualBlocksIds);
    }
    
    @Test
    public void testRemoveEntryFromEntries() {
        
        String content = directoryEntry + EntryUtil.ENTRIES_SEPARATOR
                       + fileEntry      + EntryUtil.ENTRIES_SEPARATOR
                       + linkEntry      + EntryUtil.ENTRIES_SEPARATOR;
        
        String expected = directoryEntry + EntryUtil.ENTRIES_SEPARATOR
                        + linkEntry      + EntryUtil.ENTRIES_SEPARATOR;
        
        String actual = EntryUtil.removeEntryFromEntries(content, "myfile");
        
        assertEquals(expected, actual);
        
    }
}
