/*
 * #%L
 * Diswork File-System
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractDisworkMapTest {
    
    private final Log log = LogFactory.getLog(AbstractDisworkMapTest.class);
    

    protected DisworkMap map1;
    protected DisworkMap map2;
    
    /**
     * should be overridden by method that set map1 and map2 with different
     * implementation of DisworkMap
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {  
    }
    
    @After
    public void tearDown() throws Exception {
        if (map1 != null)
            map1.close();
        if (map2 != null)
            map2.close();
    }
    
    /**
     * this test show that the InMemoryMap put implies a copy of the data.
     * After the put, the original value is modified. A final check shows
     * there is no side effect     
     */
    @Test
    public void testPutMakeCopy() {
        byte[] expected = {0x1, 0x2};        
        map1.put("key", expected);
        expected[0] = 0xf;
        byte[] actual = map1.get("key");
        assertFalse(Arrays.equals(expected, actual));
    }
    
    
    @Test
    public void testPutGet() throws Exception {        
        byte[] newBytes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        map1.put("test", newBytes);
        
        byte[] getResult = map1.get("test");
        assertArrayEquals(newBytes, getResult);
    }
    
    @Test
    public void test2nodes() throws Exception {
        
        byte[] newBytes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        log.info("trying put test");
        map1.put("test", newBytes);

        log.info("trying contains test");
        assertTrue(map2.containsKey("test"));
        log.info("trying contains unknownkey");
        assertFalse(map2.containsKey("unknownkey"));

        log.info("trying get test");
        byte[] getResult = map2.get("test");
        
        assertArrayEquals(newBytes, getResult);

        log.info("trying remove test");
        byte[] removeResult = map2.remove("test");
        assertArrayEquals(newBytes, removeResult);

        log.info("trying put test");
        map1.put("test", new byte[0]);
                
        log.info("trying put test");
        map2.put("test", new byte[1]);

        log.info("trying remove unknow");
        map2.remove("unknow");
        
    }
    
    @Test
    public void testStoreLargeValue() {

    	byte[] newBytes = new byte[11 * 1000 * 1000];
    	Random random = new Random();
    	random.nextBytes(newBytes);

    	log.debug("putting large value");
    	map1.put("test", newBytes);
    	log.debug("getting large value");
    	
    	byte[] getResult = map2.get("test");

    	assertNotNull(getResult);
    	assertEquals(newBytes.length, getResult.length);
        assertArrayEquals(newBytes, getResult);
        
    }
    
}
