/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;


import java.nio.charset.Charset;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;

/**
 * This class contains String utilities to ease operations on
 * <dl>
 *   <dt>a single entry;</dt>
 *     <dd>a record in the list of content in a directory</dd>
 *   <dt>multiple entries;</dt>
 *     <dd>a string containing multiple entry (as above) separated by
 *         {@link #ENTRIES_SEPARATOR}
 *      </dd>
 *   <dt>a metablock;</dt>
 *     <dd>a metablock is a String that begins with the total size of the data
 *         it describes and the ids where some blocks containing the data
 *         are stored into the map</dd>
 *   <dt>a path.</dt>
 *     <dd>a string like /a/b/c/d</dd>
 * </dl>
 * 
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by: $Author$
 */
public class EntryUtil {

    static public enum TYPE { /** Directory */ D,
                              /** Link      */ L,
                              /** File      */ F
                            };
    static final public int TYPE_LENGTH = 1;

    static final public String ENTRY_SEPARATOR = ":";
    static final public String ENTRIES_SEPARATOR = "\n";
    static final public String BLOCKIDS_SEPARATOR = ";";
    static final public String PATH_SEPARATOR = "/";
    static final public String ROOT_DIRECTORY = "/";
    static final public String EMPTY_DIRECTORY_CONTENT = "";
    static final public String LOCK_SEPARATOR = "%";
    
    static final public int ENTRY_SEPARATOR_LENGTH = ENTRY_SEPARATOR.length();

    /**
     * This charset is forced in string-to-bytes and bytes-to-string conversions 
     */
    static final protected Charset CHARSET = Charset.forName("UTF-8");

    /**
     * This is a utility class thus should not be instantiated 
     */
    protected EntryUtil() {};
    
    /**
     * generate a new Id usage as a new key for the map
     * @return a String containing the id
     */
    static public String generateId() {
        String result = UUID.randomUUID().toString();
        return result;
    }

    /**
     * @param entry a string taken from the entries of a directory
     * @return true if the entry is of type directory, false if it's a file or
     *         a link
     */
    static public boolean isDirectory(String entry) {
        boolean result = getTypeFromEntry(entry) == TYPE.D;
        return result;
    }

    /**
     * @param entry a string taken from the entries of a directory
     * @return true if the entry is of type link, false if it's a file or
     *         a directory
     */
    static public boolean isLink(String entry) {
        boolean result = getTypeFromEntry(entry) == TYPE.L;
        return result;
    }

    /**
     * @param entry a string taken from the entries of a directory
     * @return true if the entry is of type file, false if it's a link or
     *         a directory
     */
    static public boolean isFile(String entry) {
        boolean result = getTypeFromEntry(entry) == TYPE.F;
        return result;
    }

    static public TYPE getTypeFromEntry(String entry) {
        int index = entry.indexOf(ENTRY_SEPARATOR);
        String type = entry.substring(0, index);
        TYPE result = TYPE.valueOf(type);
        return result;
    }

    static public String getNameFromEntry(String entry) {
        int start = entry.indexOf(ENTRY_SEPARATOR);
        int last = entry.lastIndexOf(ENTRY_SEPARATOR);
        String result = entry.substring(start + ENTRY_SEPARATOR_LENGTH, last);
        return result;
    }

    static public String getIdFromEntry(String entry) {
        int index = entry.lastIndexOf(ENTRY_SEPARATOR);
        String result = entry.substring(index + ENTRY_SEPARATOR_LENGTH);
        return result;
    }
    

    /**
     * find file name in directory content description
     * 
     * @param directoryContent the entries of the directory to search in
     * @param name
     * @return the entry or null if name is not found
     */
    static public String findEntryInDirectory(String directoryContent,
                                              String name) {
        int index = directoryContent.indexOf(
                                      ENTRY_SEPARATOR + name + ENTRY_SEPARATOR);
        String result = null;
        if (index != -1) {
            result = directoryContent.substring(
                    index - TYPE_LENGTH, directoryContent.length());
            result = result.substring(0, result.indexOf(ENTRIES_SEPARATOR));
        }
        return result;
    }

    public static String removeEntryFromEntries(String content, String name) {
        String entry = findEntryInDirectory(content, name);
        String newContent = content.replaceFirst(entry + ENTRIES_SEPARATOR, "");
        return newContent;
    }

    /**
     * 
     * @param content the string content of the directory
     *        (all entries before add)
     * @param type the type of the new entry
     * @param name the name (not a full path) of the new entry
     * @param id the id where the content of the entry can be found
     * @return a String with the new content
     */
    static public String addEntryToDirectoryContent
                         (String content, TYPE type, String name, String id) {
        String newEntry = type.name() + ENTRY_SEPARATOR + name +
        ENTRY_SEPARATOR + id + ENTRIES_SEPARATOR;
        return content + newEntry;
    }

    static public int getTotalSizeFromMetaBlock(String metaBlock) {
        // dealing with empty meta-block (for empty directory or file)
        Integer result = null;
        if (metaBlock.equals("0")) {
            result = 0;
        } else {
            String resultAsString = metaBlock.substring(0,
                                       metaBlock.indexOf(BLOCKIDS_SEPARATOR)); 
            result = Integer.valueOf(resultAsString);
        }
        return result;
    }
    
    static public String[] getBlockIdsFromMetaBlock(String metaBlock) {
        // dealing with empty meta-block (for empty directory or file)
        if (metaBlock.indexOf(BLOCKIDS_SEPARATOR) == -1) {
            return new String[0];
        }
        String blockIdsAsString = metaBlock.substring(
                                    metaBlock.indexOf(BLOCKIDS_SEPARATOR) + 1,
                                    metaBlock.length());
        String[] blockIds = blockIdsAsString.split(BLOCKIDS_SEPARATOR);
        return blockIds;
    }

    /**
     * may be used to store strings in a byte[]. String can be found back
     * using {@link #bytesToString(byte[])}
     * @param string the string
     * @return a byte array containing all the bytes in <code>string</code>
     */
    public static byte[] stringToBytes(String string) {
        byte[] bytes = string.getBytes(CHARSET);
        return bytes;
    }
    
    /**
     * @see #stringToBytes(String)
     * @param bytes the bytes to convert
     * @return a byte array containing all the bytes in <code>string</code>
     */
    public static String bytesToString(byte[] bytes) {
        return new String(bytes, CHARSET);
    }

    /**
     * Resolve a path from a parent directory and a link 
     * 
     * ie resolve /dir/subdir, ../file returns /dir/file 
     * @param parent the path to the dir where start from compute the path
     * @param link the path to follow (may be absolute or relative to
     *        <code>parent</code>)
     * @return the path to the destination
     */
    static public String resolveLink(String parent, String link) {
        return FilenameUtils.concat(parent, link);
    }
    
    public static String getParentFromPath(String path) {
        String parent = FilenameUtils.getFullPathNoEndSeparator(path);
        if (parent.isEmpty()) {
            parent = ROOT_DIRECTORY;
        }
        return parent;
    }
    
    public static String getNameFromPath(String path) {
        return FilenameUtils.getName(path);
    }
    
    public static String getOwnerFromLock(byte[] lock) {
        String lockAsString = bytesToString(lock);
        String lockInfo[] = lockAsString.split(LOCK_SEPARATOR);
        String owner = lockInfo[0];
        return owner;
    }
    
    public static Long getTimeFromLock(byte[] lock) {
        String lockAsString = bytesToString(lock);
        String lockInfo[] = lockAsString.split(LOCK_SEPARATOR);
        Long lockAge = Long.parseLong(lockInfo[1]);
        return lockAge;        
    }
    
    public static byte[] newLock(String ownerId) {
        Long currentDate = System.currentTimeMillis();
        String lock = ownerId + LOCK_SEPARATOR + currentDate;
        byte[] result = EntryUtil.stringToBytes(lock);
        return result;
    }
    
}
