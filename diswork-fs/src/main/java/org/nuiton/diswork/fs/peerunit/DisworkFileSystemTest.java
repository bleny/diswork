/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.peerunit;

import static fr.inria.peerunit.test.assertion.Assert.assertEquals;
import static fr.inria.peerunit.test.assertion.Assert.assertTrue;
import static fr.inria.peerunit.test.assertion.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystem;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;

import fr.inria.peerunit.TestCaseImpl;
import fr.inria.peerunit.parser.TestStep;

public class DisworkFileSystemTest extends TestCaseImpl {
    
    private static final Log log =
                               LogFactory.getLog(DisworkFileSystemTest.class);
    
    // protected Integer port = 31000;
    
    protected DisworkFileSystem fileSystem;
    
    protected byte[] bytes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    
    @TestStep(range="0",timeout=1000000, order = 0)
    public void testConnect() throws Exception {
        DisworkFileSystemConfig config = new DisworkFileSystemConfig();
        config.setMapType("kademlia");
        config.setUsedPort(31000);
        fileSystem = new DisworkFileSystem(config);
        
        config.printConfig();
        
        log.info("i bootstrap : " + DisworkFileSystemConfig.getLocalIp() + ":" + 31000);
        put(0, DisworkFileSystemConfig.getLocalIp());
        put(1, 31000);
        
    }
    
    @TestStep(range="1",timeout=1000000, order = 1)
    public void testConnect1() throws Exception {
        // Integer myPort = port + getId();
        Integer myPort = 31001;
        DisworkFileSystemConfig config = new DisworkFileSystemConfig();
        config.setMapType("kademlia");
        config.setUsedPort(myPort);
        
        // get bootstrap info
        
        String bootstrapIp = (String) get(0);
        Integer bootstrapPort = (Integer) get(1);
        
        config.setBootstrapIp("127.0.0.1");
        config.setBootstrapPort(bootstrapPort);
        
        config.printConfig();
        
        log.info("bootstrap is " + bootstrapIp + ":" + bootstrapPort);
        
        fileSystem = new DisworkFileSystem(config);
    }
    
    @TestStep(range="0",timeout=1000000, order = 2)
    public void testWrite() throws Exception {
        InputStream source = new ByteArrayInputStream(bytes);
        fileSystem.write("/myfile", source);
        source.close();
    }
    
    @TestStep(range="1",timeout=1000000, order = 3)
    public void testRead() throws Exception {
        try {
            InputStream source = fileSystem.read("/myfile");
            byte[] readResult = IOUtils.toByteArray(source);
            // source.close();
            boolean arraysContentEquals = Arrays.equals(bytes, readResult);

            System.out.println("arraysContentEquals = " + arraysContentEquals);
            assertTrue(arraysContentEquals);
        } catch (FileNotFoundException e) {
            System.out.println("file not found");
            fail();
        }
    }
    
    @TestStep(range="1",timeout=1000000, order = 4)
    public void testCreateDir() throws Exception {
        fileSystem.createDirectory("/mydir");
    }
    
    @TestStep(range="0",timeout=1000000, order = 5)
    public void testCreateDir2() throws Exception {
        assertTrue(fileSystem.exists("/mydir"));
    }
    
    @TestStep(range="0",timeout=1000000, order = 6)
    public void testCreateSubDir() throws Exception {
        fileSystem.createDirectory("/mydir/mysubdir");
    }
    
    @TestStep(range="1",timeout=1000000, order = 7)
    public void testReadDir() throws Exception {
        assertTrue(fileSystem.exists("/mydir/mysubdir"));
        assertEquals(1, fileSystem.readDirectory("/mydir").size());
    }
    
}
