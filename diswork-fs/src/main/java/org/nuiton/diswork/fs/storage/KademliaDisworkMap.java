/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.diswork.fs.DisworkFileSystemException;
import org.planx.xmlstore.routing.Identifier;
import org.planx.xmlstore.routing.Kademlia;
import org.planx.xmlstore.routing.RoutingException;

public class KademliaDisworkMap implements DisworkMap {

    private static final Log log = LogFactory.getLog(KademliaDisworkMap.class);

    protected Kademlia kad;

    public KademliaDisworkMap(DisworkFileSystemConfig config)
                                           throws DisworkFileSystemException {

        try {
            log.info("booting on port " + config.getUsedPort());
            kad = new Kademlia(Identifier.randomIdentifier(), config.getUsedPort());
            
            if (config.getBootstrapIp() != null) {
                InetSocketAddress bootstrap = new InetSocketAddress(
                        config.getBootstrapIp(),
                        config.getBootstrapPort());
                try {
                    log.info("trying to connect to " + bootstrap);
                    kad.connect(bootstrap);
                } catch (RoutingException e) {
                    log.error("bootstrap node is unreachable", e);
                    throw new DisworkFileSystemException("bootstrap failure : "
                                          + "bootstrap node is unreachable", e);
                }
            }
            
            log.info("kademlia status : " + kad);
            
            //Identifier.IDSIZE = 1024;
            log.info("using " + Identifier.IDSIZE + " bytes for identifiers");
        } catch (IOException e) {
            throw new DisworkFileSystemException("failure while bootstraping", e);
        }
    }

    protected static Identifier stringToIdentifier(String s) {
        UUID uuid = UUID.nameUUIDFromBytes(EntryUtil.stringToBytes(s));
        s = uuid.toString();
        
        /*
        byte[] bytes = EntryUtil.stringToBytes(s);
        Identifier id = null;
        try {
            id = new Identifier(bytes);
        } catch (java.lang.IndexOutOfBoundsException e) {
            log.error("generated id is out of bound : Identifier.IDSIZE " + 
                    " is too small");
        }
        */
        
        InputStream in = IOUtils.toInputStream(s);
        DataInput dataInput = new DataInputStream(in);
        Identifier id = null;
        try {
            id = new Identifier(dataInput);
        } catch (IOException e) {
            log.error("unable to create key from string " + s);
        }
        

        log.debug("key for string " + s + " is " + id);
        return id;
    }
    
    @Override
    public void close() throws IOException {
        kad.close();
    }

    @Override
    public boolean containsKey(Object key) {
        Identifier id = stringToIdentifier((String) key);
        Boolean result = null;
        try {
            result = kad.contains(id);
        } catch (Exception e) {
            // FIXME 20100604 bleny bad exception management
            log.error("Kademlia exception caught", e);
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public byte[] get(Object key) {
        Identifier id = stringToIdentifier((String) key);
        byte[] result = null;
        try {
            result = (byte[]) kad.get(id);
        } catch (Exception e) {
            // FIXME 20100604 bleny bad exception management
            log.error("Kademlia exception caught", e);
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public byte[] put(String key, byte[] value) {
        byte[] previousValue = null;
        try {
            previousValue = get(key);
            Identifier id = stringToIdentifier((String) key);
            kad.put(id, value);
        } catch (Exception e) {
            // FIXME 20100604 bleny bad exception management
            log.error("Kademlia exception caught", e);
            throw new RuntimeException(e);
        }
        return previousValue;
    }

    @Override
    public byte[] remove(Object key) {
        byte[] previousValue = null;
        try {
            previousValue = get(key);
            Identifier id = stringToIdentifier((String) key);
            kad.remove(id);
        } catch (Exception e) {
            // FIXME 20100604 bleny bad exception management
            log.error("Kademlia exception caught", e);
            throw new RuntimeException(e);
        }
        return previousValue;
    }

    @Override
    @Deprecated
    public Collection<byte[]> values() {
        throw new UnsupportedOperationException("not yet implemented");
    }
    
    @Override
    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public boolean containsValue(Object arg0) {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public Set<java.util.Map.Entry<String, byte[]>> entrySet() {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public int size() {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public void putAll(Map<? extends String, ? extends byte[]> arg0) {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public Set<String> keySet() {
        throw new UnsupportedOperationException("not yet implemented");
    }

    @Override
    @Deprecated
    public boolean isEmpty() {
        throw new UnsupportedOperationException("not yet implemented");
    }
}
