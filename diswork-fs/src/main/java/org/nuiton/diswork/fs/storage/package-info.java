/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * <p>
 *   This package provides to {@link org.nuiton.diswork.fs.DisworkFileSystem} a
 *   way to persistently store data. This is done by the
 *   {@link org.nuiton.diswork.fs.storage.Storage} class
 *   which permit to store different type of data.
 * </p>
 * <p>
 *   We illustrate the use of a file system by setting the content of "/",
 *   the root directory as follow:
 * </p>
 * <ul>
 *   <li>
 *     a directory named "my_folder" that contains:
 *     <ul>
 *       <li>
 *         a directory named "my_sub_folder" that contains
 *         <ul>
 *           <li>a file name "my_deep_file.ext"</li>
 *           <li>an empty folder named "my_empty_folder"</li>
 *         </ul>       
 *       </li>
 *       <li>a file named "my_other_file.ext"</li>
 *     </ul>   
 *   </li>
 *   <li>
 *     a file named "my_file.ext" (containing binary data: 010101)
 *   </li>
 *   <li>
 *     a symbolic link named "my_link" which target the path 
 *     <code>"/my_folder/my_sub_folder/my_deep_file.ext"</code>
 *   </li>
 * </ul>
 * 
 * Storage uses a Map to store data.
 * 
 * <table>
 *   <caption>Use of map, abstract point of view</caption>
 *   <tr>
 *     <th>Key (String)</th>
 *     <th>Value (byte[])</th>
 *   </tr>
 *   <tr>
 *     <td>"/"</td>
 *     <td>"D:my_folder:ID1\n<br />
 *          F:my_file.ext:ID2\n<br />
 *          L:my_link:ID3"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID1"</td>
 *     <td>"D:my_sub_folder:ID4\n<br />
 *          F:my_other_file.ext:ID5"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID2"</td>
 *     <td>010101</td>
 *   </tr>
 *   <tr>
 *     <td>"ID3"</td>
 *     <td>"/my_folder/my_sub_folder/my_deep_file.ext"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID4"</td>
 *     <td>"F:my_deep_file:ID6\n<br />
 *          D:my_empty_folder:ID7"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID5"</td>
 *     <td>011010100111010...</td>
 *   </tr>
 *   <tr>
 *     <td>"ID6"</td>
 *     <td>1100010101010110110010...</td>
 *   </tr>
 *   <tr>
 *     <td>"ID7"</td>
 *     <td>""</td>
 *   </tr>
 * </table>
 * 
 * <p>
 *   An entry is, for example, "D:my_folder:ID1". It contains the type of the 
 *   entry (<strong>D</strong>irectory, <strong>F</strong>ile or
 *   <strong>L</strong>ink), the name of the element, and an ID to be used as
 *   a key on the map to get the actual content. Those three informations are
 *   separated by ":" which is
 *   {@link org.nuiton.diswork.fs.storage.EntryUtil#ENTRY_SEPARATOR}.
 * </p>
 * 
 * <p>
 *   A directory way have multiple <em>entries</em>. Entries of a directory are
 *   separated by "\n" which is
 *   {@link org.nuiton.diswork.fs.storage.EntryUtil#ENTRIES_SEPARATOR}.
 * </p>
 * <p>
 *   The above description shows the main principle used to store a tree
 *   structure in a map.
 * </p>
 * <p>
 *   In fact, the storage of actual data is a bit more complex, due to
 *   the need of splitting the data that may be too large (the value of "ID6"
 *   may be a series of 100 * 2<sup>20</sup> bytes for a 200 MiB file).
 *   Actually, files content (like ID2, ID5, ID6) and directory content
 *   values ("/", ID1, ID4) will be split and the map will look like that (
 *   <strong>note that only the files have been split for readability</strong>): 
 * </p>
 * <table>
 *   <caption>Use of map, concrete point of view</caption>
 *   <tr>
 *     <th>Key (String)</th>
 *     <th>Value (byte[])</th>
 *   </tr>
 *   <tr>
 *     <td>"/"</td>
 *     <td>"D:my_folder:ID1\n<br />
 *          F:my_file.ext:ID2\n<br />
 *          L:my_link:ID3"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID1"</td>
 *     <td>"D:my_sub_folder:ID4\n<br />
 *          F:my_other_file.ext:ID5"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID2"</td>
 *     <td>"6;ID8"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID3"</td>
 *     <td>"/my_folder/my_sub_folder/my_deep_file.ext"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID4"</td>
 *     <td>"F:my_deep_file:ID6\n<br />
 *          D:my_empty_folder:ID7"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID5"</td>
 *     <td>"14689;ID9"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID6"</td>
 *     <td>"79874567;ID10;ID11;ID12"</td>
 *   </tr>
 *   <tr>
 *     <td>"ID7"</td>
 *     <td>""</td>
 *   </tr>
 *   <tr>
 *     <td>"ID8"</td>
 *     <td>010101</td>
 *   </tr>
 *   <tr>
 *     <td>"ID9"</td>
 *     <td>011010100111010...</td>
 *   </tr>
 *   <tr>
 *     <td>"ID10"</td>
 *     <td>110001010101011...</td>
 *   </tr>
 *   <tr>
 *     <td>"ID11"</td>
 *     <td>111010110101101...</td>
 *   </tr>
 *   <tr>
 *     <td>"ID12"</td>
 *     <td>011101100111010...</td>
 *   </tr>
 * </table>
 * 
 * <p>
 *   Values of ID2, ID5, ID6 are no longer the file content. It's now a set
 *   of meta-information information data called <code>metablock</code>.
 *   A metablock is composed of the total size of the file followed by
 *   an ordered lists of IDs of the different blocks composing the file.
 *   Those informations are separated by ";" (see
 *   {@link org.nuiton.diswork.fs.storage.EntryUtil#BLOCKIDS_SEPARATOR}).
 * </p>
 * <p>
 *   In the above example:
 *   <ul>
 *     <li>
 *       "/my_file.ext" is a file which size is 6 bytes, it is
 *       composed of one block available at ID8;
 *     </li>
 *     <li>
 *       "/my_sub_folder/my_other_file.ext" is a file (size = 14689 bytes)
 *       which content is available at ID9;
 *     </li>
 *     <li>
 *       "/my_sub_folder/my_sub_folder/my_deep_file.ext" is a big file: its
 *       size is 79874567. It has been split in three blocks: ID10, ID11 and
 *       ID12.
 *     </li>
 *   </ul>
 * </p>
 * <p>
 *   When reading and writing in storage, split is done transparently. When
 *   reading, a Stream is returned: it loads data blocks after blocks
 *   when needed (see 
 *   {@link org.nuiton.diswork.fs.storage.Storage.SplitBlocksInputStream}).
 *   When writing, data are split in blocks of a maximum configurable size
 *   (see {@link org.nuiton.diswork.fs.DisworkFileSystemConfig}).
 * </p>
 */

package org.nuiton.diswork.fs.storage;