/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Demo {
    
    private static final Log log = LogFactory.getLog(Demo.class);

    protected static final String USAGE =  
        "Usage :\n"
      + "org.nuiton.disworkfs.Demo producer|consumer"
      + " usePort [bootStrapIp bootStrapPort]";
    
    protected static DisworkFileSystem fileSystem;

    protected static class Producer implements Runnable {

        protected String waitingForJob = null;
        protected Integer expectedResult = null;
        
        @Override
        public void run() {
            while (true) {
                try {
                    if (fileSystem.exists("/todo")) {
                        if (waitingForJob == null) {
                            Random random = new Random();
                            waitingForJob = "/todo/job-" + random.nextInt();
                            Integer randomInteger = random.nextInt();
                            expectedResult = randomInteger + 1;
                            String job = randomInteger.toString();
                            InputStream source = IOUtils.toInputStream(job);
                            log.info("writing job " + waitingForJob + " (" +
                                                         randomInteger + ")");
                            System.out.println("submited job " + randomInteger);
                            fileSystem.write(waitingForJob, source);
                            source.close();
                        } else {
                            String resultPath = waitingForJob + ".result";
                            if (fileSystem.exists(resultPath)) {
                                log.info("results has been published");
                                InputStream resultStream =
                                                  fileSystem.read(resultPath);
                                String resultString =
                                               IOUtils.toString(resultStream);
                                Integer result = Integer.parseInt(resultString);
                                log.info("result is " + result + " (expected : "
                                         + expectedResult + ")");
                                System.out.println("job result published "
                                                   + result);
                                fileSystem.delete(resultPath);
                                fileSystem.delete(waitingForJob);
                                waitingForJob = null;
                                expectedResult = null;
                            }
                        }
                    } else {
                        fileSystem.createDirectory("/todo");
                    }
                } catch (DisworkFileSystemException e) {
                    log.error(e);
                    System.out.println("error occured " + e);
                } catch (IOException e) {
                    log.error(e);
                    System.out.println("error occured " + e);
                }
                try {
                    Thread.sleep(15 * 1000);
                } catch (InterruptedException e) {
                    log.info("sleep interrupted", e);
                }
            }
        }
    }
    
    protected static class Consumer implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    if (fileSystem.exists("/todo")) {
                        List<String> todos = fileSystem.readDirectory("/todo");
                        if (todos.isEmpty()) {
                            log.info("nothing to do");
                            System.out.println("nothing to do");
                        } else {
                            // taking a random job
                            Random random = new Random();
                            String todo = todos.get(
                                                    random.nextInt(todos.size())
                                                   );
                            if (!todo.endsWith(".result")) {
                                String jobPath = "/todo/" + todo;
                                log.info("reading the job " + jobPath);
                                InputStream in = fileSystem.read(jobPath);
                                String operation = IOUtils.toString(in);
                                in.close();
                                log.info("operation to do " + operation);
                                System.out.println("job found "
                                                   + operation);
                                Integer i = Integer.parseInt(operation);
                                i += 1;
                                String result = i.toString();
                                System.out.println("published result "
                                        + result);
                                log.info("result is " + result);
                                InputStream source =
                                                IOUtils.toInputStream(result);
                                fileSystem.write(jobPath + ".result", source);
                                source.close();
                            }
                        }
                    } else {
                        fileSystem.createDirectory("/todo");
                    }
                } catch (IOException e) {
                    log.error(e);
                    System.out.println("error occured " + e);
                } catch (DisworkFileSystemException e) {
                    log.error(e);
                    System.out.println("error occured " + e);
                }
                try {
                    Thread.sleep(10 * 1000);
                } catch (InterruptedException e) {
                    log.info("sleep interrupted", e);
                    System.out.println("error occured " + e);
                }
            }            
        }
    }
    
    public static void printUsage() {
        System.out.println(USAGE);
    }
    
    /**
     * 
     * <dl>
     *   <dt>producer|consumer (required)</dt>
     *     <dd>a producer will produce operations todo, a consumer may read
     *         operations and try to execute them
     *     </dd>
     *   <dt>port (required)</dt>
     *     <dd>the port to use</dd>
     *   <dt>bootstrapip (optional)</dt>
     *     <dd>if not provided, the node will bootstrap itself, creating a
     *         new network. If provided, the node will first try to join a
     *         network</dd>
     *   <dt>bootstrap port (required if bootstrapip is set)</dt>
     *     <dd>the port used by the node to join</dd>
     *  </dl>
     *  
     *  Example :
     *  org.nuiton.disworkfs.Demo consumer 9001
     *  org.nuiton.disworkfs.Demo producer 9002 127.0.0.1 9001
     *  org.nuiton.disworkfs.Demo consumer 9003 127.0.0.1 9002
     *  org.nuiton.disworkfs.Demo producer 9004 127.0.0.1 9001
     *  org.nuiton.disworkfs.Demo consumer 9005 127.0.0.1 9003
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws Exception {
        if (args.length == 2) {
            DisworkFileSystemConfig config = new DisworkFileSystemConfig();
            config.setOption("diswork.fs.use_port", args[1]);
            fileSystem = new DisworkFileSystem(config);
            if ("producer".equals(args[0])) {
                Thread t = new Thread(new Producer());
                log.info("starting a producer");
                t.start();
            } else if ("consumer".equals(args[0])) {
                Thread t = new Thread(new Consumer());
                log.info("starting a consumer");
                t.start();
            }
        } else if (args.length == 4) {
            DisworkFileSystemConfig config = new DisworkFileSystemConfig();
            config.setOption("diswork.fs.use_port", args[1]);
            config.setOption("diswork.fs.bootstrap.ip", args[2]);
            config.setOption("diswork.fs.bootstrap.port", args[3]);
            fileSystem = new DisworkFileSystem(config);
            if ("producer".equals(args[0])) {
                Thread t = new Thread(new Producer());
                log.info("starting a producer");
                t.start();
            } else if ("consumer".equals(args[0])) {
                Thread t = new Thread(new Consumer());
                log.info("starting a consumer");
                t.start();
            }
        } else {
            printUsage();
        }
    }
}