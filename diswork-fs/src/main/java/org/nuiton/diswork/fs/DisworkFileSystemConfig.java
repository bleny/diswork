/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.ApplicationConfig;

/**
 * This object contains parameters used in {@link DisworkFileSystem}.
 * 
 * Available parameters are:
 * <dl>
 *   <dt>blocks_size</dt>
 *     <dd>
 *       The size of blocks to create when splitting big data in bytes
 *       (by default, 10 MiB)
 *     </dd>
 * </dl>
 * 
 * This class provides utilities to write tests easily.
 * 
 * You can get multiples diswork configs ready to use. All the instances uses
 * a new port and the local IP.
 * 
 * <pre>
 *    c = DisworkFileSystemConfig(); // create a config for a bootstrap node
 *    c2 = DisworkFileSystemConfig(c.getUsedPort()) // creates a config for a node that
 *                                                  // will bootstrap by joining the
 *                                                  // first node
 *    c3 = DisworkFileSystemConfig(c.getUsedPort())
 * </pre>
 */
public class DisworkFileSystemConfig extends ApplicationConfig {
    
    private static final Log log =
                             LogFactory.getLog(DisworkFileSystemConfig.class);
    
    protected static Integer port = 18999;
 
    /**
     * returns a new port, returned value change at each call.
     * @return
     */
    public static Integer getPort() {
        port += 1;
        return port;
    }
 
    /**
     * returns the IP on the local machine. Trying to get an public IP or a LAN
     * IP or the loopback IP if there is no other interface
     * @return
     */
    public static String getLocalIp() {
        InetAddress result = null;
        try {
            result = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            // LocalHost is always known
            log.error(e);
        }
        
        if (result.isLoopbackAddress()) {
            try {
                Socket temp = new Socket("microsoft.com", 80);
                result = temp.getLocalAddress();
                temp.close();
            } catch (IOException e) {
                log.warn("can't get external IP address");
            }
        }
        return result.getHostAddress();
    }
    
    public DisworkFileSystemConfig() {
        this(null);
    }
    
    public DisworkFileSystemConfig(Integer bootstrapPort) {
        this(bootstrapPort == null ? null : getLocalIp(), bootstrapPort);
    }
    
    public DisworkFileSystemConfig(String bootstrapIP, Integer bootstrapPort) {
        setDefaultOption("diswork.fs.blocks_size", "10485760"); // 10 MiB

        setDefaultOption("diswork.fs.map_type", "kademlia");
        setDefaultOption("diswork.fs.use_port", getPort().toString());
        
        if (bootstrapIP != null) {
            setOption("diswork.fs.bootstrap.ip", bootstrapIP);
            setOption("diswork.fs.bootstrap.port", bootstrapPort.toString());
        }
    }

    public void setBlockSize(Integer size) {
        setOption("diswork.fs.blocks_size", size.toString());
    }

    public int getBlockSize() {
        return getOptionAsInt("diswork.fs.blocks_size");
    }
    
    public Integer getUsedPort() {
        return getOptionAsInt("diswork.fs.use_port");
    }
    
    public void setUsedPort(Integer port) {
        setOption("diswork.fs.use_port", port.toString());
    }
    
    /**
     * 
     * @return null if none specified
     */
    public String getBootstrapIp() {
        String value = getOption("diswork.fs.bootstrap.ip");
        return "".equals(value) ? null : value;
    }
    
    public void setBootstrapIp(String ip) {
        setOption("diswork.fs.bootstrap.ip", ip);
    }

    public Integer getBootstrapPort() {
        return getOptionAsInt("diswork.fs.bootstrap.port");
    }
    
    public void setBootstrapPort(Integer port) {
        setOption("diswork.fs.bootstrap.port", port.toString());
    }
    
    public String getMapType() {
        return getOption("diswork.fs.map_type");
    }
    
    public void setMapType(String mapType) {
        setOption("diswork.fs.map_type", mapType);
    }
}
