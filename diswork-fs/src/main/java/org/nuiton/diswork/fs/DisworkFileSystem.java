/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.storage.DisworkMap;
import org.nuiton.diswork.fs.storage.EntryUtil;
import org.nuiton.diswork.fs.storage.Storage;

/** Main class of Diswork File System, provide methods for all operations 
 * You can use:
 * <ul>
 *   <li>{@link #createDirectory(String)} and {@link #readDirectory(String)}
 *       to create and browse directories</li>
 *   <li>{@link #write(String, InputStream)} and {@link #read(String)}
 *       to write a file and read it</li>
 *   <li>{@link #createSymbolicLink(String, String)} to create symbolic links</li>
 *   <li>{@link #exists(String)} and {@link #delete(String)} can be used
 *       on directories, files and symlinks</li>
 * </ul>
 * 
 * Those methods are not thread-safe, so manipulating the content of 
 * same directory in two different thread using the same instance may
 * lead to errors.
 * 
 */
public class DisworkFileSystem {
    
    public static class PathUtil {
        public static final String separator = EntryUtil.PATH_SEPARATOR;
        public static final String root = EntryUtil.ROOT_DIRECTORY;
        
        /** build an absolute path from its elements */ 
        static String path(String... elements) {
            String path = "";
            if (elements.length == 0) {
                path = root;
            } else {
                for(String element : elements) {
                    path += separator + element;
                }
                path.replaceFirst(separator, root);
            }
            return path;
        }
    }
    
    private static final Log log = LogFactory.getLog(DisworkFileSystem.class);

    /** storage will permit to save and read directories, files and links */
    protected Storage storage;
    
    /** number of try to acquire a lock before giving up */
    protected static final int LOCK_MAX_NUMBER_OF_TRY = 10;
    
    /** time (ms) to wait between two try to acquire a lock */
    protected static final int LOCK_WAIT = 10 * 1000; // ten seconds
    
    /** default constructor (uses default configuration parameters)
     * @throws DisworkFileSystemException caused by network issue
     */
    public DisworkFileSystem() throws DisworkFileSystemException {
        this(new DisworkFileSystemConfig());
    }
    
    /** constructor allowing to provide another configuration to use 
     * 
     * @param config the configuration
     * @throws DisworkFileSystemException caused by network issue
     */
    public DisworkFileSystem(DisworkFileSystemConfig config)
                                           throws DisworkFileSystemException {
        storage = new Storage(config);
    }
    
    /** tests the existence of a file/dir/link at a given path
     * return true if something exists at path p, it will be true if a call
     * to {@link #createDirectory(String)}, {@link #write(String, InputStream)}
     * or {@link #createSymbolicLink(String, String)} has been done before. 
     * @param path a path in the virtual FS
     * @return true is something (a link, a file, or a directory) exists at path 
     * @throws DisworkFileSystemException
     */
    public boolean exists(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        String entry = walk(path);
        boolean result = entry != null;
        return result;
    }

    /**
     * use this method to read the content of a file. A call of read may be
     * done after a call to {@link #write}
     * @param path the path to the file to read
     * @return an InputStream on the file
     * @throws FileNotFoundException if no file exists at this path
     * @throws DisworkFileSystemException if path exists but is a directory
     */
    public InputStream read(String path) throws DisworkFileSystemException,
                                                FileNotFoundException {
        checkPathSyntax(path);

        String entry = walk(path);
        if (entry == null) {
            throw new FileNotFoundException(path);
        }
        
        InputStream result = null;
                
        if (EntryUtil.isLink(entry)) {
            log.info("reading link " + path);
            String id = EntryUtil.getIdFromEntry(entry);
            String link = storage.getLink(id);
            String newTarget = EntryUtil.resolveLink(path, link);
            result = read(newTarget);
        } else if (EntryUtil.isDirectory(entry)) {
            throw new IllegalArgumentException(path + " is not a file but a directory");
        } else if (EntryUtil.isFile(entry)) {
            log.info("reading file " + path);
            String id = EntryUtil.getIdFromEntry(entry);
            result = storage.getFile(id);
        }
        
        return result;
    }

    /**
     * write a file on the file system at a given place
     * @param path
     * @param source
     * @throws DisworkFileSystemException if file already exists
     * @throws ConcurrentModificationException if file is already being written
     */
    public void write(String path, InputStream source)
                                           throws DisworkFileSystemException {
        checkPathSyntax(path);
        if (source == null) {
            throw new NullPointerException("source stream is null");
        }
        String parent = EntryUtil.getParentFromPath(path);
        String name = EntryUtil.getNameFromPath(path);
        write(parent, name, source);
    }

    protected void write(String parent, String fileName, InputStream source)
                                           throws DisworkFileSystemException {
        String entryParent = walk(parent);
        
        if (entryParent == null) {
            throw new IllegalArgumentException(parent + " directory doesn't exists");
        }
        
        if (EntryUtil.isDirectory(entryParent)) {
            String parentId = EntryUtil.getIdFromEntry(entryParent);

            
            // file do not exists, write file on the FS
            String newFileId = EntryUtil.generateId();
            storage.putFile(newFileId, source);
            
            // update directory content
            int numberOfTry = 0;
            boolean lockAcquired = false;
            while (numberOfTry <= LOCK_MAX_NUMBER_OF_TRY && !lockAcquired) {
                lockAcquired = storage.tryToLock(parentId);
                if (lockAcquired) {
                    // parent dir is locked, do the update

                    // checking that file do not already exists in this directory
                    String content = storage.getDirectory(parentId);
                    String oldEntry = EntryUtil.findEntryInDirectory
                                                            (content, fileName);
                    
                    if (oldEntry != null) {
                        content = EntryUtil.removeEntryFromEntries(content, fileName);
                    }
                    
                    content = EntryUtil.addEntryToDirectoryContent(
                                content, EntryUtil.TYPE.F, fileName, newFileId);
                    storage.putDirectory(parentId, content);
                    storage.unLock(parentId);
                    
                    // removing old data
                    if (oldEntry != null) {
                        storage.removeFile(EntryUtil.getIdFromEntry(oldEntry));
                    }
                    
                } else {
                    log.info(parent + " is locked and can't be written");
                    try {
                        Thread.sleep(LOCK_WAIT);
                    } catch (InterruptedException e) {
                        log.info("wait for lock interrupted", e);
                        throw new DisworkFileSystemException(
                                 "interrupted while trying to acquire lock", e);
                    }
                }
            }
            
            if (!lockAcquired) {
                // fail, parent dir have not been written
                throw new ConcurrentModificationException
                           ("can't write " + parent + " directory is locked");
            }
        } else if (EntryUtil.isLink(entryParent)) {
            String linkTarget = storage.getLink(
                    EntryUtil.getIdFromEntry(entryParent));
            String newTarget = EntryUtil.resolveLink(parent, linkTarget);
            write(newTarget, fileName, source);
        } else if (EntryUtil.isFile(entryParent)) {
            throw new IllegalArgumentException(parent + " is not a directory");
        } else {
            log.warn("strange entry" + entryParent);
            throw new DisworkFileSystemException("strange entry" + entryParent);
        }
    }

    /**
     * creates a new empty directory
     * @param path the absolute path (ending by the name) of the directory
     * @throws DisworkFileSystemException if parent path is not correct
     */
    public void createDirectory(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        String parent = EntryUtil.getParentFromPath(path);
        String dirName = EntryUtil.getNameFromPath(path);
        createDirectory(parent, dirName);
    }
    
    protected void createDirectory(String parent, String dirName)
                                           throws DisworkFileSystemException {
        
        log.info("trying to create directory " + dirName + " in " + parent);
        
        String entryParent = walk(parent);
        
        if (entryParent == null) {
            throw new IllegalArgumentException(parent + " directory doesn't exists");
        }
        
        if (EntryUtil.isDirectory(entryParent)) {
            String parentId = EntryUtil.getIdFromEntry(entryParent);
            String content = storage.getDirectory(parentId);

            // first, check if nothing already exists with this name 
            String findResult = EntryUtil.findEntryInDirectory
                                                          (content, dirName);
            if (findResult != null) {
                throw new DisworkAlreadyExistsException(parent
                             + " already contains an element named " + dirName);
            }
            
            // store file before meta info
            String newDirectoryId = EntryUtil.generateId();
            storage.putDirectory(newDirectoryId,
                                          EntryUtil.EMPTY_DIRECTORY_CONTENT);
            
            // update directory content
            int numberOfTry = 0;
            boolean lockAcquired = false;
            while (numberOfTry <= LOCK_MAX_NUMBER_OF_TRY && !lockAcquired) {
                lockAcquired = storage.tryToLock(parentId);
                if (lockAcquired) {
                    // we have locked, do the update
                    content = storage.getDirectory(parentId);
                    String newContent = EntryUtil.addEntryToDirectoryContent(
                            content, EntryUtil.TYPE.D, dirName, newDirectoryId);
                    storage.putDirectory(parentId, newContent);
                    storage.unLock(parentId);
                } else {
                    log.info(parent + " is locked and can't be written");
                    try {
                        Thread.sleep(LOCK_WAIT);
                    } catch (InterruptedException e) {
                        log.info("wait for lock interrupted", e);
                        throw new DisworkFileSystemException(
                                 "interrupted while trying to acquire lock", e);
                    }
                }
            }
            
            if (!lockAcquired) {
                // fail, parent dir have not been written
                throw new ConcurrentModificationException
                           ("can't write " + parent + " directory is locked");
            }
        } else if (EntryUtil.isLink(entryParent)) {
            String linkTarget = storage.getLink(
                    EntryUtil.getIdFromEntry(entryParent));
            String newTarget = EntryUtil.resolveLink(parent, linkTarget);
            createDirectory(newTarget, dirName);
        } else if (EntryUtil.isFile(entryParent)) {
            throw new IllegalArgumentException(parent + " is not a directory");
        } else {
            log.warn("strange entry" + entryParent);
            throw new DisworkFileSystemException("strange entry" + entryParent);
        }
    }

    /**
     * create a new symbolic link
     * @param path the path (ending by the name) where the link will be created
     * @param target the path where the link point to
     *        (may be relative or absolute)
     * @throws DisworkFileSystemException
     */
    public void createSymbolicLink(String path, String target)
                                           throws DisworkFileSystemException {
        if (exists(path)) {
            throw new DisworkAlreadyExistsException(path);
        }
        checkPathSyntax(path);
        String parent = EntryUtil.getParentFromPath(path);
        String name = EntryUtil.getNameFromPath(path);
        createSymbolicLink(parent, name, target);        
    }
    
    /**
     * @see #createSymbolicLink(String, String)
     */
    protected void createSymbolicLink(String parent, String name, String target)
                                           throws DisworkFileSystemException {
        
        if (target.startsWith(EntryUtil.PATH_SEPARATOR)) {
            // target is absolute
        } else {
            // target is relative, taking this in consideration
            target = EntryUtil.resolveLink(parent, target);
        }
        
        log.info("trying to create symbolic link named " + name + " at path " +
                 parent + " with target \"" + target + "\"");
        
        if (exists(target)) {
            
            String entryParent = walk(parent);
            
            if (entryParent == null) {
                throw new IllegalArgumentException(parent + " doesn't exists");
            }
            
            if (EntryUtil.isDirectory(entryParent)) {

                String parentId = EntryUtil.getIdFromEntry(entryParent);
                String content = storage.getDirectory(parentId);

                // first, check if nothing already exists with this name 
                String findResult = EntryUtil.findEntryInDirectory
                                                              (content, name);
                if (findResult != null) {
                    throw new DisworkFileSystemException(
                        parent + " already contains an element named " + name);
                }

                // store file before meta info
                String newLinkId = EntryUtil.generateId();
                storage.putLink(newLinkId, target);

                // update directory content
                int numberOfTry = 0;
                boolean lockAcquired = false;
                while (numberOfTry <= LOCK_MAX_NUMBER_OF_TRY && !lockAcquired) {
                    lockAcquired = storage.tryToLock(parentId);
                    if (lockAcquired) {
                        // we have locked, do the update
                        content = storage.getDirectory(parentId);
                        String newContent = EntryUtil.addEntryToDirectoryContent(
                                content, EntryUtil.TYPE.L, name, newLinkId);
                        storage.putDirectory(parentId, newContent);
                        storage.unLock(parentId);
                    } else {
                        log.info(parent + " is locked and can't be written");
                        try {
                            Thread.sleep(LOCK_WAIT);
                        } catch (InterruptedException e) {
                            log.info("wait for lock interrupted", e);
                            throw new DisworkFileSystemException( 
                                 "interrupted while trying to acquire lock", e);
                        }
                    }
                }

                if (!lockAcquired) {
                    // fail, parent dir have not been written
                    throw new ConcurrentModificationException
                             ("can't write " + parent + " directory is locked");
                }

            } else if (EntryUtil.isLink(entryParent)) {
                String linkTarget = storage.getLink(
                                         EntryUtil.getIdFromEntry(entryParent));
                String newTarget = EntryUtil.resolveLink(parent, linkTarget);
                createSymbolicLink(newTarget, name, target);
            } else if (EntryUtil.isFile(entryParent)) {
                throw new IllegalArgumentException(parent + " is not a directory");
            } else {
                log.warn("strange entry" + entryParent);
                throw new DisworkFileSystemException(
                                                 "strange entry" + entryParent);
            }
        } else {
            throw new IllegalArgumentException(target + " is not a valid " + 
                                               "target (do not exists)");
        }
    }

    /**
     * remove a file, directory, or link. Non-empty directories can't be
     * removed.
     * @param path the complete path to the entity to remove
     * @throws DisworkFileSystemException
     */
    public void delete(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        if (!exists(path)) {
            throw new DisworkFileSystemException(path + " doesn't not exists");
        }
        String parent = EntryUtil.getParentFromPath(path);
        String name = EntryUtil.getNameFromPath(path);
        log.info("trying to delete " + path);
        delete(parent, name);
    }
    
    /**
     * @see #delete(String)
     */
    protected void delete(String parent, String name)
                                           throws DisworkFileSystemException {
        String entryParent = walk(parent);
        
        if (EntryUtil.isDirectory(entryParent)) {
            String parentId = EntryUtil.getIdFromEntry(entryParent);
            String content = storage.getDirectory(parentId);
            
            String entry = EntryUtil.findEntryInDirectory(content, name);
            String idToRemove = EntryUtil.getIdFromEntry(entry);
            
            // according to the type of the entry name, delete it 
            if (EntryUtil.isDirectory(entry)) {
                // check if not removing a non-empty directory
                String innerDirectoryId = EntryUtil.getIdFromEntry(entry);
                String innerDirectoryContent =
                                       storage.getDirectory(innerDirectoryId);
                // checking the emptiness of the directory
                if (!innerDirectoryContent.equals(
                                      EntryUtil.EMPTY_DIRECTORY_CONTENT)) {
                    // directory is not empty
                    throw new IllegalArgumentException( 
                                   "trying to remove a non-empty directory");
                }

                // remove it
                storage.removeDirectory(idToRemove);
                
            } else if (EntryUtil.isFile(entry)) {
                storage.removeFile(idToRemove);
            } else if (EntryUtil.isLink(entry)) {
                storage.removeLink(idToRemove);
            } else {
                log.warn("strange entry" + entryParent);
                throw new DisworkFileSystemException(
                                               "strange entry" + entryParent);
            }
            
            
            // update directory content
            int numberOfTry = 0;
            boolean lockAcquired = false;
            while (numberOfTry <= LOCK_MAX_NUMBER_OF_TRY && !lockAcquired) {
                lockAcquired = storage.tryToLock(parentId);
                if (lockAcquired) {
                    // we have locked, do the update
                    content = storage.getDirectory(parentId);
                    String newContent = EntryUtil.removeEntryFromEntries(content, name);
                    storage.putDirectory(parentId, newContent);
                    storage.unLock(parentId);
                } else {
                    log.info(parent + " is locked and can't be written");
                    try {
                        Thread.sleep(LOCK_WAIT);
                    } catch (InterruptedException e) {
                        log.info("wait for lock interrupted", e);
                        throw new DisworkFileSystemException(
                                "interrupted while trying to acquire lock", e);
                    }
                }
            }

            if (!lockAcquired) {
                // fail, parent dir have not been written
                throw new ConcurrentModificationException
                         ("can't write " + parent + " directory is locked");
            }
            
        } else if (EntryUtil.isLink(entryParent)) {
            String linkTarget = storage.getLink(
                    EntryUtil.getIdFromEntry(entryParent));
            String newTarget = EntryUtil.resolveLink(parent, linkTarget);
            delete(newTarget, name);
        } else if (EntryUtil.isFile(entryParent)) {
            throw new IllegalArgumentException(parent + " is not a directory");
        } else {
            log.warn("strange entry" + entryParent);
            throw new DisworkFileSystemException("strange entry" + entryParent);
        }
    }

    /**
     * list the content of a directory
     * @param path the complete path to the directory
     * @return a list of the names of the elements in <code>path</code>
     * @throws DisworkFileSystemException
     */
    public List<String> readDirectory(String path)
                                           throws DisworkFileSystemException {
        checkPathSyntax(path);
        String entry = walk(path);
        List<String> result = null;
        
        if (entry == null) {
            throw new IllegalArgumentException(path + " directory doesn't exists");
        } else {
            // path may be a link, if it's the case,
            // entry become the actual directory
            if (EntryUtil.isLink(entry)) {
                String target = storage.getLink(
                                             EntryUtil.getIdFromEntry(entry));
                return readDirectory(target);
            } else if (EntryUtil.isDirectory(entry)) {
                result = new ArrayList<String>();
                String content = storage.getDirectory(
                                             EntryUtil.getIdFromEntry(entry));
                if (EntryUtil.EMPTY_DIRECTORY_CONTENT.equals(content)) {
                    // directory is empty, add nothing
                } else {
                    String[] entries = content.split(
                                                 EntryUtil.ENTRIES_SEPARATOR);
                    for (String elementEntry : entries) {
                        result.add(EntryUtil.getNameFromEntry(elementEntry));
                    }
                }
            } else if (EntryUtil.isFile(entry)) {
                throw new IllegalArgumentException(path + " is not a directory but a file");
            } else {
                log.warn("strange entry" + entry);
                throw new DisworkFileSystemException("strange entry" + entry);
            }
        }
        
        log.debug("readDirectory " + path + " returns " + result.size() + " results");
        
        return result;
    }

    /**
     * move a file, a directory a link. May be used for renaming purpose
     * @param path the path to the object to move
     * @param destination the full path of the target
     * @throws DisworkFileSystemException 
     */
    public void move(String path, String destination)
                                           throws DisworkFileSystemException {
        checkPathSyntax(path);
        checkPathSyntax(destination);

        if (exists(destination)) {
            throw new DisworkAlreadyExistsException(destination + " already exists");
        }
        
        String pathParent = EntryUtil.getParentFromPath(path);
        String pathName = EntryUtil.getNameFromPath(path);
        String destinationParent = EntryUtil.getParentFromPath(destination);
        String destinationName = EntryUtil.getNameFromPath(destination);
     
        move(pathParent, pathName, destinationParent, destinationName);
        
    }
     
    protected void move(String pathParent, String pathName,
                        String destinationParent, String destinationName)
                                           throws DisworkFileSystemException {
        
        String entryParent = walk(pathParent);
        
        if (entryParent == null) {
            throw new IllegalArgumentException(pathParent + " doesn't exists");
        }
        
        if (EntryUtil.isDirectory(entryParent)) {

            String destinationParentEntry = walk(destinationParent);
            if (destinationParentEntry == null) {
                throw new IllegalArgumentException(destinationParent + " doesn't exists");
            }
            
            if (EntryUtil.isDirectory(destinationParentEntry)) {
              
                // we are now in actual directories for old path and destination
                
                String parentContent = storage.getDirectory(
                                           EntryUtil.getIdFromEntry(entryParent));
                String oldEntry = EntryUtil.findEntryInDirectory(parentContent,
                                                                 pathName);
                if (oldEntry == null) {
                    throw new IllegalArgumentException("no element " + pathName + " in " + pathParent);
                }

                String parentId = EntryUtil.getIdFromEntry(entryParent);
                String destinationParentId = EntryUtil.getIdFromEntry(destinationParentEntry);
                // update directory content
                int numberOfTry = 0;
                boolean lockAcquired = false;
                while (numberOfTry <= LOCK_MAX_NUMBER_OF_TRY && !lockAcquired) {
                    lockAcquired = storage.tryToLock(parentId)
                                && storage.tryToLock(destinationParentId);
                    
                    if (lockAcquired) {
                        // we have locked, do the update
                        parentContent = storage.getDirectory(parentId);
                        String newContent = EntryUtil.removeEntryFromEntries(parentContent, pathName);
                        storage.putDirectory(parentId, newContent);
                        

                        String destinationParentContent = storage.getDirectory(destinationParentId);
                        newContent = EntryUtil.addEntryToDirectoryContent(destinationParentContent,
                                EntryUtil.getTypeFromEntry(oldEntry),
                                destinationName,
                                EntryUtil.getIdFromEntry(oldEntry));
                        storage.putDirectory(destinationParentId, newContent);
                        
                        storage.unLock(parentId);
                        storage.unLock(destinationParentId);
                    } else {
                        log.info(pathParent + " is locked");
                        try {
                            Thread.sleep(LOCK_WAIT);
                        } catch (InterruptedException e) {
                            log.info("wait for lock interrupted", e);
                            throw new DisworkFileSystemException(
                                 "interrupted while trying to acquire lock", e);
                        }
                    }
                }

                if (!lockAcquired) {
                    // fail, parent dir have not been written
                    throw new ConcurrentModificationException
                    ("can't write " + pathParent + " directory is locked");
                }
                
            } else if (EntryUtil.isLink(destinationParentEntry)) {
                String linkTarget = storage.getLink(
                        EntryUtil.getIdFromEntry(destinationParentEntry));
                String newTarget = EntryUtil.resolveLink(destinationParent, linkTarget);
                move(newTarget, pathName, destinationParent, destinationName);
            } else if (EntryUtil.isFile(destinationParentEntry)) {
                throw new IllegalArgumentException(destinationParent + " is not a directory");
            } else {
                log.warn("strange entry" + destinationParentEntry);
                throw new DisworkFileSystemException("strange entry" +
                                                     destinationParentEntry);
            }
            
        } else if (EntryUtil.isLink(entryParent)) {
            String linkTarget = storage.getLink(
                    EntryUtil.getIdFromEntry(entryParent));
            String newTarget = EntryUtil.resolveLink(pathParent, linkTarget);
            move(pathParent, pathName, newTarget, destinationName);
        } else if (EntryUtil.isFile(entryParent)) {
            throw new IllegalArgumentException(pathParent + " is not a directory");
        } else {
            log.warn("strange entry" + entryParent);
            throw new DisworkFileSystemException("strange entry" + entryParent);
        }
        
    }
    
    public boolean isLink(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        String entry = walk(path);
        if (entry == null) {
            throw new IllegalArgumentException(path + " doesn't not exists");
        }
        return EntryUtil.isLink(entry);        
    }

    public boolean isFile(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        String entry = walk(path);
        if (entry == null) {
            throw new IllegalArgumentException(path + " doesn't not exists");
        }
        return EntryUtil.isFile(entry);        
    }

    public boolean isDirectory(String path) throws DisworkFileSystemException {
        checkPathSyntax(path);
        String entry = walk(path);
        if (entry == null) {
            throw new IllegalArgumentException(path + " doesn't not exists");
        }
        return EntryUtil.isDirectory(entry);        
    }

    public void createDirectories(String path)
                                           throws DisworkFileSystemException {
        log.info("trying create directories for " + path);
        
        String pathWithoutRoot = path.substring(1, path.length());
        
        String[] dirs = pathWithoutRoot.split(EntryUtil.PATH_SEPARATOR);
        String dirPath = "";
        for (String dir : dirs) {
            dirPath += EntryUtil.PATH_SEPARATOR + dir;
            if (!exists(dirPath)) {
                createDirectory(dirPath);
            }
        }
    }

    public void deleteRecursively(String path) throws DisworkFileSystemException {
        if (isDirectory(path)) {
            List<String> directoryContent = readDirectory(path);
            for (String contentName : directoryContent) {
                deleteRecursively(path + EntryUtil.PATH_SEPARATOR + contentName);
            }
        }
        delete(path);
    }

    /**
     * return the entry of the element at the end of <code>path</code>
     * @param path
     * @return null if path is not valid
     */
    protected String walk(String path) throws DisworkFileSystemException {
        String result = walk(path, null, null);
        log.debug("walking to " + path + " returns " + result);
        return result;
    }

    /**
     * This method is a recursive function to walk through the tree structure
     * of the directories and starting for root directory, following the 
     * symbolic links to reach the given path
     * @param path
     * @param current
     * @param content
     * @return null if path is not valid or the entry corresponding to path
     * @throws DisworkFileSystemException
     */
    protected String walk(String path, String current, String content)
                                           throws DisworkFileSystemException {
        // FIXME 20105021 bleny works fine but is not understandable
        String result = null;
        
        // if path is "/", recursion can be initiated with this value :
        // (it returns "/" as the id where to get the content of "/"
        if (path.equals(EntryUtil.ROOT_DIRECTORY)) {
            return EntryUtil.TYPE.D + EntryUtil.ENTRY_SEPARATOR
                    + EntryUtil.ROOT_DIRECTORY + EntryUtil.ENTRY_SEPARATOR
                    + EntryUtil.ROOT_DIRECTORY;
        }

        String parentPath = EntryUtil.getParentFromPath(path);

        if (content == null) {
            // start the recursion from root directory
            content = storage.getRootDirectory();
            result = walk(path, EntryUtil.ROOT_DIRECTORY, content);
        } else if (parentPath.equals(current)) {
            // we are now in the last directory
            // ie if path is a/b/p we are at a/b
            
            // p is the name of the element in a/b we search for 
            String tail = path.substring(current.length());
            String p = EntryUtil.getNameFromPath(tail);
            
            log.trace("in final dir " + current + ", looking for " + p);
            
            String entry = EntryUtil.findEntryInDirectory(content, p);
            result = entry;
        } else {
            // in middle of path
            // if path is a/b/c/d/e/f, we may be at b, c, d...
            
            String tail; // the path still remaining when in current
            // ie if we are in c, tail is "d/e/f"
            
            // if we are at root directory, deal with the "/"
            if (current.equals(EntryUtil.ROOT_DIRECTORY)) {
                tail = path.substring(current.length());
            } else {
                tail = path.substring(current.length() + 1);
            }
            
            log.trace("current = " + current);
            log.trace("tail = " + tail);
            String[] elementsNames = tail.split(EntryUtil.PATH_SEPARATOR);
            String p = elementsNames[0];

            log.trace("in intermediate dir " + current + ", looking for " + p);
            
            // updating current for recursion
            if (current.equals(EntryUtil.ROOT_DIRECTORY)) {
                current = ""; // avoid "//path" next line 
            }
            current += EntryUtil.PATH_SEPARATOR + p;

            String entry = EntryUtil.findEntryInDirectory(content, p);
            if (entry == null) {
                result = null;
            } else {
                // we have found the entry to call recursion
                
                if (EntryUtil.isDirectory(entry)) {
                    String id = EntryUtil.getIdFromEntry(entry);
                    content = storage.getDirectory(id);
                    result = walk(path, current, content);
                } else if (EntryUtil.isLink(entry)) {
                    String id = EntryUtil.getIdFromEntry(entry);
                    String linkContent = storage.getLink(id);
                    String newTarget = 
                                  EntryUtil.resolveLink(current, linkContent);
                    newTarget += path.substring(current.length());

                    // restart walk from /
                    result = walk(newTarget, null, null);
                } else if (EntryUtil.isFile(entry)) {
                    // error, found file in path like '/dir1/dir2/filename/dir3'
                    result = null;
                } else {
                    log.warn("strange case: " + entry);
                    result = null;
                }
            }
        }
        return result;
    }

    public void close() throws DisworkFileSystemException {
        storage.close();
    }    
 
    protected void setMap(DisworkMap map) {
        storage.setMap(map);
    }
    
    /**
     * check a path is absolute and syntactically correct, throw exception if
     * that's not the case. 
     */
    protected void checkPathSyntax(String path) {
        if (path == null) {
            throw new NullPointerException("path is null");
        }
        
        if (!path.startsWith(EntryUtil.ROOT_DIRECTORY)) {
            throw new IllegalArgumentException(
                    "\"" + path + "\" is not correct, all pathes " +
            		"have to be absolute (thus, starts with)" +
            		EntryUtil.ROOT_DIRECTORY);
        }
        
        String doubleSeparator = EntryUtil.PATH_SEPARATOR
                               + EntryUtil.PATH_SEPARATOR;
        if (path.contains(doubleSeparator)) {
            throw new IllegalArgumentException(
                    "\"" + path + "\" is not correct, it contains "
                    + doubleSeparator);
        }
    }
}