/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** An in-memory implementation of a Map
 * This class is a map where put make a copy of the value. It is used for
 * stubbing purpose. It emulates the behavior of a DHT and thus can be used
 * by {@link Storage} to store data and blocks.
 */
public class InMemoryDisworkMap extends HashMap<String, byte[]>
                                                       implements DisworkMap {
    
    private static final long serialVersionUID = 1L;
    
    private final Log log = LogFactory.getLog(InMemoryDisworkMap.class);
    
    @Override
    public byte[] put(String key, byte[] value) {
        byte[] valueCopy = value.clone();
        if (log.isTraceEnabled()) {
            log.trace("put(\"" + key + "\"," + Arrays.toString(value) + ") " +
                      valueCopy.length);
        }
        return super.put(key, valueCopy);
    }

    @Override
    public void close() throws IOException {
        // nothing to do
    }
}