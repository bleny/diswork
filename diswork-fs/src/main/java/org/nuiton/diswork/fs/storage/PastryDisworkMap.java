/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import java.io.IOException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.diswork.fs.DisworkFileSystemException;

import rice.Continuation;
import rice.environment.Environment;
import rice.p2p.commonapi.Id;
import rice.p2p.past.ContentHashPastContent;
import rice.p2p.past.PastContent;
import rice.p2p.past.PastImpl;
import rice.pastry.NodeIdFactory;
import rice.pastry.PastryNode;
import rice.pastry.PastryNodeFactory;
import rice.pastry.commonapi.PastryIdFactory;
import rice.pastry.socket.SocketPastryNodeFactory;
import rice.pastry.standard.RandomNodeIdFactory;
import rice.persistence.EmptyCache;
import rice.persistence.MemoryStorage;
import rice.persistence.StorageManagerImpl;

/**
 * An implementation of {@link DisworkMap} using FreePastry.
 * 
 * @see <a href="https://trac.freepastry.org/">FreePastry site</a>
 *
 */
public class PastryDisworkMap implements DisworkMap {

    private final Log log = LogFactory.getLog(PastryDisworkMap.class);

    protected static class ByteArrayPastContent extends ContentHashPastContent {

        private static final long serialVersionUID = 7584834640788361309L;

        byte[] content;

        public ByteArrayPastContent(Id id, byte[] content) {
            super(id);
            this.content = content;
        }

        public String toString() {
            return "ByteArrayPastContent (" + content.length + " bytes)";
        }
    }
    
    protected abstract class MyContinuation<R, E extends Exception>
                                       implements Continuation<R, Exception> {

        protected boolean finished = false;
        
        public boolean isFinished() {
            return finished;
        }
        
        public void waitUntilFinished() {
            while(!finished) {
                try {
                    Thread.sleep(1500);
                    log.info("waiting for response");
                } catch (InterruptedException e) {
                    throw new RuntimeException
                                ("interrupted while waiting for response", e);
                }
            }
        }

        @Override
        public void receiveException(Exception exception) {
            log.error("exception received : " + exception);
            finished = true;
        }

        @Override
        public void receiveResult(R result) {
            finished = true;            
        }

    }

    protected PastImpl past;
    
    protected PastryIdFactory pastryIdFactory;
    
    public PastryDisworkMap(DisworkFileSystemConfig config)
                                           throws DisworkFileSystemException {
        
        try {
            Environment env = new Environment();

            // disable the UPnP setting (in case you are testing this on a NATted LAN)
            // env.getParameters().setString("nat_search_policy", "never");

            // the port to use locally
            int bindport = config.getUsedPort();

            InetAddress bootaddr = InetAddress.getByName(config.getBootstrapIp());
            int bootport = config.getBootstrapPort();

            InetSocketAddress bootaddress = new InetSocketAddress(bootaddr, bootport);

            log.info("boot address : " + bootaddress);

            NodeIdFactory nidFactory = new RandomNodeIdFactory(env);

            // construct the PastryNodeFactory, this is how we use rice.pastry.socket
            PastryNodeFactory factory = null;
        
            // FIXME 20100602 bleny this is a work-around to deal with time out on
            // connect exceptions that occurs "sometimes"
            int numberOfTry = 0;
            while(factory == null && numberOfTry <= 10) {
                try {
                    factory = new SocketPastryNodeFactory(nidFactory, bindport, env);
                } catch (BindException e) {
                    // the bootstrap node can't be joined
                    throw new IOException(""/*"bootstrap node can't be joined"*/, e);
                } catch (IllegalStateException e) {
                    // the bootstrap node can't be joined
                    throw new IOException
                           ("unable to bind to already used port " + bindport, e);                
                } catch (ConnectException e) {
                    // this occurs some times, it may work after some time... 
                    numberOfTry += 1;
                    try {
                        Thread.sleep(10 * 1000);
                    } catch (InterruptedException ee) {
                        throw new IOException("Pastry boot process interrupted", ee);
                    }
                }
            }
            if (factory == null) {
                throw new IOException("unable to connect");
            }
            
            // construct a node, but this does not cause it to boot
            PastryNode node = factory.newNode();
            
            // in later tutorials, we will register applications before calling boot
            node.boot(bootaddress);
    
            // the node may require sending several messages to fully boot into the ring
            synchronized(node) {
                while(!node.isReady() && !node.joinFailed()) {
                    // delay so we don't busy-wait
                    try {
                        node.wait(500);
                    } catch (InterruptedException e) {
                        throw new IOException("Pastry boot process interrupted", e);
                    }
    
                    // abort if can't join
                    if (node.joinFailed()) {
                        throw new IOException("Could not join the FreePastry ring. "
                                + "Reason : " + node.joinFailedReason()); 
                    }
                }
            }
    
            log.info("finished creating new node " + node);
    
            pastryIdFactory = new PastryIdFactory(env);
            
            rice.persistence.Storage storage = new MemoryStorage(pastryIdFactory);
            past = new PastImpl(
                       node,
                       new StorageManagerImpl(
                           pastryIdFactory,
                           storage,
                           // Using a cache do not permit to remove while putting
                           /*
                           new LRUCache(
                               new MemoryStorage(pastryIdFactory),
                               512 * 1024,
                               node.getEnvironment()
                           )
                           */
                           new EmptyCache(pastryIdFactory)
                       ),
                       0, // replication factor
                       ""
                   );
        } catch (IOException e) {
            throw new DisworkFileSystemException("network failure while"
                                                 + " bootstraping", e);
        }
    }

    protected class ContainsKeyContinuation extends
                                          MyContinuation<Boolean, Exception> {
        
        public Boolean result = null;
        
        @Override
        public void receiveResult(Boolean result) {
            log.info("contains key result received : " + result);
            this.result = result;
            finished = true;
        }
    }
    
    @Override
    public boolean containsKey(Object key) {
        Id id = pastryIdFactory.buildId((String) key);
        ContainsKeyContinuation continuation = new ContainsKeyContinuation();
        past.existsInOverlay(id, continuation);
        continuation.waitUntilFinished();
        boolean result = continuation.result;
        log.info("containsKey " + key + " (id = " + id + ") returns " + result);
        return result;  
    }

    protected class GetContinuation extends
                                      MyContinuation<PastContent, Exception> {
        
        public byte[] result = null;
        
        @Override
        public void receiveResult(PastContent result) {
            log.info("get result received " + result);
            if (result != null) {
                this.result = ((ByteArrayPastContent) result).content;
            }
            finished = true;
        }

        @Override
        public void receiveException(Exception exception) {
            log.error("exception received while getting : " + exception);
            finished = true;
        }
    }
    
    protected byte[] atomicGet(Object key) {
        Id id = pastryIdFactory.buildId((String) key);   
        GetContinuation getContinuation = new GetContinuation();
        past.lookup(id, getContinuation);
        getContinuation.waitUntilFinished();
        return getContinuation.result;
    }
    
    @Override
    public byte[] get(Object key) {     
        byte[] result = atomicGet(key);
        if (result == null) {
            log.info("get " + key + " returns null");
        } else {
            log.info("get " + key + " returns " + result.length + " bytes");
        }
        return result;
    }

    protected class PutContinuation extends
                                        MyContinuation<Boolean[], Exception> {
        @Override
        public void receiveResult(Boolean[] result) {
            log.info("insert result received : " + Arrays.toString(result));
            finished = true;
        }

        @Override
        public void receiveException(Exception exception) {
            log.error("exception received while inserting : " + exception);
            finished = true;
        }
    }
    
    protected void atomicPut(String key, byte[] value) {
        Id id = pastryIdFactory.buildId(key);
        PastContent pastContent = new ByteArrayPastContent(id, value);        
        PutContinuation putContinuation = new PutContinuation();
        past.insert(pastContent, putContinuation);
        putContinuation.waitUntilFinished();
    }
    
    @Override
    public byte[] put(String key, byte[] value) {
        byte[] previousValue = atomicGet(key);
        if (previousValue != null) {
            atomicRemove(key);
        }
        atomicPut(key, value);
        log.info("put " + value.length + " bytes at " + key );
        return previousValue;        
    }

    protected class RemoveContinuation extends
                                          MyContinuation<Boolean, Exception> {
        
        public Boolean result = null;
        
        @Override
        public void receiveResult(Boolean result) {
            log.info("remove result received : " + result);
            this.result = result;
            finished = true;
        }

        @Override
        public void receiveException(Exception exception) {
            log.error("exception received while removing : " + exception);
            finished = true;
        }
    }
    
    protected void atomicRemove(Object key) {
        Id id = pastryIdFactory.buildId((String) key);
        RemoveContinuation removeContinuation = new RemoveContinuation();
        past.remove(id, removeContinuation);
        removeContinuation.waitUntilFinished();
        log.info("atomic remove " + key + " has returned " +
                 removeContinuation.result);
    }
    
    @Override
    public byte[] remove(Object key) {
        byte[] previousValue = atomicGet(key);
        if (previousValue == null) {
            log.info("remove " + key + " returns null");
        } else {
            atomicRemove(key);
            log.info("remove " + key + " returns " + previousValue.length +
                     " bytes");
        }
        return previousValue;  
    }

    @Override
    public void close() throws IOException {
        // TODO 20100528 bleny should close Pastry and close all sockets
        // throw new UnsupportedOperationException("not yet implemented");
        /*
        ServerSocket s = new ServerSocket(9001);
        s.getChannel().close();
        s.close();
        */
    }

    @Deprecated
    @Override
    public void putAll(Map<? extends String, ? extends byte[]> arg0) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public int size() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public Collection<byte[]> values() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public Set<String> keySet() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public boolean containsValue(Object arg0) {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Deprecated
    @Override
    public Set<java.util.Map.Entry<String, byte[]>> entrySet() {
        throw new UnsupportedOperationException();
    }

}
