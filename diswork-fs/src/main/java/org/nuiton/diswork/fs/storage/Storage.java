/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.diswork.fs.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.diswork.fs.DisworkFileSystemConfig;
import org.nuiton.diswork.fs.DisworkFileSystemException;

/** 
 * This class is the middle layer between the File System operations and
 * the Map<String, byte[]>. It offers put and get on directories, files
 * and links and deal with splitting consideration and concurrency
 * 
 * It needs a place to store data called a {@link DisworkMap}.
 */
public class Storage {

    private static final Log log = LogFactory.getLog(Storage.class);

    protected static final long LOCK_VALID_TIME = 60 * 60 * 1000;
    
    /** this id will be used to sign locks */
    protected String ownerId = EntryUtil.generateId();

    /** a key where the lock for key should be put at the returned value */
    protected static String keyToLockKey(String key) {
        return key + "_lock";
    }

    /** when using copy-on-write new data for the value of key should be stored
     * to the returned value */    
    protected static String keyToNewDataKey(String key) {
        return key + "_new_data";
    }
    
    // FIXME 20100602 bleny this collection should be stored somewhere in
    // the map so another node will perform remove if this one fail
    protected List<String> idsToBeRemoved = new ArrayList<String>();
    
    /**
     * This class is a InputStream used to read data from the map. When
     * someone read from the filesytem, he will bre returned a
     * SplitBlocksInputStream. This InputStream loads blocks only when needed 
     */
    protected class SplitBlocksInputStream extends InputStream {

        protected Map<String, byte[]> map;
        protected String id;

        protected String[] blockIds;
        protected int blockIdsIndex;

        protected ByteArrayInputStream currentBlock;
        
        protected int numberOfAvailableBytes;

        public SplitBlocksInputStream(Map<String, byte[]> map, String id) {  
            super();
            this.map = map;
            this.id = id;
        }

        @Override
        public int read() throws IOException {
            checkInitialization();
            
            if (blockIdsIndex >= blockIds.length) {
                return -1;
            }

            // update current block if needed
            if (currentBlock == null || currentBlock.available() == 0) {
                blockIdsIndex += 1;
                if (blockIdsIndex < blockIds.length) {
                    // closing previously opened stream
                    IOUtils.closeQuietly(currentBlock);
                    byte[] data = map.get(blockIds[blockIdsIndex]);
                    if (data == null) {
                    	// maybe data has not been posted or DHT replication
                    	// failed
                    	log.error("a block is missing into the map");
                    	throw new IOException("missing block");
                    }
                    currentBlock = new ByteArrayInputStream(data);
                    log.debug("new current block (size = " +
                                    currentBlock.available() + ")");
                } else {
                    return -1;
                }
            }
            
            int result = currentBlock.read();
            numberOfAvailableBytes -= 1;
            
            if(log.isDebugEnabled()) {
                log.debug("reading block number " + blockIdsIndex  +
                        " (available = " + numberOfAvailableBytes + ")" +
                        " returns " + result);
            }
            return result;
        }
        
        // This method is called at the first read or the first available
        // it initialize all the necessary field
        protected void checkInitialization() {
            if (blockIds == null) {                
                byte[] bytes = map.get(id);
                String metaBlock = EntryUtil.bytesToString(bytes);
                blockIds = EntryUtil.getBlockIdsFromMetaBlock(metaBlock);
                blockIdsIndex = -1;
                numberOfAvailableBytes = 
                                 EntryUtil.getTotalSizeFromMetaBlock(metaBlock);
                log.debug("initializing stream with meta block \"" + metaBlock
                        + "\" (" + blockIds.length + " blocks, " +
                        numberOfAvailableBytes + " bytes)");
            }
        }
        
        @Override
        public int available() {
            checkInitialization();
            return numberOfAvailableBytes;            
        }

    }

    protected DisworkMap map;
    
    protected DisworkFileSystemConfig config;

    /**
     * 
     * @param config an instance of 
     * @param map null at normal use. Will set map according to the content of
     *        config 
     * @throws DisworkFileSystemException if network fail
     */
    public Storage(DisworkFileSystemConfig config, DisworkMap map)
                                           throws DisworkFileSystemException {

        this.config = config;

        if (map == null) {
            // instantiating a map according to config directives
            String mapType = config.getOption("diswork.fs.map_type");
            
            if (mapType == null) {
                log.info("no map type specified");
                this.map = null;
            } else {
                if ("inmemory".equals(mapType)) {
                    log.info("using in-memory map");
                    this.map = new InMemoryDisworkMap();
                } else if ("pastry".equals(mapType)) {
                    log.info("using Pastry map");
                    this.map = new PastryDisworkMap(config);
                } else if ("kademlia".equals(mapType)) {
                    log.info("using Kademlia map");
                    this.map = new KademliaDisworkMap(config);
                }
            }
        } else {
            this.map = map;
        }
        
        
        // creating root directory, if needed
        if (! this.map.containsKey(EntryUtil.ROOT_DIRECTORY)) {
            log.info("creating root directory");
            putDirectory(EntryUtil.ROOT_DIRECTORY,
                         EntryUtil.EMPTY_DIRECTORY_CONTENT);
        }
    }
    
    public Storage(DisworkFileSystemConfig config)
                                           throws DisworkFileSystemException {
        this(config, null);
    }

    /**
     * @return the content (entries) of the root directory
     * @throws DisworkFileSystemException
     */
    public String getRootDirectory() throws DisworkFileSystemException {
        String result = getDirectory(EntryUtil.ROOT_DIRECTORY);
        return result;
    }

    
    
    /**
     * @param id an id of a directory
     * @return a the entries of the directory
     * @throws DisworkFileSystemException
     */    
    public String getDirectory(String id) throws DisworkFileSystemException {
        InputStream in = get(id);
        try {
            String content = IOUtils.toString(in);
            log.debug("getDirectory(\"" + id + "\") returns \n" + content);
            return content;
        } catch (IOException e) {
            throw new DisworkFileSystemException(e);
        }
    }

    public InputStream getFile(String id) {
        InputStream result = get(id);
        return result;
    }

    public String getLink(String id) {
        String content = EntryUtil.bytesToString(map.get(id));
        log.debug("getLink(\"" + id + "\") returns \"" + content + "\"");
        return content;
    }

    public void putDirectory(String id, String content)
                                           throws DisworkFileSystemException {
        log.debug("putDirectory(\"" + id + "\", \"" + content + "\")");
        InputStream value = IOUtils.toInputStream(content);
        put(id, value);
    }

    public void putFile(String id, InputStream content)
                                           throws DisworkFileSystemException {
        try {
            log.debug("putFile(\"" + id + "\", stream of "+ content.available()
                      + " bytes)");
            put(id, content);
        } catch (IOException e) {
            throw new DisworkFileSystemException(e);
        }
    }

    public void putLink(String id, String content) {
        // FIXME 20100715 bleny since put() is not used to skip split, there is no concurrency management
        log.debug("putLink(\"" + id + "\", \"" + content + "\")");
        byte[] contentAsBytes = EntryUtil.stringToBytes(content);
        map.put(id, contentAsBytes);
    }

    public void removeDirectory(String id) throws DisworkFileSystemException {
        log.debug("removeDirectory(\"" + id + "\")");
        remove(id);
    }

    public void removeFile(String id) throws DisworkFileSystemException {
        log.debug("removeFile(\"" + id + "\")");
        remove(id);
    }

    public void removeLink(String id) {
        log.debug("removeLink(\"" + id + "\")");
        removeKey(id);
    }


    /**
     * see {@link #get(String)}
     */
    protected InputStream get(String key) {
        InputStream in = new SplitBlocksInputStream(map, key);
        return in;
    }

    /**
     * a put in the map, this involves split considerations and concurrency.
     * a lock is acquired (it might have been acquired before).
     * Lock is released at the end  
     * @param key
     * @param value
     * @throws DisworkFileSystemException
     */
    protected void put(String key, InputStream value)
                                           throws DisworkFileSystemException {
        boolean lockAcquired = tryToLock(key);
        
        if (lockAcquired) {
            try {
                log.debug("lock on " + key + " acquired");

                // here, we know we can write or an exception
                // would have been thrown earlier
                String blocksIds = "";
                int readResult = 0;
                int totalSize = 0;

                String metaBlock = totalSize + blocksIds;        
                String newDataKey = keyToNewDataKey(key);
                map.put(newDataKey, EntryUtil.stringToBytes(metaBlock));
                
                // creating a buffer of the size of a block
                int bufferSize = config.getBlockSize();        
                byte[] buffer = new byte[bufferSize];
                
                while ((readResult = value.read(buffer)) != -1) {
                    totalSize += readResult;
    
                    byte[] newBlock = buffer;
    
                    // if the buffer is not full, truncate the block
                    if (readResult < buffer.length) {
                        newBlock = new byte[readResult];
                        System.arraycopy(buffer, 0, newBlock, 0, readResult);
                    }
    
                    String id = EntryUtil.generateId();
                    blocksIds += EntryUtil.BLOCKIDS_SEPARATOR + id;
    
                    log.debug("saving new block (size = " + newBlock.length +
                                                           ") at key " + id);
    
                    // copy block in map 
                    map.put(id, newBlock);
    
                    metaBlock = totalSize + blocksIds;
    
                    log.debug("putting metablock " + metaBlock + " at key "
                                                                       + key);
                    
                    map.put(newDataKey, EntryUtil.stringToBytes(metaBlock));
                    
                    // updating lock
                    updateLock(key);
                }
                
                eraseDependenciesOfMetablock(key);
                
                if (isLockStillOwned(key)) {
                    map.put(key, map.get(newDataKey));
                }
                
                map.remove(newDataKey);
                
                unLock(key);
            } catch (IOException e) {
                throw new DisworkFileSystemException
                          ("can't read value", e);
            }
            
        } else {
            throw new DisworkFileSystemException("key " + key + " is locked"
                                       + " another node way being writing it");
        }
    }

    protected void remove(String key) throws DisworkFileSystemException {
        eraseDependenciesOfMetablock(key);
        removeKey(key);
    }
    
    /**
     * due to concurrency, key should not be removed immediately. Someone may
     * read it now. It will be removed later, using {@link #clean()}. 
     * @param key the key to be removed from the map
     */
    public void removeKey(String key) {
        idsToBeRemoved.add(key);
    }
    
    /**
     * this method clean the map by removing all the blocks that are not
     * referenced (ie, their key is not in any meta-block or in entries)
     */
    public void clean() {
        for (String key : idsToBeRemoved) {
            map.remove(key);
        }
    }

    public void close() throws DisworkFileSystemException {
        clean();
        try {
            map.close();
        } catch (IOException e) {
            log.error("error while closing map", e);
            throw new DisworkFileSystemException("unable to close the map", e);
        }
    }
    
    /**
     * 
     * @param key
     * @return true if lock acquired, false is lock is owned by another
     */
    public boolean tryToLock(String key) {
        // trying to acquire lock
        log.debug("trying to acquire a lock on " + key);

        String lockKey = keyToLockKey(key);
        byte[] lock = map.put(lockKey, EntryUtil.newLock(ownerId));
        
        Boolean result = null;
        // if there was no lock or if current lock is mine
        if (lock == null || 
                (EntryUtil.getOwnerFromLock(lock).equals(ownerId))) {
            
            // file was not locked, we have took the lock
            result = true;
        } else {
            // file is locked by other node, check date
            if (isObsolete(lock)) {
                log.info("lock is out-dated");
                // this lock is out-dated, let's erase all data
                eraseNewData(key);
                result = true;
            } else {
                log.info(key + " is currently written, stopping operation");
                map.put(lockKey, lock);
                result = false;
            }
        }
        return result;
    }
    
    protected void updateLock(String key) {
        String lockKey = keyToLockKey(key);
        map.put(lockKey, EntryUtil.newLock(ownerId));
    }

    public void unLock(String key) {
        String lockKey = keyToLockKey(key);
        map.remove(lockKey);
    }
    
    protected void eraseNewData(String key) {
        String newDataKey = keyToNewDataKey(key);
        eraseDependenciesOfMetablock(newDataKey);
    }
    
    /**
     * the value at this key has to be a metablock
     * @param key
     */
    protected void eraseDependenciesOfMetablock(String key) {
        byte[] value = map.get(key);
        if (value != null) {
            String obsoleteMetaBlock = EntryUtil.bytesToString(value);
            if (obsoleteMetaBlock != null) {
                String[] obsoleteBlocksIds =
                      EntryUtil.getBlockIdsFromMetaBlock(obsoleteMetaBlock);
                log.debug("removing " + obsoleteBlocksIds.length + 
                          " old blocks");
                for (String obsoleteBlockId : obsoleteBlocksIds) {
                    removeKey(obsoleteBlockId);
                }
            }
        }
    }

    protected boolean isLockStillOwned(String key) {
        String lockKey = keyToLockKey(key);
        byte[] lock = map.get(lockKey);
        String lockOwner = EntryUtil.getOwnerFromLock(lock);
        boolean isLockOwned = ownerId.equals(lockOwner);
        return isLockOwned;
    }
    
    protected boolean isObsolete(byte[] lock) {
        Long currentDate = System.currentTimeMillis();
        Long currentLockTime = EntryUtil.getTimeFromLock(lock);
        Long lockAge = currentDate - currentLockTime;
        return lockAge > LOCK_VALID_TIME;
    }

    /**
     * a setter to inject the map dependency
     */
    public void setMap(DisworkMap map) {
        this.map = map;
    }

    public DisworkMap getMap() {
        return map;  
    }
    
}