/*
 * #%L
 * disworkfs
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
/**
 * DisworkFS is a distributed file system. You can use it by instantiating
 * {@link org.nuiton.diswork.fs.DisworkFileSystem} and use it to store
 * directories and their content.
 * 
 * You can change the default DisworkFileSystem behavior by provide a
 * {@link org.nuiton.diswork.fs.DisworkFileSystemConfig} instance at
 * construction. If you don't provide one, a instance will be created
 */

package org.nuiton.diswork.fs;